"""
    In this file handling all flask api route and maintain all of operation and sessions
"""

import json
import os
import random
import uuid
from datetime import datetime, timedelta
from functools import wraps

import html
import jwt
import pandas as pd
from flask import (flash, Flask, redirect, render_template, request,
                   session, url_for, send_file, jsonify)
from flask_cors import CORS
from flask_mail import Mail
from flask_socketio import SocketIO, emit
from string import ascii_uppercase
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename

from constant import constant_data
from operations.common_func import (search_and_display_panel_data, is_date_passed, edit_panel_data,
                                    calculate_remaining_milliseconds, get_filtered_admin_data,
                                    export_student_panel_data, get_chat_timestamp, search_panel_data,
                                    delete_all_panel_data,checking_upload_folder,get_filtered_student_data,
                                    get_unique_subject_id, export_teacher_panel_data, export_panel_data,
                                    delete_panel_data,get_search_data,get_student_data,
                                    get_unique_department_id, get_profile_data, calculate_time_ago,
                                    get_unique_admin_id, get_admin_data, validate_phone_number, password_validation,
                                    logger_con, get_timestamp, get_unique_student_id, sending_email_mail,
                                    get_unique_teacher_id, get_all_country_state_names, delete_teacher_panel_data)
from operations.mongo_connection import (mongo_connect, data_added, find_all_data, find_spec_data, update_mongo_data)

secreat_id = uuid.uuid4().hex

# create a flask app instance
app = Flask(__name__)

# Apply cors policy in our app instance
CORS(app)

# setup all config variable
app.config["enviroment"] = constant_data.get("enviroment", "qa")
app.config["SECRET_KEY"] = secreat_id
app.config['MAIL_SERVER'] = constant_data["mail_configuration"].get("server_host")
app.config['MAIL_PORT'] = int(constant_data["mail_configuration"].get("server_port"))
app.config['MAIL_USERNAME'] = constant_data["mail_configuration"].get("server_username")
app.config['MAIL_PASSWORD'] = constant_data["mail_configuration"].get("server_password")
app.config['MAIL_USE_SSL'] = True
app.config["PROFILE_UPLOAD_FOLDER"] = 'static/uploads/profiles/'
app.config["EXPORT_UPLOAD_FOLDER"] = 'static/uploads/export_file/'
app.config["MAX_CONTENT_LENGTH"] = 16 * 1024 * 1024
app.config["IMPORT_UPLOAD_FOLDER"] = 'static/uploads/import_file/'
app.config['REJECTED_DATA_UPLOAD_FOLDER'] = 'static/uploads/rejected_data/'
app.config["email_configuration"] = {}
app.config["mapping_user_dict"] = {}
app.config["user_mapping_dict"] = {}
app.config["chats_data"] = {}

# handling our application secure type like http or https
secure_type = constant_data["secure_type"]

# create mail instance for our application
mail = Mail(app)

# logger & MongoDB connection
logger_con(app=app)
client = mongo_connect(app=app)

# allow only that image file extension
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif' 'svg'}
socketio = SocketIO(app)

def allowed_photos(filename):
    """
    checking file extension is correct or not

    :param filename: file name
    :return: True, False
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def token_required(func):
    # decorator factory which invoks update_wrapper() method and passes decorated function as an argument
    @wraps(func)
    def decorated(*args, **kwargs):
        login_dict = session.get("login_dict", "available")
        # token = app.config["mapping_user_dict"].get(login_dict.get("id", "nothing"), {}).get("token", False)
        if login_dict == "available":
            app.logger.debug("please first login in your app...")
            flash("Please login first...", "danger")
            return redirect(url_for('login', _external=True, _scheme=secure_type))
        return func(*args, **kwargs)
    return decorated

def generate_token(username):
    try:
        expiration_time = datetime.now() + timedelta(minutes=30)
        payload = {
            'username': username,
            'exp': expiration_time
        }
        token = jwt.encode(payload, app.config["SECRET_KEY"], algorithm='HS256')
        return token
    except Exception as e:
        app.logger.debug(f"error in generate token {e}")

############################ Login operations ##################################

@app.route("/view_logs", methods=['GET'])
def view_logs():
    try:
        server_file_name = "server.log"
        file = os.path.abspath(server_file_name)
        lines = []
        with open(file, "r") as f:
            lines += f.readlines()
        return render_template("logs.html", lines=lines)

    except Exception as e:
        app.logger.debug(f"Error in show log api route : {e}")
        return {"message": "data is not present"}

@app.route("/", methods=["GET", "POST"])
def login():
    """
    In this route we can handling student, teacher and admin login process
    :return: login template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        if login_dict != "nothing":
            type = login_dict["type"]
            if type == "student":
                return redirect(url_for('student_dashboard', _external=True, _scheme=secure_type))
            elif type == "teacher":
                return redirect(url_for('teacher_dashboard', _external=True, _scheme=secure_type))
            else:
                return redirect(url_for('admin_dashboard', _external=True, _scheme=secure_type))

        db = client["college_management"]
        all_types = ["Teacher", "Student", "Admin"]
        if request.method == "POST":
            email = request.form["email"]
            password = request.form["password"]

            di = {"username": email}
            di_email = {"email": email}
            teacher_data = find_spec_data(app, db, "login_mapping", di)
            teacher_data_email = find_spec_data(app, db, "login_mapping", di_email)
            teacher_data = list(teacher_data)
            teacher_data_email = list(teacher_data_email)

            if len(teacher_data) == 0 and len(teacher_data_email) == 0:
                flash("Please use correct credential..", "danger")
                return render_template("authentication/login.html", all_types=all_types)
            elif len(teacher_data)>0:
                teacher_data = teacher_data[0]
                if check_password_hash(teacher_data["password"], password):
                    id = teacher_data["username"]
                    type = teacher_data["type"]
                    photo_link = teacher_data["photo_link"]
                    session["otp_dict"] = {"email": teacher_data["email"]}
                    startmessage = "Nice meeting you again"
                    token = generate_token(id)
                    session["mapping_user_dict"] = {"token": token}
                    session["username"] = id
                    session["login_dict"] = {"id": id, "type": type, "photo_link": photo_link, "startmessage":startmessage}
                    app.logger.debug(f"Login Dict in session: {session.get('login_dict')}")
                    app.logger.debug(f"otp Dict in session: {session.get('otp_dict')}")
                    return redirect(url_for("otp_verification", _external=True, _scheme=secure_type))
                else:
                    flash("Please use correct credential..", "danger")
                    return render_template("authentication/login.html", all_types=all_types)
            else:
                teacher_data_email = teacher_data_email[0]
                if check_password_hash(teacher_data_email["password"], password):
                    id = teacher_data_email["username"]
                    type = teacher_data_email["type"]
                    photo_link = teacher_data_email["photo_link"]
                    startmessage = "Nice meeting you again"
                    token = generate_token(id)
                    session["otp_dict"] = {"email": teacher_data_email["email"]}
                    # app.logger.debug(f"token in login route : {token}")
                    session["mapping_user_dict"] = {"token": token}
                    session["username"] = id
                    session["login_dict"] = {"id": id, "type": type, "photo_link": photo_link, "startmessage": startmessage}
                    app.logger.debug(f"Login Dict in session: {session.get('login_dict')}")
                    app.logger.debug(f"otp Dict in session: {session.get('otp_dict')}")
                    flash("Login Successfully...", "success")
                    return redirect(url_for("otp_verification", _external=True, _scheme=secure_type))
                else:
                    flash("Please use correct credential..", "danger")
                    return render_template("authentication/login.html", all_types=all_types)

        else:
            return render_template("authentication/login.html", all_types=all_types)

    except Exception as e:
        app.logger.debug(f"Error in login route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('login', _external=True, _scheme=secure_type))

@app.route("/otp_verification", methods=["GET", "POST"])
def otp_verification():
    """
    That funcation can use otp_verification and new_password set link generate
    """

    try:
        email = session.get("otp_dict", {}).get("email", "")
        if request.method == "POST":
            get_otp = request.form["otp"]
            get_otp = int(get_otp)
            send_otp = session.get("otp", "")
            if get_otp:
                login_dict = session.get("login_dict", "nothing")
                type = login_dict["type"]
                flash("Login Successfully...", "success")
                return redirect(url_for(f'{type}_dashboard', _external=True, _scheme=secure_type))
            else:
                flash("OTP is wrong. Please enter correct otp...", "danger")
                return render_template("authentication/otp_verification.html")
        else:
            if email:
                otp = random.randint(100000, 999999)
                session["otp"] = otp
                server_host = app.config['MAIL_SERVER']
                server_port = app.config['MAIL_PORT']
                server_username = app.config['MAIL_USERNAME']
                server_password = app.config['MAIL_PASSWORD']
                subject_title = "OTP Received"
                mail_format = f"Hello There,\n We hope this message finds you well. As part of our ongoing commitment to ensure the security of your account, we have initiated a verification process.\nYour One-Time Password (OTP) for account verification is: [{otp}]\nPlease enter this OTP on the verification page to complete the process. Note that the OTP is valid for a limited time, so we recommend entering it promptly.\nIf you did not initiate this verification or have any concerns regarding your account security, please contact our support team immediately at help@codescatter.com\n\nThank you for your cooperation.\nBest regards,\nCodescatter"
                html_format = f'<p>Hello There,</p><p>We hope this message finds you well. As part of our ongoing commitment to ensure the security of your account, we have initiated a verification process.</p><p>Your One-Time Password (OTP) for account verification is: <h2><b>{otp}</h2></b></p><p>Please enter this OTP on the verification page to complete the process. Note that the OTP is valid for a limited time, so we recommend entering it promptly.</p><p>If you did not initiate this verification or have any concerns regarding your account security, please contact our support team immediately at help@codescatter.com</p><p>Thank you for your cooperation.</p><p>Best regards,<br>Codescatter</p>'
                attachment_all_file = []
                # sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                #                    server_password, server_host, int(server_port), attachment_all_file)
            return render_template("authentication/otp_verification.html")

    except Exception as e:
        app.logger.debug(f"Error in otp verification route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('otp_verification', _external=True, _scheme=secure_type))

@app.route("/forgot_password", methods=["GET", "POST"])
def forgot_password():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        db = client["college_management"]
        if request.method == "POST":
            email = request.form["email"]
            all_login_data = find_spec_data(app, db, "login_mapping", {"email": email})
            all_login_data = list(all_login_data)
            if len(all_login_data)==0:
                flash("Entered email does not exits Please try with different mail...", "danger")
                return render_template("authentication/forgat-password.html")
            else:
                login_data = list(all_login_data)[0]
                type = login_data.get("type", "student")
                if type=="student":
                    id_data = login_data.get("student_id", "")
                elif type=="teacher":
                    id_data = login_data.get("teacher_id", "")
                elif type=="admin":
                    id_data = login_data.get("admin_id", "")

                server_host = app.config['MAIL_SERVER']
                server_port = app.config['MAIL_PORT']
                server_username = app.config['MAIL_USERNAME']
                server_password = app.config['MAIL_PASSWORD']
                subject_title = "OTP Received"
                mail_format = f"Hello There,\n I hope this email finds you well. It has come to our attention that you have requested to reset your password for your APPIACS account. If you did not initiate this request, please disregard this email.\nTo reset your password,\nplease follow the link below: \nClick Here \nPlease note that this link is valid for the next 30 Minutes. After this period, you will need to submit another password reset request.\nIf you continue to experience issues or did not request a password reset, please contact our support team for further assistance.\nThank you for using Website.\n\nBest regards,\nHarshit Gadhiya"
                html_format = f"<p>Hello There,</p><p> I hope this email finds you well. It has come to our attention that you have requested to reset your password for your APPIACS account. If you did not initiate this request, please disregard this email.</p><p>To reset your password,</p><p>please follow the link below: </p><p><a href='http://127.0.0.1:5000/update_password?id={type}-*{id_data}'><b>Click Here</b></a></p><p>Please note that this link is valid for the next 30 Minutes. After this period, you will need to submit another password reset request.</p><p>If you continue to experience issues or did not request a password reset, please contact our support team for further assistance.</p><p>Thank you for using the Website.</p><br><p>Best regards,<br>Harshit Gadhiya</p>"
                attachment_all_file = []
                sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                                   server_password, server_host, int(server_port), attachment_all_file)
                flash("Reset password mail sent successfully...", "success")
                return render_template("authentication/forgot-password.html")
        else:
            return render_template("authentication/forgot-password.html")

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...","danger")
        return redirect(url_for('student_mail', _external=True, _scheme=secure_type))

@app.route("/update_password", methods=["GET", "POST"])
def update_password():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        db = client["college_management"]
        id = request.args.get("id", "nothing")
        if request.method == "POST":
            id = session["data_id"]
            spliting_obj = id.split("-*")
            new_id = spliting_obj[-1]
            type = spliting_obj[0]
            password = request.form["password"]
            con_password = request.form["con_password"]
            if not password_validation(app=app, password=password):
                flash("Please choose strong password. Add at least 1 special character, number, capitalize latter..", "danger")
                return render_template("forgat-password.html", password=password, con_password=con_password)

            if not password_validation(app=app, password=con_password):
                flash("Please choose strong password. Add at least 1 special character, number, capitalize latter..", "danger")
                return render_template("forgat-password.html", password=password, con_password=con_password)

            if password==con_password:
                password = generate_password_hash(password)
                if type=="admin":
                    condition_dict = {"type": "admin", "admin_id": new_id}
                    update_mongo_data(app, db, "admin_data", condition_dict, {"password": password})
                elif type=="student":
                    condition_dict = {"type": "student", "student_id": new_id}
                    update_mongo_data(app, db, "students_data", condition_dict, {"password": password})
                elif type=="teacher":
                    condition_dict = {"type": "teacher", "teacher_id": new_id}
                    update_mongo_data(app, db, "teacher_data", condition_dict, {"password": password})

                update_mongo_data(app, db, "login_mapping", condition_dict, {"password": password})
                flash("Password Reset Successfully...", "success")
                return redirect(url_for('login', _external=True, _scheme=secure_type))
            else:
                flash("Password or Confirmation Password Does Not Match. Please Enter Correct Details", "danger")
                return render_template("forgat-password.html", password=password, con_password=con_password)
        else:
            session["data_id"] = id
            return render_template("authentication/update_password.html", id=id)

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...","danger")
        return redirect(url_for('student_mail', _external=True, _scheme=secure_type))


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    """
    That funcation was logout session and clear user session
    """

    try:
        session.clear()
        app.logger.debug(f"session is {session}")
        return redirect(url_for('login', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"error is {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('login', _external=True, _scheme=secure_type))

########################## Common Operation Route ####################################

@app.route('/get_province')
def get_province():
    city_province_mapping = constant_data.get("city_province_mapping", {})
    city = request.args.get('city')
    province = city_province_mapping.get(city, 'Unknown')
    print(province)
    return province

@app.route("/admin/delete_data/<object>", methods=["GET", "POST"])
@token_required
def delete_data(object):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        if "event" in object or "department" in object or "homework" in object:
            spliting_object = object.split("$$$")
        else:
            spliting_object = object.split("-")
        panel = spliting_object[0]
        id = spliting_object[1]
        delete_dict = {}
        if panel == "admin":
            coll_name = "admin_data"
            delete_dict["username"] = id
            delete_dict["type"] = "admin"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))
        elif panel == "student":
            delete_dict["username"] = id
            delete_dict["type"] = "student"
            coll_name = "students_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('student_data_list', _external=True, _scheme=secure_type))
        elif panel == "feedback":
            delete_dict["feedback_msg"] = id
            coll_name = "feedback_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('student_data_list', _external=True, _scheme=secure_type))
        elif panel == "department":
            delete_dict["department_name"] = id
            coll_name = "department_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('department_data_list', _external=True, _scheme=secure_type))
        elif panel == "event":
            delete_dict["event_name"] = id.split("***")[0]
            delete_dict["event_description"] = id.split("***")[-1]
            coll_name = "event_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('event_data_list', _external=True, _scheme=secure_type))
        elif panel == "subject":
            delete_dict["subject_name"] = id
            coll_name = "subject_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('subject_data_list', _external=True, _scheme=secure_type))
        elif panel == "homework":
            datasplit = id.split("***")
            title = datasplit[0]
            department_name = datasplit[1]
            class_name = datasplit[2]
            delete_dict["title"] = title
            delete_dict["department_name"] = department_name
            delete_dict["class_name"] = class_name
            coll_name = "homework_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            coll_name1 = "homework_comment"
            delete_panel_data(app, client, "college_management", coll_name1, delete_dict)
            return redirect(url_for('homework_data_list', _external=True, _scheme=secure_type))
        else:
            delete_dict["username"] = id
            delete_dict["type"] = "teacher"
            coll_name = "teacher_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('teacher_data_list', _external=True, _scheme=secure_type))


    except Exception as e:
        app.logger.debug(f"Error in delete data from database: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('delete_data', _external=True, _scheme=secure_type))

@app.route("/admin/download_file/<object>", methods=["GET", "POST"])
@token_required
def download_file(object):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        object = object.replace("----", "/")
        filepath = os.path.abspath(object)
        return send_file(filepath, as_attachment=True)

    except Exception as e:
        app.logger.debug(f"Error in download file from database: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('download_file', _external=True, _scheme=secure_type))

@app.route("/admin/deleteall/<object>", methods=["GET", "POST"])
@token_required
def delete_all_data(object):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        panel = object
        if panel=="department":
            coll_name = "department_data"
        elif panel=="student":
            coll_name = "students_data"
        elif panel=="admin":
            coll_name = "admin_data"
        elif panel=="teacher":
            coll_name = "teacher_data"
        elif panel=="subject":
            coll_name = "subject_data"
        elif panel=="feedback":
            coll_name = "feedback_data"
        delete_result = delete_all_panel_data(app, client, "college_management", coll_name, panel)
        flash("All data deleted successfully", "success")
        return redirect(f'/admin/{panel}_data')

    except Exception as e:
        app.logger.debug(f"Error in delete data from database: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('delete_all_data', _external=True, _scheme=secure_type))

@app.route("/admin/export/<object>", methods=["GET", "POST"])
@token_required
def export_data(object):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        db = client["college_management"]
        spliting_object = object.split("-")
        panel = spliting_object[0]
        type = spliting_object[1]
        if panel == "admin":
            res = find_all_data(app, db, "admin_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "student":
            res = find_all_data(app, db, "students_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "department":
            res = find_all_data(app, db, "department_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "classes":
            res = find_all_data(app, db, "class_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "attendance":
            res = find_all_data(app, db, "attendance_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "feedback":
            res = find_all_data(app, db, "feedback_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "event":
            res = find_all_data(app, db, "event_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "subject":
            res = find_all_data(app, db, "subject_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "homework":
            res = find_all_data(app, db, "homework_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        else:
            res = find_all_data(app, db, "teacher_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)


    except Exception as e:
        app.logger.debug(f"Error in export data from database: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('export_data', _external=True, _scheme=secure_type))

@app.route("/admin/import_data/<panel_obj>", methods=["GET", "POST"])
@token_required
def import_data(panel_obj):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        db = client["college_management"]
        ## Check if panel object is in the mapping and request method is POST
        ## If yes, then it will store the id type in a variable with which it will be checking for present record
        if request.method == "POST":
            ## Getting file from request
            file = request.files["file"]
            ## Checking if file is selected, if yes, secure the filename
            if file.filename != "":
                ## Securing file and getting file extension
                file_name = secure_filename(file.filename)
                if not os.path.isdir(app.config['IMPORT_UPLOAD_FOLDER']):
                    os.makedirs(app.config['IMPORT_UPLOAD_FOLDER'], exist_ok=True)
                if not os.path.exists(app.config['REJECTED_DATA_UPLOAD_FOLDER']):
                    os.makedirs(app.config['REJECTED_DATA_UPLOAD_FOLDER'], exist_ok=True)
                file_path = os.path.join(app.config['IMPORT_UPLOAD_FOLDER'], file_name)
                file.save(file_path)
                file_extension = os.path.splitext(file_name)[1]

                if file_extension == ".xlsx":
                    dataload_excel = pd.read_excel(file_path)
                    json_record_data = dataload_excel.to_json(orient='records')
                    json_data = json.loads(json_record_data)
                elif file_extension == ".csv":
                    dataload = pd.read_csv(file_path)
                    json_record_data = dataload.to_json(orient='records')
                    json_data = json.loads(json_record_data)
                else:
                    with open(file_path, encoding='utf-8') as json_file:
                        json_data = json.load(json_file)


                flag = False
                if panel_obj == "admin":
                    get_login_data = find_spec_data(app, db, "login_mapping", {"type": "admin"})
                    all_login_data = []
                    for login_data in get_login_data:
                        di = {}
                        di["username"] = login_data["username"]
                        di["email"] = login_data["email"]
                        all_login_data.append(di)
                    for record in json_data:
                        record["admin_id"] = str(record["admin_id"])
                        make_dict = {}
                        make_dict["username"] = record["username"]
                        make_dict["email"] = record["email"]
                        if make_dict not in all_login_data:
                            make_dict = {}
                            make_dict["photo_link"] = record["photo_link"]
                            make_dict["admin_id"] = str(record["admin_id"])
                            make_dict["username"] = record["username"]
                            make_dict["email"] = record["email"]
                            if "scrypt" not in record["password"]:
                                make_dict["password"] = generate_password_hash(password=record["password"])
                            else:
                                make_dict["password"] = record["password"]
                            make_dict["type"] = "admin"
                            data_added(app, db, "login_mapping", make_dict)
                            coll_chats = db["chats_mapping"]
                            chats_dict = {}
                            chats_dict["username"] = record["username"]
                            chats_dict["photo_link"] = record["photo_link"]
                            chats_dict["user_mapping_dict"] = []
                            chats_dict["status"] = "inactive"
                            chats_dict["type"] = "admin"
                            chats_dict["last_seen"] = get_timestamp(app)
                            coll_chats.insert_one(chats_dict)
                            coll = db["admin_data"]
                            record["photo_link"] = record["photo_link"]
                            if "scrypt" not in record["password"]:
                                record["password"] = generate_password_hash(password=record["password"])
                            coll.insert_one(record)
                            notification_dict = {}
                            notification_dict["admin_id"] = str(record["admin_id"])
                            notification_dict["username"] = str(record["username"])
                            notification_dict["notification_list"] = []
                            notification_dict["type"] = "admin"
                            coll = db["notification_data"]
                            coll.insert_one(notification_dict)
                        else:
                            flag = True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "student":
                    get_login_data = find_spec_data(app, db, "login_mapping", {"type": "student"})
                    all_login_data = []
                    for login_data in get_login_data:
                        di = {}
                        di["username"] = login_data["username"]
                        di["email"] = login_data["email"]
                        all_login_data.append(di)
                    for record in json_data:
                        record["student_id"] = str(record["student_id"])
                        make_dict = {}
                        make_dict["username"] = record["username"]
                        make_dict["email"] = record["email"]
                        if make_dict not in all_login_data:
                            make_dict = {}
                            make_dict["photo_link"] = record["photo_link"]
                            make_dict["student_id"] = str(record["student_id"])
                            make_dict["username"] = record["username"]
                            make_dict["email"] = record["email"]
                            if "scrypt" not in record["password"]:
                                make_dict["password"] = generate_password_hash(password=record["password"])
                            else:
                                make_dict["password"] = record["password"]
                            make_dict["type"] = "student"
                            data_added(app, db, "login_mapping", make_dict)
                            coll_chats = db["chats_mapping"]
                            chats_dict = {}
                            chats_dict["username"] = record["username"]
                            chats_dict["photo_link"] = record["photo_link"]
                            chats_dict["user_mapping_dict"] = []
                            chats_dict["status"] = "inactive"
                            chats_dict["type"] = "student"
                            chats_dict["last_seen"] = get_timestamp(app)
                            coll_chats.insert_one(chats_dict)
                            coll = db["students_data"]
                            record["photo_link"] = record["photo_link"]
                            if "scrypt" not in record["password"]:
                                record["password"] = generate_password_hash(password=record["password"])
                            coll.insert_one(record)
                            notification_dict = {}
                            notification_dict["student_id"] = str(record["student_id"])
                            notification_dict["username"] = str(record["username"])
                            notification_dict["notification_list"] = []
                            notification_dict["type"] = "student"
                            coll = db["notification_data"]
                            coll.insert_one(notification_dict)
                            classes_mapping_dict = {}
                            classes_mapping_dict["student_id"] = str(record["student_id"])
                            classes_mapping_dict["department"] = record["department"]
                            classes_mapping_dict["class_name"] = record["classes"]
                            classes_mapping_dict["type"] = "student"
                            coll = db["class_data"]
                            coll.insert_one(classes_mapping_dict)
                        else:
                            flag = True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")

                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "teacher":
                    get_login_data = find_spec_data(app, db, "login_mapping",{"type": "teacher"})
                    all_login_data = []
                    for login_data in get_login_data:
                        di = {}
                        di["username"] = login_data["username"]
                        di["email"] = login_data["email"]
                        all_login_data.append(di)
                    for record in json_data:
                        record["teacher_id"] = str(record["teacher_id"])
                        make_dict = {}
                        make_dict["username"] = record["username"]
                        make_dict["email"] = record["email"]
                        if make_dict not in all_login_data:
                            make_dict = {}
                            make_dict["photo_link"] = record["photo_link"]
                            make_dict["teacher_id"] = str(record["teacher_id"])
                            make_dict["username"] = record["username"]
                            make_dict["email"] = record["email"]
                            if "scrypt" not in record["password"]:
                                make_dict["password"] = generate_password_hash(password=record["password"])
                            else:
                                make_dict["password"] = record["password"]
                            make_dict["type"] = "teacher"
                            data_added(app, db, "login_mapping", make_dict)
                            coll_chats = db["chats_mapping"]
                            chats_dict = {}
                            chats_dict["username"] = record["username"]
                            chats_dict["photo_link"] = record["photo_link"]
                            chats_dict["user_mapping_dict"] = []
                            chats_dict["status"] = "inactive"
                            chats_dict["type"] = "teacher"
                            chats_dict["last_seen"] = get_timestamp(app)
                            coll_chats.insert_one(chats_dict)
                            subject_mapping_dict = {}
                            subject_mapping_dict["teacher_id"] = str(record["teacher_id"])
                            subject_mapping_dict["department_name"] = record["department"]
                            subject_mapping_dict["subject"] = record["subject"]
                            subject_mapping_dict["type"] = "teacher"
                            coll_subject = db["subject_mapping"]
                            coll_subject.insert_one(subject_mapping_dict)
                            coll = db["teacher_data"]
                            record["photo_link"] = record["photo_link"]
                            if "scrypt" not in record["password"]:
                                record["password"] = generate_password_hash(password=record["password"])
                            coll.insert_one(record)
                            notification_dict = {}
                            notification_dict["teacher_id"] = str(record["teacher_id"])
                            notification_dict["username"] = str(record["username"])
                            notification_dict["notification_list"] = []
                            notification_dict["type"] = "teacher"
                            coll = db["notification_data"]
                            coll.insert_one(notification_dict)
                        else:
                            flag = True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "department":
                    get_department_data = find_all_data(app, db, "department_data")
                    all_department_data = []
                    for department_data in get_department_data:
                        di = {}
                        di["department_id"] = str(department_data["department_id"])
                        di["department_name"] = department_data["department_name"]
                        di["HOD_name"] = department_data["HOD_name"]
                        all_department_data.append(di)
                    for record in json_data:
                        record["department_id"] = str(record["department_id"])
                        make_dict = {}
                        make_dict["department_id"] = str(record["department_id"])
                        make_dict["department_name"] = record["department_name"]
                        make_dict["HOD_name"] = record["HOD_name"]
                        if make_dict not in all_department_data:
                            coll = db["department_data"]
                            coll.insert_one(record)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "homework":
                    get_homework_data = find_all_data(app, db, "homework_data")
                    all_homework_data = []
                    for homework_data in get_homework_data:
                        di = {}
                        di["title"] = str(homework_data["title"])
                        di["description"] = homework_data["description"]
                        di["department_name"] = homework_data["department_name"]
                        di["class_name"] = homework_data["class_name"]
                        di["subject_name"] = homework_data["subject_name"]
                        all_homework_data.append(di)
                    for record in json_data:
                        make_dict = {}
                        make_dict["title"] = str(record["title"])
                        make_dict["description"] = record["description"]
                        make_dict["department_name"] = record["department_name"]
                        make_dict["class_name"] = record["class_name"]
                        make_dict["subject_name"] = record["subject_name"]
                        if make_dict not in all_homework_data:
                            coll = db["homework_data"]
                            coll.insert_one(record)
                            make_dict["attechment_filepath_list"] = []
                            make_dict["comments"] = []
                            make_dict["inserted_on"] = get_timestamp(app=app)
                            make_dict["updated_on"] = get_timestamp(app=app)
                            coll1 = db["homework_comment"]
                            coll1.insert_one(make_dict)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "subject":
                    get_subject_data = find_all_data(app, db, "subject_data")
                    all_subject_data = []
                    for subject_data in get_subject_data:
                        di = {}
                        di["subject_name"] = subject_data["subject_name"]
                        di["department_name"] = subject_data["department_name"]
                        all_subject_data.append(di)
                    for record in json_data:
                        record["subject_id"] = str(record["subject_id"])
                        make_dict = {}
                        make_dict["subject_name"] = record["subject_name"]
                        make_dict["department_name"] = record["department_name"]
                        coll = db["subject_data"]
                        if make_dict not in all_subject_data:
                            coll.insert_one(record)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "attendance":
                    get_attendance_data = find_all_data(app, db, "attendance_data")
                    all_attendance_data = []
                    for attendance_data in get_attendance_data:
                        di = {}
                        di["student"] = attendance_data["student"]
                        di["department"] = attendance_data["department"]
                        di["class_name"] = attendance_data["class_name"]
                        di["teacher_name"] = attendance_data["teacher_name"]
                        di["attendance_date"] = attendance_data["attendance_date"]
                        di["status"] = attendance_data["status"]
                        all_attendance_data.append(di)
                    for record in json_data:
                        make_dict = {}
                        make_dict["student"] = record["student"]
                        make_dict["department"] = record["department"]
                        make_dict["class_name"] = record["class_name"]
                        make_dict["teacher_name"] = record["teacher_name"]
                        make_dict["attendance_date"] = record["attendance_date"]
                        make_dict["status"] = record["status"]
                        coll = db["attendance_data"]
                        if make_dict not in all_attendance_data:
                            coll.insert_one(record)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')
                elif panel_obj == "event":
                    get_event_data = find_all_data(app, db, "event_data")
                    all_event_data = []
                    for event_data in get_event_data:
                        di = {}
                        di["event_name"] = event_data["event_name"]
                        di["event_date"] = event_data["event_date"]
                        di["event_time"] = event_data["event_time"]
                        di["teacher_name"] = event_data["teacher_name"]
                        di["classes"] = event_data["classes"]
                        di["event_description"] = event_data["event_description"]
                        all_event_data.append(di)
                    for record in json_data:
                        make_dict = {}
                        make_dict["event_name"] = record["event_name"]
                        make_dict["event_date"] = record["event_date"]
                        make_dict["event_time"] = record["event_time"]
                        make_dict["teacher_name"] = record["teacher_name"]
                        make_dict["classes"] = record["classes"]
                        make_dict["event_description"] = record["event_description"]
                        coll = db["event_data"]
                        if make_dict not in all_event_data:
                            coll.insert_one(record)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/admin/{panel_obj}_data')

            else:
                flash("No file selected, please select a file", "danger")
            return render_template(f'{panel_obj}s.html')

    except Exception as e:
        app.logger.debug(f"Error in export data from database: {e}")
        print(f"Error in export data from database: {e}")
        flash("Please try again...", "danger")
        if panel_obj == "class":
            return render_template(f'{panel_obj}es.html')
        return render_template(f'{panel_obj}s.html')

@app.route("/admin/search/<object>", methods=["GET", "POST"])
@token_required
def search_data(object):
    """
    That funcation can use search data from student, teacher and admin from admin panel
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method=="POST":
            textdata = request.form["textdata"]
            if object=="userpanel":
                htmlfilename = f"admins/user_panel.html"
                coll = db["user_data"]
                field_list = constant_data.get("show_data_dict", {}).get("user_data", [])
                all_coll_data = coll.find({})
                all_coll_data = list(all_coll_data)
                coll_all_data = []
                for coll_data in all_coll_data:
                    values_data = []
                    del coll_data["_id"]
                    for field in field_list:
                        values_data.append(coll_data[field])
                    del coll_data["photo_link"]
                    coll_data_list = list(coll_data.values())
                    for data_list in coll_data_list:
                        if textdata.lower() in data_list.lower():
                            coll_all_data.append(values_data)
                            break

                all_values = coll_all_data
                all_keys = []
            elif object=="students":
                htmlfilename = f"admins/{object}.html"
                coll = db["user_data"]
                all_coll_data = coll.find({})
                all_coll_data = list(all_coll_data)
                coll_all_data = []
                for coll_data in all_coll_data:
                    del coll_data["_id"]
                    coll_data_list = list(coll_data.values())
                    for data_list in coll_data_list:
                        if textdata.lower() in data_list.lower():
                            coll_all_data.append(coll_data)
                            break

                all_values = coll_all_data
                all_keys = []

            elif object=="subject":
                htmlfilename = "admins/subjects.html"
                condition_dict = {}
                subject_name = request.form["subject_name"]
                department_name = request.form["department_name"]
                if subject_name:
                    condition_dict["subject_name"] = subject_name
                if department_name:
                    condition_dict["department_name"] = department_name

                all_keys, all_values = get_search_data(app, client, "college_management", "subject_data",
                                                       condition_dict)

            if all_keys and all_values:
                return render_template(htmlfilename, all_keys=all_keys[0], all_values=all_values,
                                       type=type, admin_id=id, photo_link=photo_link, search="true")
            else:
                return render_template(htmlfilename, all_keys=all_keys, all_values=all_values,
                                       type=type, admin_id=id, photo_link=photo_link, search="true")

    except Exception as e:
        app.logger.debug(f"Error in searching data from database: {e}")
        print("Error in searching data", e)
        flash("Please try again...", "danger")
        panel = object
        if panel == "class":
            return render_template(f'{panel}es.html')
        return render_template(f'{panel}s.html')

@app.route("/admin/view_data", methods=["GET", "POST"])
@token_required
def view_data():
    """
    That funcation can use search data from student, teacher and admin from admin panel
    """
    try:
        data = json.loads(request.data)
        type = data["type"]
        db = client["college_management"]
        if type=="student":
            coll = db["user_data"]
            username = data["username"]
            all_data = coll.find({"username": username, "type": "student"})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["photo_link"] = all_data_main["photo_link"]
            all_type_data["student_id"] = all_data_main["student_id"]
            all_type_data["username"] = all_data_main["username"]
            all_type_data["first_name"] = all_data_main["first_name"]
            all_type_data["last_name"] = all_data_main["last_name"]
            all_type_data["dob"] = all_data_main["dob"]
            all_type_data["gender"] = all_data_main["gender"]
            all_type_data["contact_no"] = all_data_main["contact_no"]
            all_type_data["email"] = all_data_main["email"]
            all_type_data["address"] = all_data_main["address"]
            all_type_data["city"] = all_data_main["city"]
            all_type_data["province"] = all_data_main["province"]
            all_type_data["class_name"] = all_data_main["class_name"]
            all_type_data["batch_year"] = all_data_main["batch_year"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="admin":
            coll = db["user_data"]
            username = data["username"]
            all_data = coll.find({"username": username, "type": type})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["photo_link"] = all_data_main["photo_link"]
            all_type_data["admin_id"] = all_data_main["admin_id"]
            all_type_data["username"] = all_data_main["username"]
            all_type_data["first_name"] = all_data_main["first_name"]
            all_type_data["last_name"] = all_data_main["last_name"]
            all_type_data["gender"] = all_data_main["gender"]
            all_type_data["contact_no"] = all_data_main["contact_no"]
            all_type_data["email"] = all_data_main["email"]
            all_type_data["address"] = all_data_main["address"]
            all_type_data["city"] = all_data_main["city"]
            all_type_data["province"] = all_data_main["province"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="teacher":
            coll = db["user_data"]
            username = data["username"]
            all_data = coll.find({"username": username, "type": type})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["photo_link"] = all_data_main["photo_link"]
            all_type_data["teacher_id"] = all_data_main["teacher_id"]
            all_type_data["username"] = all_data_main["username"]
            all_type_data["first_name"] = all_data_main["first_name"]
            all_type_data["last_name"] = all_data_main["last_name"]
            all_type_data["dob"] = all_data_main["dob"]
            all_type_data["gender"] = all_data_main["gender"]
            all_type_data["contact_no"] = all_data_main["contact_no"]
            all_type_data["email"] = all_data_main["email"]
            all_type_data["address"] = all_data_main["address"]
            all_type_data["city"] = all_data_main["city"]
            all_type_data["province"] = all_data_main["province"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="subject":
            coll = db["subject_data"]
            username = data["username"]
            all_data = coll.find({"subject_name": username})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["subject_id"] = all_data_main["subject_id"]
            all_type_data["subject_name"] = all_data_main["subject_name"]
            all_type_data["department_name"] = all_data_main["department_name"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)

    except Exception as e:
        app.logger.debug(f"Error in view data from database: {e}")
        flash("Please try again...", "danger")
        return jsonify({"message": "none"})

def get_notifications(id):
    try:
        db = client["college_management"]
        coll_notification = db["notification_data"]
        notifications_dict = coll_notification.find({"username": id})
        notifications_dict = list(notifications_dict)
        if notifications_dict:
            all_notifications = notifications_dict[0]["notification_list"]
        else:
            all_notifications = []

        return all_notifications

    except Exception as e:
        app.logger.debug(f"Error in notification list: {e}")
        return []

@app.route("/admin_dashboard", methods=["GET", "POST"])
@token_required
def admin_dashboard():
    """
    That funcation can use show admin dashboard to admin user
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        if login_dict != "nothing":
            type = login_dict["type"]
            id = login_dict["id"]
            if login_dict["photo_link"]!="static/assets/img/profiles/avatar-01.jpg":
                photo_link = "/" + login_dict["photo_link"]
            else:
                photo_link = login_dict["photo_link"]
        else:
            type = "Anonymous"
            id = "anonymous"
            photo_link = "static/assets/img/profiles/avatar-01.jpg"
        db = client["college_management"]
        coll = db["event_data"]
        get_all_data = coll.find({}).sort("event_date")

        upcoming_event = []
        completed_list = []
        for data in get_all_data:
            del data["_id"]
            del data["inserted_on"]
            del data["updated_on"]
            date_str = f"{data['event_date']} {data['event_time']}"
            date_format = "%d-%m-%Y %H:%M"
            date = datetime.strptime(date_str, date_format)

            # Get the current datetime
            current_datetime = datetime.now()

            # Compare the parsed datetime with the current datetime
            if date < current_datetime:
                completed_list.append(data)
            else:
                upcoming_event.append(data)

        show_completed_list = completed_list[-2:]
        show_upcoming_list = upcoming_event[:2]
        coll_user = db["user_data"]
        admin_data = coll_user.find({"type": "admin"})
        admin_data = list(admin_data)
        admin_count = len(admin_data)
        student_data = coll_user.find({"type": "student"})
        student_data = list(student_data)
        student_count = len(student_data)
        teacher_data = coll_user.find({"type": "teacher"})
        teacher_data = list(teacher_data)
        teacher_count = len(teacher_data)
        all_user_data = coll_user.find({})
        all_user_names_list = [userdata["username"] for userdata in all_user_data]
        coll = db["event_data"]
        get_all_data = coll.find({})
        coll_atten = db["attendance_data"]
        today = datetime.now().date()
        dates_dict = {}
        for i in range(7):
            date = today - timedelta(days=i)
            formatted_date = date.strftime("%d-%m-%Y")
            atten_student_data = coll_atten.find({"attendance_date": formatted_date, "status": "P"})
            atten_student_data_list = list(atten_student_data)
            dates_dict[formatted_date] = len(atten_student_data_list)
        all_date_key = list(dates_dict.keys())
        all_date_values = list(dates_dict.values())
        event_count = len(list(get_all_data))
        count_mapping = [admin_count, student_count, teacher_count]

        return render_template("admins/index.html", all_date_key=all_date_key,all_date_values=all_date_values, all_notifications=get_notifications(id), admin_id=id, type=type, photo_link=photo_link,
                           student_count=student_count, all_user_names_list=all_user_names_list, admin_count=admin_count, teacher_count=teacher_count,
                           event_count=event_count, count_mapping=count_mapping, upcoming_event=show_upcoming_list,completed_list=show_completed_list)

    except Exception as e:
        app.logger.debug(f"Error in admin dashboard route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('admin_dashboard', _external=True, _scheme=secure_type))

@app.route("/admin/admin_data", methods=["GET", "POST"])
@token_required
def admin_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        # set all dynamic variable value
        allcity = list(constant_data.get("city_province_mapping", {}).keys())
        allstate, allcountrycode = get_all_country_state_names(app)
        all_keys, all_values = get_admin_data(app, client, "college_management", "admin_data")

        if all_keys and all_values:
            return render_template("admins/admins.html", all_notifications=get_notifications(id), allcity=allcity, allstate=allstate, allcountrycode=allcountrycode,
                                all_keys=all_keys[0], all_values=all_values, type=type, admin_id=id,
                                photo_link=photo_link)
        else:
            return render_template("admins/admins.html", all_notifications=get_notifications(id), all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                photo_link=photo_link,allcity=allcity,allstate=allstate, allcountrycode=allcountrycode)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))

@app.route("/admin/user_panel", methods=["GET", "POST"])
@token_required
def user_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        # set all dynamic variable value
        allcity = list(constant_data.get("city_province_mapping", {}).keys())
        allstate, allcountrycode = get_all_country_state_names(app)
        all_keys, all_values = get_admin_data(app, client, "college_management", "user_data")

        if all_keys and all_values:
            return render_template("admins/user_panel.html", all_notifications=get_notifications(id), allcity=allcity, allstate=allstate, allcountrycode=allcountrycode,
                                all_keys=all_keys[0], all_values=all_values, type=type, admin_id=id,
                                photo_link=photo_link)
        else:
            return render_template("admins/user_panel.html", all_notifications=get_notifications(id), all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                photo_link=photo_link,allcity=allcity,allstate=allstate, allcountrycode=allcountrycode)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('user_data_list', _external=True, _scheme=secure_type))

@app.route("/admin/filter/<datafilter>", methods=["GET", "POST"])
@token_required
def filter_data(datafilter):
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        spliting = datafilter.split("-")
        if spliting[0] == "admin":
            coll_name = "admin_data"
            filename = "admins/admins.html"
        elif spliting[0] == "classes":
            coll_name = "class_data"
            filename = "admins/classes.html"
        elif spliting[0] == "student":
            coll_name = "students_data"
            filename = "admins/students.html"
        elif spliting[0] == "teacher":
            coll_name = "teacher_data"
            filename = "admins/teachers.html"
        elif spliting[0] == "department":
            coll_name = "department_data"
            filename = "admins/departments.html"
        elif spliting[0] == "event":
            coll_name = "event_data"
            filename = "admins/event.html"
        elif spliting[0] == "subject":
            coll_name = "subject_data"
            filename = "admins/subjects.html"
        elif spliting[0] == "homework":
            coll_name = "homework_data"
            filename = "admins/homework.html"
        elif spliting[0] == "feedback":
            coll_name = "feedback_data"
            filename = "admins/feedback.html"
        all_keys, all_values = get_filtered_admin_data(app, client, "college_management", coll_name, spliting[1])
        if all_keys and all_values:
            return render_template(filename, all_keys=all_keys[0], all_notifications=get_notifications(id),all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)
        else:
            return render_template(filename, all_keys=all_keys, all_notifications=get_notifications(id), all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))

@app.route("/student/student_profile", methods=["GET", "POST"])
@token_required
def student_profile():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        search_dict = get_profile_data(app, client, "college_management", type)
        return render_template("student/student_profile.html", all_notifications=get_notifications(id), search_dict=search_dict, type=type, student_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show student data in profile section: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('student_profile', _external=True, _scheme=secure_type))

@app.route("/admin/admin_profile", methods=["GET", "POST"])
@token_required
def admin_profile():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["user_data"]
        get_admin_data = coll.find({"username": id})
        get_admin_data = list(get_admin_data)
        return render_template("admins/admin_profile.html", all_notifications=get_notifications(id), search_dict=get_admin_data, type=type, student_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show student data in profile section: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('admin_profile', _external=True, _scheme=secure_type))

@app.route("/teacher/teacher_profile", methods=["GET", "POST"])
@token_required
def teacher_profile():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        search_dict = get_profile_data(app, client, "college_management", type)
        return render_template("teacher_profile.html", all_notifications=get_notifications(id), search_dict=search_dict, type=type, teacher_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show student data in profile section: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('teacher_profile', _external=True, _scheme=secure_type))


@app.route("/admin/add_admin", methods=["GET", "POST"])
@token_required
def add_admin():
    """
    In this route we can handling admin register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_main_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        # set all dynamic variable value
        allcity = list(constant_data.get("city_province_mapping", {}).keys())
        allstate, allcountrycode = get_all_country_state_names(app)

        if request.method == "POST":
            photo_link = request.files["photo_link"]
            first_name = request.form["first_name"]
            last_name = request.form["last_name"]
            username = request.form["username"]
            password = request.form["password"]
            gender = request.form["gender"]
            countrycode = request.form["countrycode"]
            contact_no = request.form["contact_no"]
            emergency_contact_no = request.form["emer_contact_no"]
            email = request.form["email"]
            address = request.form["address"]
            city = request.form["city"]
            province = request.form["province"]
            new_contact_no = countrycode + " " + contact_no
            new_emergency_contact_no = countrycode + " " + emergency_contact_no
            image_file_name = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], photo_link.filename)
            spliting_email = email.split("@")[-1]
            if "." not in spliting_email:
                flash("Email is not valid...", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city,
                                       province=province,image_url=image_file_name,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            if not first_name.isalpha():
                flash("Firstname allows only character...", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city,
                                       province=province,image_url=image_file_name,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            if not last_name.isalpha():
                flash("Lastname allows only character...", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city,
                                       province=province,image_url=image_file_name,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)
            # username validation
            all_admin_data = find_all_data(app, db, "login_mapping")
            get_all_username = [ad_data["username"] for ad_data in all_admin_data]
            if username in get_all_username:
                flash("Username already exits. Please try with different Username", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            all_admin_data = find_all_data(app, db, "login_mapping")
            get_all_email = [ad_data["email"] for ad_data in all_admin_data]
            if email in get_all_email:
                flash("Email already exits. Please try with different Email..", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            # password validation
            if not password_validation(app=app, password=password):
                flash("Please choose strong password. Add at least 1 special character, number, capitalize latter..", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            # contact number and emergency contact number validation
            get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
            get_emergency_phone_val = validate_phone_number(app, emergency_contact_no)
            if get_phone_val == "invalid number":
                flash("Please enter correct contact no.", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            if get_emergency_phone_val == "invalid number":
                flash("Please enter correct emergency contact no.", "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            if 'photo_link' not in request.files:
                flash('No file part', "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            if photo_link.filename == '':
                flash('No image selected for uploading', "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,image_url=image_file_name,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            if photo_link and allowed_photos(photo_link.filename):
                filename = secure_filename(username + ".jpg")
                ans = checking_upload_folder(app=app, filename=filename)
                if ans != "duplicate":
                    photo_link.save(os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename))
                    photo_path = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename)
                else:
                    flash('This filename already exits', "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,key="add",
                                           first_name=first_name, last_name=last_name, username=username,
                                           password=password, all_notifications=get_notifications(admin_id),
                                           gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city,
                                           province=province, type=type, photo_link=photo_main_link, admin_id=admin_id)
            else:
                flash('This file format is not supported.....', "danger")
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate,key="add", all_notifications=get_notifications(admin_id),
                                       first_name=first_name, last_name=last_name, username=username, password=password,
                                       gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                       email=email, address=address, countrycode=countrycode, city=city, province=province,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)

            password = generate_password_hash(password)
            # admin-id validation
            all_admin_data = find_spec_data(app, db, "login_mapping", {"type": "admin"})
            get_all_admin_id = [ad_data["admin_id"] for ad_data in all_admin_data]
            unique_admin_id = get_unique_admin_id(app, get_all_admin_id)
            register_dict = {
                "photo_link": photo_path,
                "admin_id": unique_admin_id,
                "username": username,
                "first_name": first_name,
                "last_name": last_name,
                "password": password,
                "gender": gender,
                "contact_no": new_contact_no,
                "emergency_contact_no": new_emergency_contact_no,
                "email": email,
                "address": address,
                "city": city,
                "province": province,
                "type": "admin",
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            admin_mapping_dict = {}
            admin_mapping_dict["photo_link"] = photo_path
            admin_mapping_dict["admin_id"] = str(unique_admin_id)
            admin_mapping_dict["username"] = username
            admin_mapping_dict["email"] = email
            admin_mapping_dict["password"] = password
            admin_mapping_dict["type"] = "admin"
            notification_dict = {}
            notification_dict["admin_id"] = str(unique_admin_id)
            notification_dict["username"] = username
            notification_dict["notification_list"] = []
            notification_dict["type"] = "admin"
            chats_dict = {}
            chats_dict["username"] = username
            chats_dict["photo_link"] = photo_path
            chats_dict["user_mapping_dict"] = []
            chats_dict["status"] = "inactive"
            chats_dict["type"] = "admin"
            chats_dict["last_seen"] = get_timestamp(app)
            data_added(app, db, "chats_mapping", chats_dict)
            data_added(app, db, "notification_data", notification_dict)
            data_added(app, db, "admin_data", register_dict)
            get_login_data = find_all_data(app, db, "login_mapping")
            all_login_data = []
            for login_data in get_login_data:
                del login_data["_id"]
                all_login_data.append({"email":login_data["email"], "username": login_data["username"]})
            if [admin_mapping_dict["email"], admin_mapping_dict["username"]] not in all_login_data:
                data_added(app, db, "login_mapping", admin_mapping_dict)

            flash("Data Added Successfully", "success")
            return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))
        else:
            return render_template("admins/add-admin.html", key="add", all_notifications=get_notifications(admin_id), type=type, photo_link=photo_main_link, admin_id=admin_id,
                                   allcountrycode=allcountrycode, allcity=allcity, allstate=allstate)

    except Exception as e:
        app.logger.debug(f"Error in add admin data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_admin', _external=True, _scheme=secure_type))

@app.route("/admin/edit_data/<object>", methods=["GET", "POST"])
@token_required
def updated_data(object):
    """
    In this route we can handling admin register process
    :return: register template
    """
    try:
        data_split = object.split("|")
        panel = data_split[0]
        username_condition = data_split[1]
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_main_link = "/" + login_dict["photo_link"]

        db = client["college_management"]
        allcity = list(constant_data.get("city_province_mapping", {}).keys())
        allstate, allcountrycode = get_all_country_state_names(app)
        class_data = find_all_data(app, db, "class_data")
        allclass_data = [class_text["class_name"] for class_text in class_data]
        allclass = list(set(list(allclass_data)))
        allbatchyear = list(range(2000, 2025))
        allbatchyear = allbatchyear[::-1]
        allqualification = ["MBA", "ME", "BCA", "MCA", "BBA"]
        subject_data = find_all_data(app, db, "subject_data")
        allsubjects = [subject["subject_name"] for subject in subject_data]
        allsubjects = list(set(allsubjects))
        student_data = find_all_data(app, db, "students_data")
        allstudents = [student["username"] for student in student_data]


        if request.method == "POST":
            if panel=="admin":
                photo_link = request.files["photo_link"]
                first_name = request.form["first_name"]
                last_name = request.form["last_name"]
                username = request.form["username"]
                gender = request.form["gender"]
                countrycode = request.form["countrycode"]
                contact_no = request.form["contact_no"]
                emergency_contact_no = request.form["emer_contact_no"]
                email = request.form["email"]
                address = request.form["address"]
                city = request.form["city"]
                province = request.form["province"]
                new_contact_no = countrycode + " " + contact_no
                new_emergency_contact_no = countrycode + " " + emergency_contact_no
                image_file_name = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], photo_link.filename)
                spliting_email = email.split("@")[-1]
                if "." not in spliting_email:
                    flash("Email is not valid...", "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city,
                                           province=province,image_url=image_file_name,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                if not first_name.isalpha():
                    flash("Firstname allows only character...", "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city,
                                           province=province,image_url=image_file_name,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                if not last_name.isalpha():
                    flash("Lastname allows only character...", "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city,
                                           province=province,image_url=image_file_name,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)
                # username validation
                all_admin_data = find_all_data(app, db, "login_mapping")
                get_all_username = [ad_data["username"] for ad_data in all_admin_data]
                get_all_username.remove(username_condition)
                if username in get_all_username:
                    flash("Username already exits. Please try with different Username", "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,image_url=image_file_name,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city, province=province,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                # contact number and emergency contact number validation
                get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
                get_emergency_phone_val = validate_phone_number(app, emergency_contact_no)
                if get_phone_val == "invalid number":
                    flash("Please enter correct contact no.", "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,image_url=image_file_name,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city, province=province,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                if get_emergency_phone_val == "invalid number":
                    flash("Please enter correct emergency contact no.", "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,image_url=image_file_name,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city, province=province,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                if 'photo_link' not in request.files:
                    flash('No file part', "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,image_url=image_file_name,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city, province=province,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                if photo_link.filename == '':
                    flash('No image selected for uploading', "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,image_url=image_file_name,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city, province=province,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)

                if photo_link and allowed_photos(photo_link.filename):
                    filename = secure_filename("update_"+username + ".jpg")
                    ans = checking_upload_folder(app=app, filename=filename)
                    if ans != "duplicate":
                        photo_link.save(os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename))
                        photo_path = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename)
                    else:
                        flash('This filename already exits', "danger")
                        return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                               allstate=allstate,key="update", all_notifications=get_notifications(admin_id),
                                               first_name=first_name, last_name=last_name, username=username,
                                               gender=gender, contact_no=contact_no,
                                               emergency_contact_no=emergency_contact_no,
                                               email=email, address=address, countrycode=countrycode, city=city,
                                               province=province, type=type, photo_link=photo_main_link, admin_id=admin_id)
                else:
                    flash('This file format is not supported.....', "danger")
                    return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                           allstate=allstate,key="update", all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, username=username,
                                           gender=gender, contact_no=contact_no, emergency_contact_no=emergency_contact_no,
                                           email=email, address=address, countrycode=countrycode, city=city, province=province,
                                           type=type, photo_link=photo_main_link, admin_id=admin_id)


                register_dict = {
                    "photo_link": photo_path,
                    "username": username,
                    "first_name": first_name,
                    "last_name": last_name,
                    "gender": gender,
                    "contact_no": new_contact_no,
                    "emergency_contact_no": new_emergency_contact_no,
                    "email": email,
                    "address": address,
                    "city": city,
                    "province": province,
                    "updated_on": get_timestamp(app)
                }

                admin_mapping_dict = {}
                admin_mapping_dict["photo_link"] = photo_path
                admin_mapping_dict["username"] = username
                admin_mapping_dict["email"] = email

                notification_dict = {}
                notification_dict["username"] = username
                chats_dict = {}
                chats_dict["username"] = username

                update_mongo_data(app, db, "admin_data", {"username": username_condition}, register_dict)
                update_mongo_data(app, db, "login_mapping", {"username": username_condition}, admin_mapping_dict)
                update_mongo_data(app, db, "chats_mapping", {"username": username_condition}, chats_dict)
                update_mongo_data(app, db, "notification_data", {"username": username_condition}, notification_dict)

                flash("Data Updated Successfully...", "success")
                return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))
            elif panel=="student":
                photo_link = request.files["photo_link"]
                first_name = request.form["first_name"]
                last_name = request.form["last_name"]
                username = request.form["username"]
                dob = request.form["dob"]
                gender = request.form["gender"]
                countrycode = request.form["countrycode"]
                contact_no = request.form["contact_no"]
                emergency_contact_no = request.form["emer_contact_no"]
                email = request.form["email"]
                address = request.form["address"]
                city = request.form["city"]
                province = request.form["province"]
                admission_date = request.form["admission_date"]
                department = request.form["department"]
                classes = request.form["classes"]
                batch_year = request.form["batch_year"]
                new_contact_no = countrycode + " " + contact_no
                new_emergency_contact_no = countrycode + " " + emergency_contact_no

                spliting_email = email.split("@")[-1]
                if "." not in spliting_email:
                    flash("Email is not valid...", "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment, department=department,
                                           allstate=allstate, type=type, admin_id=admin_id, key="update",
                                           photo_link=photo_main_link, classes=classes,
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                if not first_name.isalpha():
                    flash("First name has only contain character", "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment, department=department,
                                           allstate=allstate, type=type, admin_id=admin_id, key="update",
                                           photo_link=photo_main_link, classes=classes,
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)
                if not last_name.isalpha():
                    flash("Last name has only contain character", "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment, department=department,
                                           allstate=allstate, type=type, admin_id=admin_id, key="update",
                                           photo_link=photo_main_link, classes=classes,
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)
                # username validation
                all_student_data = find_all_data(app, db, "students_data")
                get_all_username = [st_data["username"] for st_data in all_student_data]
                get_all_username.remove(username_condition)
                if username in get_all_username:
                    flash("Username already exits. Please try with different Username", "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment, department=department,
                                           allstate=allstate, type=type, admin_id=admin_id, key="update",
                                           photo_link=photo_main_link, classes=classes,
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                # contact number and emergency contact number validation
                get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
                get_emergency_phone_val = validate_phone_number(app, emergency_contact_no)
                if get_phone_val == "invalid number":
                    flash("Please enter correct contact no.", "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment,
                                           allstate=allstate, type=type, admin_id=admin_id, key="update",
                                           photo_link=photo_main_link, classes=classes, department=department,
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                if get_emergency_phone_val == "invalid number":
                    flash("Please enter correct emergency contact no.", "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment,
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           classes=classes, department=department, key="update",
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                if 'photo_link' not in request.files:
                    flash('No file part', "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment,
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, key="update",
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           classes=classes, department=department,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                if photo_link.filename == '':
                    flash('No image selected for uploading', "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment,
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, key="update",
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           classes=classes, department=department,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                if photo_link and allowed_photos(photo_link.filename):
                    filename = secure_filename("update_student_" + username + ".jpg")
                    ans = checking_upload_folder(app=app, filename=filename)
                    if ans != "duplicate":
                        photo_link.save(os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename))
                        photo_path = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename)
                    else:
                        flash('This filename already exits', "danger")
                        return render_template("admins/add-student.html", allcountrycode=allcountrycode,
                                               username=username, all_notifications=get_notifications(admin_id),
                                               allcity=allcity, alldepartment=alldepartment,
                                               allstate=allstate, type=type, admin_id=admin_id,
                                               photo_link=photo_main_link, key="update",
                                               allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                               countrycode=countrycode, city=city, province=province,
                                               classes=classes, department=department,
                                               dob=dob, gender=gender, contact_no=contact_no,
                                               emergency_contact_no=emergency_contact_no, email=email,
                                               batch_year=batch_year,
                                               address=address, admission_date=admission_date)
                else:
                    flash('This file format is not supported.....', "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment,
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, key="update",
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           countrycode=countrycode, city=city, province=province,
                                           classes=classes, department=department,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year, all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)

                register_dict = {
                    "photo_link": photo_path,
                    "username": username,
                    "first_name": first_name,
                    "last_name": last_name,
                    "dob": dob,
                    "gender": gender,
                    "contact_no": new_contact_no,
                    "emergency_contact_no": new_emergency_contact_no,
                    "email": email,
                    "address": address,
                    "city": city,
                    "province": province,
                    "admission_date": admission_date,
                    "classes": classes,
                    "department": department,
                    "batch_year": batch_year,
                    "updated_on": get_timestamp(app)
                }

                student_mapping_dict = {}
                student_mapping_dict["photo_link"] = photo_path
                student_mapping_dict["username"] = username
                student_mapping_dict["email"] = email
                classes_mapping_dict = {}
                classes_mapping_dict["department"] = department
                classes_mapping_dict["class_name"] = classes

                notification_dict = {}
                notification_dict["username"] = username
                chats_dict = {}
                chats_dict["username"] = username

                update_mongo_data(app, db, "class_data", {"username": username_condition}, classes_mapping_dict)
                update_mongo_data(app, db, "students_data", {"username": username_condition}, register_dict)
                update_mongo_data(app, db, "login_mapping", {"username": username_condition}, student_mapping_dict)
                update_mongo_data(app, db, "chats_mapping", {"username": username_condition}, chats_dict)
                update_mongo_data(app, db, "notification_data", {"username": username_condition}, notification_dict)
                flash("Data Updated Successfully", "success")
                return redirect(url_for('student_data_list', _external=True, _scheme=secure_type))
            elif panel=="teacher":
                photo_link = request.files["photo_link"]
                first_name = request.form["first_name"]
                last_name = request.form["last_name"]
                username = request.form["username"]
                dob = request.form["dob"]
                gender = request.form["gender"]
                countrycode = request.form["countrycode"]
                contact_no = request.form["contact_no"]
                emergency_contact_no = request.form["emer_contact_no"]
                email = request.form["email"]
                address = request.form["address"]
                city = request.form["city"]
                province = request.form["province"]
                qualification = request.form["qualification"]
                department = request.form["department"]
                subject = request.form["subject"]
                joining_date = request.form["joining_date"]
                new_contact_no = countrycode + " " + contact_no
                new_emergency_contact_no = countrycode + " " + emergency_contact_no

                spliting_email = email.split("@")[-1]
                if "." not in spliting_email:
                    flash("Email is not valid...", "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name,dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                if not first_name.isalpha():
                    flash("First name only container character", "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity, all_notifications=get_notifications(admin_id),
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,key="update",
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode,
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)
                if not last_name.isalpha():
                    flash("Last name only container character", "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, all_notifications=get_notifications(admin_id),
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode,
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                all_teacher_data = find_all_data(app, db, "teacher_data")
                get_all_username = [te_data["username"] for te_data in all_teacher_data]
                get_all_username.remove(username_condition)
                if username in get_all_username:
                    flash("Username already exits. Please try with different Username", "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                # contact number and emergency contact number validation
                get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
                get_emergency_phone_val = validate_phone_number(app, emergency_contact_no)
                if get_phone_val == "invalid number":
                    flash("Please enter correct contact no.", "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                if get_emergency_phone_val == "invalid number":
                    flash("Please enter correct emergency contact no.", "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                if 'photo_link' not in request.files:
                    flash('No file part', "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                if photo_link.filename == '':
                    flash('No image selected for uploading', "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, all_notifications=get_notifications(admin_id),
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode,
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)

                if photo_link and allowed_photos(photo_link.filename):
                    filename = secure_filename("update_teacher_" + username + ".jpg")
                    ans = checking_upload_folder(app=app, filename=filename)
                    if ans != "duplicate":
                        photo_link.save(os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename))
                        photo_path = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename)
                    else:
                        flash('This filename already exits', "danger")
                        return render_template("admins/add-teacher.html", username=username,
                                               allcountrycode=allcountrycode,
                                               allcity=allcity,key="update",
                                               allstate=allstate, type=type, admin_id=admin_id,
                                               photo_link=photo_main_link,
                                               allqualification=allqualification,
                                               allsubjects=allsubjects, alldepartment=alldepartment,
                                               countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                               first_name=first_name, last_name=last_name, dob=dob,
                                               gender=gender, contact_no=contact_no, city=city, province=province,
                                               emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                               address=address, joining_date=joining_date, qualification=qualification,
                                               department=department)
                else:
                    flash('This file format is not supported.....', "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="update",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link,
                                           allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode, all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)
                register_dict = {
                    "photo_link": photo_path,
                    "username": username,
                    "first_name": first_name,
                    "last_name": last_name,
                    "dob": dob,
                    "gender": gender,
                    "contact_no": new_contact_no,
                    "emergency_contact_no": new_emergency_contact_no,
                    "email": email,
                    "address": address,
                    "city": city,
                    "province": province,
                    "qualification": qualification,
                    "department": department,
                    "subject": subject,
                    "joining_date": joining_date,
                    "updated_on": get_timestamp(app)
                }

                teacher_mapping_dict = {}
                teacher_mapping_dict["photo_link"] = photo_path
                teacher_mapping_dict["username"] = username
                teacher_mapping_dict["email"] = email
                subject_mapping_dict = {}
                subject_mapping_dict["department_name"] = department
                subject_mapping_dict["subject"] = subject
                notification_dict = {}
                notification_dict["username"] = username
                chats_dict = {}
                chats_dict["username"] = username
                update_mongo_data(app, db, "chats_mapping", {"username": username_condition}, chats_dict)
                update_mongo_data(app, db, "notification_data", {"username": username_condition}, notification_dict)
                update_mongo_data(app, db, "subject_mapping", {"username": username_condition}, subject_mapping_dict)
                update_mongo_data(app, db, "teacher_data", {"username": username_condition}, register_dict)
                update_mongo_data(app, db, "login_mapping", {"username": username_condition}, teacher_mapping_dict)
                flash("Data updated Successfully", "success")
                return redirect(url_for('teacher_data_list', _external=True, _scheme=secure_type))
            elif panel=="department":
                hod_name = request.form["hod_name"]
                department_date = request.form["department_date"]
                department_name = request.form["department_name"]

                # department validation
                all_department_data = find_all_data(app, db, "department_data")
                get_all_department_name = [st_data["department_name"] for st_data in all_department_data]
                get_all_department_name.remove(username_condition)
                if department_name in get_all_department_name:
                    flash("Department name already exits. Please try with different Department name", "danger")
                    return render_template("admins/add-department.html", hod_name=hod_name,
                                           department_name=department_name,key="update",
                                           department_date=department_date, type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, all_notifications=get_notifications(admin_id))

                register_dict = {
                    "department_name": department_name,
                    "HOD_name": hod_name,
                    "updated_on": get_timestamp(app)
                }
                update_mongo_data(app, db, "department_data", {"department_name": username_condition}, register_dict)
                flash("Data Updated Successfully", "success")
                return redirect(url_for('department_data_list', _external=True, _scheme=secure_type))
            elif panel=="subject":
                subject_name = request.form["subject_name"]
                department = request.form["department"]
                subject_date = request.form["subject_date"]

                all_subject_data = find_spec_data(app, db, "subject_data",
                                                  {"subject_name": subject_name, "department_name": department})
                if len(list(all_subject_data)) != 0:
                    flash("Subject name already exits. Please try with different subject name", "danger")
                    return render_template("admins/add-subject.html", key="update", type=type, admin_id=admin_id,
                                           photo_link=photo_main_link, all_notifications=get_notifications(admin_id), alldepartment=alldepartment)

                register_dict = {
                    "subject_name": subject_name,
                    "department_name": department,
                    "subject_date": subject_date,
                    "updated_on": get_timestamp(app)
                }
                update_mongo_data(app, db, "subject_data", {"subject_name": username_condition, "department_name": department}, register_dict)
                flash("Data Updated Successfully", "success")
                return redirect(url_for('subject_data_list', _external=True, _scheme=secure_type))
            elif panel=="event":
                data = json.loads(request.data)
                event_name = data["event_name"]
                event_date = data["event_date"]
                event_time = data["event_time"]
                class_list = data["class_list"]
                event_description = data["event_description"]

                if is_date_passed(event_date):
                    flash("Please select correct date... That date is already passed...", "danger")
                    return render_template("admins/add-events.html", allclass=allclass, event_name=event_name,
                                           event_description=event_description, all_notifications=get_notifications(admin_id),
                                           event_date=event_date, event_time=event_time, type=type,
                                           photo_link=photo_main_link, admin_id=admin_id)
                else:
                    mapping_dict = {}
                    mapping_dict["event_name"] = event_name
                    mapping_dict["event_date"] = event_date
                    mapping_dict["event_time"] = event_time
                    mapping_dict["class"] = class_list
                    mapping_dict["event_description"] = event_description
                    mapping_dict["updated_on"] = get_timestamp(app)
                    update_mongo_data(app, db, "event_data", {"event_name": username_condition}, mapping_dict)

                    # for depart in department_list:
                    #     all_student_info = find_spec_data(app, db, "students_data", {"department": depart})
                    #     for student in all_student_info:
                    #         student_id = student.get("student_id")
                    #         username = student.get("username")
                    #         get_notification_data = find_spec_data(app, db, "notification_data",
                    #                                                {"student_id": student_id, "username": username})
                    #         get_notification_data = list(get_notification_data)
                    #         if get_notification_data:
                    #             notification_list = get_notification_data[0]["notification_list"]
                    #             notification_list.append({"msg": "New event added please check now......", "time": get_timestamp(app)})
                    #             update_mongo_data(app, db, "notification_data",
                    #                               {"student_id": student_id, "username": username},
                    #                               {"notification_list": notification_list})
                    #
                    #     all_teacher_info = find_spec_data(app, db, "teacher_data", {"department": depart})
                    #     for teacher in all_teacher_info:
                    #         teacher_id = teacher.get("teacher_id")
                    #         username = teacher.get("username")
                    #         get_notification_data = find_spec_data(app, db, "notification_data",
                    #                                                {"teacher_id": teacher_id, "username": username})
                    #         get_notification_data = list(get_notification_data)
                    #         if get_notification_data:
                    #             notification_list = get_notification_data[0]["notification_list"]
                    #             notification_list.append({"msg": "New event added please check now......", "time": get_timestamp(app)})
                    #             update_mongo_data(app, db, "notification_data",
                    #                               {"teacher_id": teacher_id, "username": username},
                    #                               {"notification_list": notification_list})
                    #
                    # all_admin_info = find_all_data(app, db, "admin_data")
                    # for admin in all_admin_info:
                    #     username = admin.get("username")
                    #     get_notification_data = find_spec_data(app, db, "notification_data",
                    #                                            {"username": username})
                    #     get_notification_data = list(get_notification_data)
                    #     if get_notification_data:
                    #         notification_list = get_notification_data[0]["notification_list"]
                    #         notification_list.append({"msg": "New event added please check now......", "time": get_timestamp(app)})
                    #         update_mongo_data(app, db, "notification_data",
                    #                           {"username": username},
                    #                           {"notification_list": notification_list})

                    return jsonify({"message": "done"})

        else:
            if panel=="admin":
                coll_data = db["admin_data"]
                data_username = coll_data.find({"username": username_condition})
                data_username = list(data_username)
                data_username = data_username[0]
                return render_template("admins/add-admin.html", allcountrycode=allcountrycode, allcity=allcity,
                                       allstate=allstate, key="update", all_notifications=get_notifications(admin_id),
                                       first_name=data_username.get("first_name"),
                                       last_name=data_username.get("last_name"),
                                       username=data_username.get("username"),
                                       password=data_username.get("password"),
                                       gender=data_username.get("gender"),
                                       contact_no=data_username.get("contact_no", "+31 9938928999").split(" ")[-1],
                                       emergency_contact_no=data_username.get("emergency_contact_no", "+31 9938928999").split(" ")[-1],
                                       email=data_username.get('email'),
                                       address=data_username.get("address"),
                                       countrycode=data_username.get("countrycode", "+31 9938928999").split(" ")[0],
                                       city=data_username.get("city"),
                                       province=data_username.get("province"), image_url=photo_main_link,
                                       type=type, photo_link=photo_main_link, admin_id=admin_id)
            elif panel == "student":
                coll_data = db["students_data"]
                data_username = coll_data.find({"username": username_condition})
                data_username = list(data_username)
                data_username = data_username[0]
                return render_template("admins/add-student.html",
                                       key="upadte",
                                       all_notifications=get_notifications(admin_id),
                                       allcountrycode=allcountrycode,
                                       username=data_username.get("username"),
                                       allcity=allcity,
                                       alldepartment=alldepartment,
                                       department=data_username.get("department"),
                                       allstate=allstate,
                                       type=type,
                                       student_id=data_username.get("student_id"),
                                       photo_link=photo_main_link,
                                       classes=data_username.get("classes"),
                                       allbatchyear=allbatchyear,
                                       first_name=data_username.get("first_name"),
                                       last_name=data_username.get("last_name"),
                                       countrycode=data_username.get("contact_no", "+31 9938928999").split(" ")[0],
                                       city=data_username.get("city"),
                                       province=data_username.get("province"),
                                       dob=data_username.get("dob"),
                                       gender=data_username.get("gender"),
                                       contact_no=data_username.get("contact_no", "+31 9938928999").split(" ")[-1],
                                       emergency_contact_no=data_username.get("emergency_contact_no", "+31 9938928999").split(" ")[-1],
                                       email=data_username.get("email"),
                                       batch_year=data_username.get("batch_year"),
                                       address=data_username.get("address"),
                                       admission_date=data_username.get("admission_date"))
            elif panel == "teacher":
                coll_data = db["teacher_data"]
                data_username = coll_data.find({"username": username_condition})
                data_username = list(data_username)
                data_username = data_username[0]
                return render_template("admins/add-teacher.html",
                                       key="upadte",admin_id=admin_id,
                                       allcountrycode=allcountrycode,
                                       username=data_username.get("username"),
                                       allcity=allcity,
                                       all_notifications=get_notifications(admin_id),
                                       alldepartment=alldepartment,
                                       allqualification = allqualification,
                                       allsubjects=allsubjects,
                                       department=data_username.get("department"),
                                       allstate=allstate,
                                       type=type,
                                       qualification=data_username.get("qualification"),
                                       subject=data_username.get("subject"),
                                       student_id=data_username.get("student_id"),
                                       photo_link=photo_main_link,
                                       classes=data_username.get("classes"),
                                       allbatchyear=allbatchyear,
                                       first_name=data_username.get("first_name"),
                                       last_name=data_username.get("last_name"),
                                       countrycode=data_username.get("contact_no", "+31 9938928999").split(" ")[0],
                                       city=data_username.get("city"),
                                       province=data_username.get("province"),
                                       dob=data_username.get("dob"),
                                       gender=data_username.get("gender"),
                                       contact_no=data_username.get("contact_no", "+31 9938928999").split(" ")[-1],
                                       emergency_contact_no=data_username.get("emergency_contact_no", "+31 9938928999").split(" ")[-1],
                                       email=data_username.get("email"),
                                       batch_year=data_username.get("batch_year"),
                                       address=data_username.get("address"),
                                       admission_date=data_username.get("admission_date"))
            elif panel == "department":
                coll_data = db["department_data"]
                data_username = coll_data.find({"department_name": username_condition})
                data_username = list(data_username)
                data_username = data_username[0]
                return render_template("admins/add-department.html",
                                       hod_name=data_username.get("HOD_name"),
                                       department_name=data_username.get("department_name"),
                                       department_date=data_username.get("department_date"),
                                       type=type,
                                       all_notifications=get_notifications(admin_id),
                                       key="update",
                                       admin_id=admin_id,
                                       photo_link=photo_main_link)
            elif panel == "subject":
                coll_data = db["subject_data"]
                data_username = coll_data.find({"subject_name": username_condition})
                data_username = list(data_username)
                data_username = data_username[0]

                return render_template("admins/add-subject.html",
                                       key="update",
                                       type=type,
                                       all_notifications=get_notifications(admin_id),
                                       admin_id=admin_id,
                                       subject_name = data_username.get("subject_name"),
                                       subject_date = data_username.get("subject_start_date"),
                                       department_name = data_username.get("department_name"),
                                       photo_link=photo_main_link,
                                       alldepartment=alldepartment)
            elif panel == "event":
                coll_data = db["event_data"]
                data_username = coll_data.find({"event_name": username_condition})
                data_username = list(data_username)
                data_username = data_username[0]
                event_name = data_username.get("event_name")
                event_date = data_username.get("event_date")
                event_time = data_username.get("event_time")
                event_description = data_username.get("event_description")

                return render_template("admins/edit-event.html",
                                       allclass=allclass,
                                       event_name=event_name,
                                       event_description=event_description,
                                       event_date=event_date,
                                       event_time=event_time,
                                       type=type,
                                       all_notifications=get_notifications(admin_id),
                                       photo_link=photo_main_link,
                                       admin_id=admin_id)

            return render_template("admins/add-admin.html", all_notifications=get_notifications(admin_id), key="add", type=type, photo_link=photo_main_link, admin_id=admin_id,
                                   allcountrycode=allcountrycode, allcity=allcity, allstate=allstate)

    except Exception as e:
        app.logger.debug(f"Error in add admin data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_admin', _external=True, _scheme=secure_type))


@app.route("/admin/student_data", methods=["GET", "POST"])
@token_required
def student_data_list():
    """
    That funcation can use show all students data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "students_data")
        if all_keys and all_values:
            return render_template("admins/students.html", all_notifications=get_notifications(id),type=type, admin_id=id, photo_link=photo_link, all_keys=all_keys[0],
                                   all_values=all_values)
        else:
            return render_template("admins/students.html",all_notifications=get_notifications(id), type=type, admin_id=id, photo_link=photo_link, all_keys=all_keys,
                                   all_values=all_values)

    except Exception as e:
        app.logger.debug(f"Error in student data list route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('student_data_list', _external=True, _scheme=secure_type))


@app.route("/admin/add_student", methods=["GET", "POST"])
@token_required
def add_student():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_student_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        allcity = list(constant_data.get("city_province_mapping", {}).keys())
        allstate, allcountrycode = get_all_country_state_names(app)
        allbatchyear = list(range(2000, 2025))
        allbatchyear = allbatchyear[::-1]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]

        if request.method == "POST":
            photo_link = request.files["photo_link"]
            first_name = request.form["first_name"]
            last_name = request.form["last_name"]
            username = request.form["username"]
            password = request.form["password"]
            dob = request.form["dob"]
            gender = request.form["gender"]
            countrycode = request.form["countrycode"]
            contact_no = request.form["contact_no"]
            emergency_contact_no = request.form["emer_contact_no"]
            email = request.form["email"]
            address = request.form["address"]
            city = request.form["city"]
            province = request.form["province"]
            admission_date = request.form["admission_date"]
            department = request.form["department"]
            classes = request.form["classes"]
            batch_year = request.form["batch_year"]
            new_contact_no = countrycode + " " + contact_no
            new_emergency_contact_no = countrycode + " " + emergency_contact_no

            spliting_email = email.split("@")[-1]
            if "." not in spliting_email:
                flash("Email is not valid...", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment, department=department,
                                       allstate=allstate, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link, classes=classes,
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       dob=dob, gender=gender, contact_no=contact_no,all_notifications=get_notifications(admin_id),
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            if not first_name.isalpha():
                flash("First name has only contain character", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment, department=department,
                                       allstate=allstate, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link, classes=classes,
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       dob=dob, gender=gender, contact_no=contact_no,all_notifications=get_notifications(admin_id),
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)
            if not last_name.isalpha():
                flash("Last name has only contain character", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment, department=department,
                                       allstate=allstate, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link, classes=classes,
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       dob=dob, gender=gender, contact_no=contact_no,all_notifications=get_notifications(admin_id),
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)
            # username validation
            all_student_data = find_all_data(app, db, "students_data")
            get_all_username = [st_data["username"] for st_data in all_student_data]
            if username in get_all_username:
                flash("Username already exits. Please try with different Username", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment, department=department,
                                       allstate=allstate, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link, classes=classes,all_notifications=get_notifications(admin_id),
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            # password validation
            if not password_validation(app=app, password=password):
                flash("Please choose strong password. Add at least 1 special character, number, capitalize latter..", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment,all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link, classes=classes, department=department,
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            # contact number and emergency contact number validation
            get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
            get_emergency_phone_val = validate_phone_number(app, emergency_contact_no)
            if get_phone_val == "invalid number":
                flash("Please enter correct contact no.", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment,all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link, classes=classes, department=department,
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            if get_emergency_phone_val == "invalid number":
                flash("Please enter correct emergency contact no.", "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment,
                                       allstate=allstate, type=type, admin_id=admin_id,
                                       photo_link=photo_student_link,all_notifications=get_notifications(admin_id),
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       classes=classes, department=department,key="add",
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            if 'photo_link' not in request.files:
                flash('No file part', "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment,
                                       allstate=allstate, type=type, admin_id=admin_id,
                                       photo_link=photo_student_link,key="add",all_notifications=get_notifications(admin_id),
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       classes=classes, department=department,
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            if photo_link.filename == '':
                flash('No image selected for uploading', "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment,
                                       allstate=allstate, type=type, admin_id=admin_id,
                                       photo_link=photo_student_link,key="add",all_notifications=get_notifications(admin_id),
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       classes=classes, department=department,
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            if photo_link and allowed_photos(photo_link.filename):
                filename = secure_filename("student_" + username + ".jpg")
                ans = checking_upload_folder(app=app, filename=filename)
                if ans != "duplicate":
                    photo_link.save(os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename))
                    photo_path = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename)
                else:
                    flash('This filename already exits', "danger")
                    return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                           allcity=allcity, alldepartment=alldepartment,
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_student_link,key="add",
                                           allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                           password=password, countrycode=countrycode, city=city, province=province,
                                           classes=classes, department=department,
                                           dob=dob, gender=gender, contact_no=contact_no,
                                           emergency_contact_no=emergency_contact_no, email=email,
                                           batch_year=batch_year,all_notifications=get_notifications(admin_id),
                                           address=address, admission_date=admission_date)
            else:
                flash('This file format is not supported.....', "danger")
                return render_template("admins/add-student.html", allcountrycode=allcountrycode, username=username,
                                       allcity=allcity, alldepartment=alldepartment,
                                       allstate=allstate, type=type, admin_id=admin_id,
                                       photo_link=photo_student_link,key="add",
                                       allbatchyear=allbatchyear, first_name=first_name, last_name=last_name,
                                       password=password, countrycode=countrycode, city=city, province=province,
                                       classes=classes, department=department,all_notifications=get_notifications(admin_id),
                                       dob=dob, gender=gender, contact_no=contact_no,
                                       emergency_contact_no=emergency_contact_no, email=email, batch_year=batch_year,
                                       address=address, admission_date=admission_date)

            password = generate_password_hash(password)
            # student-id validation
            all_student_data = find_spec_data(app, db, "login_mapping", {"type": "student"})
            get_all_student_id = [st_data["student_id"] for st_data in all_student_data]
            unique_student_id = get_unique_student_id(app, get_all_student_id)
            register_dict = {
                "photo_link": photo_path,
                "student_id": str(unique_student_id),
                "username": username,
                "first_name": first_name,
                "last_name": last_name,
                "password": password,
                "dob": dob,
                "gender": gender,
                "contact_no": new_contact_no,
                "emergency_contact_no": new_emergency_contact_no,
                "email": email,
                "address": address,
                "city": city,
                "province": province,
                "admission_date": admission_date,
                "classes": classes,
                "department": department,
                "batch_year": batch_year,
                "type": "student",
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            student_mapping_dict = {}
            student_mapping_dict["photo_link"] = photo_path
            student_mapping_dict["student_id"] = str(unique_student_id)
            student_mapping_dict["username"] = username
            student_mapping_dict["email"] = email
            student_mapping_dict["password"] = password
            student_mapping_dict["type"] = "student"
            classes_mapping_dict = {}
            classes_mapping_dict["student_id"] = str(unique_student_id)
            classes_mapping_dict["department"] = department
            classes_mapping_dict["class_name"] = classes
            classes_mapping_dict["type"] = "student"
            notification_dict = {}
            notification_dict["student_id"] = str(unique_student_id)
            notification_dict["username"] = username
            notification_dict["notification_list"] = []
            notification_dict["type"] = "student"
            chats_dict = {}
            chats_dict["username"] = username
            chats_dict["photo_link"] = photo_path
            chats_dict["user_mapping_dict"] = []
            chats_dict["status"] = "inactive"
            chats_dict["type"] = "student"
            chats_dict["last_seen"] = get_timestamp(app)
            data_added(app, db, "chats_mapping", chats_dict)
            data_added(app, db, "class_data", classes_mapping_dict)
            data_added(app, db, "students_data", register_dict)
            data_added(app, db, "notification_data", notification_dict)
            data_added(app, db, "login_mapping", student_mapping_dict)
            flash("Data Added Successfully", "success")
            return redirect(url_for('student_data_list', _external=True, _scheme=secure_type))
        else:
            return render_template("admins/add-student.html", key="add", type=type, alldepartment=alldepartment, admin_id=admin_id,
                                   photo_link=photo_student_link, all_notifications=get_notifications(admin_id),allcountrycode=allcountrycode, allcity=allcity, allbatchyear=allbatchyear)

    except Exception as e:
        app.logger.debug(f"Error in add student data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_student', _external=True, _scheme=secure_type))


@app.route("/admin/teacher_data", methods=["GET", "POST"])
@token_required
def teacher_data_list():
    """
    That funcation can use show all students data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "teacher_data")
        if all_keys and all_values:
            return render_template("admins/teachers.html",all_notifications=get_notifications(id), all_keys=all_keys[0], all_values=all_values, type=type,
                                   admin_id=id, photo_link=photo_link)
        else:
            return render_template("admins/teachers.html",all_notifications=get_notifications(id), all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)


    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('teacher_data_list', _external=True, _scheme=secure_type))


@app.route("/admin/add_teacher", methods=["GET", "POST"])
@token_required
def add_teacher():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_teacher_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        allcity = list(constant_data.get("city_province_mapping", {}).keys())
        allstate, allcountrycode = get_all_country_state_names(app)
        allqualification = ["MBA", "ME", "BCA", "MCA", "BBA"]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]
        subject_data = find_all_data(app, db, "subject_data")
        allsubjects = [subject["subject_name"] for subject in subject_data]
        allsubjects = list(set(allsubjects))
        if request.method == "POST":
            photo_link = request.files["photo_link"]
            first_name = request.form["first_name"]
            last_name = request.form["last_name"]
            username = request.form["username"]
            password = request.form["password"]
            dob = request.form["dob"]
            gender = request.form["gender"]
            countrycode = request.form["countrycode"]
            contact_no = request.form["contact_no"]
            emergency_contact_no = request.form["emer_contact_no"]
            email = request.form["email"]
            address = request.form["address"]
            city = request.form["city"]
            province = request.form["province"]
            qualification = request.form["qualification"]
            department = request.form["department"]
            subject = request.form["subject"]
            joining_date = request.form["joining_date"]
            new_contact_no = countrycode + " " + contact_no
            new_emergency_contact_no = countrycode + " " + emergency_contact_no

            spliting_email = email.split("@")[-1]
            if "." not in spliting_email:
                flash("Email is not valid...", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                       allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            if not first_name.isalpha():
                flash("First name only container character", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                       allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)
            if not last_name.isalpha():
                flash("Last name only container character", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                       allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            all_teacher_data = find_all_data(app, db, "teacher_data")
            get_all_username = [te_data["username"] for te_data in all_teacher_data]
            if username in get_all_username:
                flash("Username already exits. Please try with different Username", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                       allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            # password validation
            if not password_validation(app=app, password=password):
                flash("Please choose strong password. Add at least 1 special character, number, capitalize latter..", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                        allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            # contact number and emergency contact number validation
            get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
            get_emergency_phone_val = validate_phone_number(app, emergency_contact_no)
            if get_phone_val == "invalid number":
                flash("Please enter correct contact no.", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                        allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            if get_emergency_phone_val == "invalid number":
                flash("Please enter correct emergency contact no.", "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                        allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            if 'photo_link' not in request.files:
                flash('No file part', "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                        allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            if photo_link.filename == '':
                flash('No image selected for uploading', "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                        allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            if photo_link and allowed_photos(photo_link.filename):
                filename = secure_filename("teacher_" + username + ".jpg")
                ans = checking_upload_folder(app=app, filename=filename)
                if ans != "duplicate":
                    photo_link.save(os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename))
                    photo_path = os.path.join(app.config["PROFILE_UPLOAD_FOLDER"], filename)
                else:
                    flash('This filename already exits', "danger")
                    return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                           allcity=allcity,key="add",
                                           allstate=allstate, type=type, admin_id=admin_id,
                                           photo_link=photo_teacher_link,
                                            allqualification=allqualification,
                                           allsubjects=allsubjects, alldepartment=alldepartment,
                                           countrycode=countrycode,all_notifications=get_notifications(admin_id),
                                           first_name=first_name, last_name=last_name, password=password, dob=dob,
                                           gender=gender, contact_no=contact_no, city=city, province=province,
                                           emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                           address=address, joining_date=joining_date, qualification=qualification,
                                           department=department)
            else:
                flash('This file format is not supported.....', "danger")
                return render_template("admins/add-teacher.html", username=username, allcountrycode=allcountrycode,
                                       allcity=allcity,key="add",all_notifications=get_notifications(admin_id),
                                       allstate=allstate, type=type, admin_id=admin_id, photo_link=photo_teacher_link,
                                        allqualification=allqualification,
                                       allsubjects=allsubjects, alldepartment=alldepartment, countrycode=countrycode,
                                       first_name=first_name, last_name=last_name, password=password, dob=dob,
                                       gender=gender, contact_no=contact_no, city=city, province=province,
                                       emergency_contact_no=emergency_contact_no, email=email, subject=subject,
                                       address=address, joining_date=joining_date, qualification=qualification,
                                       department=department)

            password = generate_password_hash(password)
            # teacher id validation
            all_teacher_data = find_spec_data(app, db, "login_mapping", {"type": "teacher"})
            get_all_teacher_id = [ta_data["teacher_id"] for ta_data in all_teacher_data]
            unique_teacher_id = get_unique_teacher_id(app, get_all_teacher_id)
            register_dict = {
                "photo_link": photo_path,
                "teacher_id": str(unique_teacher_id),
                "username": username,
                "first_name": first_name,
                "last_name": last_name,
                "password": password,
                "dob": dob,
                "gender": gender,
                "contact_no": new_contact_no,
                "emergency_contact_no": new_emergency_contact_no,
                "email": email,
                "address": address,
                "city": city,
                "province": province,
                "qualification": qualification,
                "department": department,
                "subject": subject,
                "joining_date": joining_date,
                "type": "teacher",
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            teacher_mapping_dict = {}
            teacher_mapping_dict["photo_link"] = photo_path
            teacher_mapping_dict["teacher_id"] = str(unique_teacher_id)
            teacher_mapping_dict["username"] = username
            teacher_mapping_dict["email"] = email
            teacher_mapping_dict["password"] = password
            teacher_mapping_dict["type"] = "teacher"
            subject_mapping_dict = {}
            subject_mapping_dict["teacher_id"] = str(unique_teacher_id)
            subject_mapping_dict["department_name"] = department
            subject_mapping_dict["subject"] = subject
            subject_mapping_dict["type"] = "teacher"
            notification_dict = {}
            notification_dict["teacher_id"] = str(unique_teacher_id)
            notification_dict["username"] = username
            notification_dict["notification_list"] = []
            notification_dict["type"] = "teacher"
            chats_dict = {}
            chats_dict["username"] = username
            chats_dict["photo_link"] = photo_path
            chats_dict["user_mapping_dict"] = []
            chats_dict["status"] = "inactive"
            chats_dict["type"] = "teacher"
            chats_dict["last_seen"] = get_timestamp(app)
            data_added(app, db, "chats_mapping", chats_dict)
            data_added(app, db, "notification_data", notification_dict)
            data_added(app, db, "subject_mapping", subject_mapping_dict)
            data_added(app, db, "teacher_data", register_dict)
            data_added(app, db, "login_mapping", teacher_mapping_dict)
            flash("Data Added Successfully", "success")

            return redirect(url_for('teacher_data_list', _external=True, _scheme=secure_type))
        else:
            return render_template("admins/add-teacher.html", allcountrycode=allcountrycode,
                                   allcity=allcity, allstate=allstate,key="add",all_notifications=get_notifications(admin_id),
                                   allqualification=allqualification, allsubjects=allsubjects,
                                   alldepartment=alldepartment, admin_id=admin_id, type=type, photo_link=photo_teacher_link)

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_teacher', _external=True, _scheme=secure_type))


@app.route("/admin/class_data", methods=["GET", "POST"])
@token_required
def class_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "class_data")
        if all_keys and all_values:
            return render_template("admins/classes.html",all_notifications=get_notifications(id), all_keys=all_keys[0], all_values=all_values, type=type,
                                   admin_id=id, photo_link=photo_link)
        else:
            return render_template("admins/classes.html",all_notifications=get_notifications(id), all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('class_data_list', _external=True, _scheme=secure_type))


@app.route("/admin/add_department", methods=["GET", "POST"])
@token_required
def add_department():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_student_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method == "POST":
            hod_name = request.form["hod_name"]
            department_date = request.form["department_date"]
            department_name = request.form["department_name"]

            # department validation
            all_department_data = find_all_data(app, db, "department_data")
            get_all_department_name = [st_data["department_name"] for st_data in all_department_data]
            if department_name in get_all_department_name:
                flash("Department name already exits. Please try with different Department name", "danger")
                return render_template("admins/add-department.html",all_notifications=get_notifications(admin_id), hod_name=hod_name, department_name=department_name,
                                       department_date=department_date, type=type, admin_id=admin_id,key="add",
                                       photo_link=photo_student_link)

            # department-id validation
            all_department_data = find_all_data(app, db, "department_data")
            get_all_department_id = [dt_data["department_id"] for dt_data in all_department_data]
            unique_department_id = get_unique_department_id(app, get_all_department_id)
            register_dict = {
                "department_id": unique_department_id,
                "department_name": department_name,
                "department_date": department_date,
                "HOD_name": hod_name,
                "type": "department",
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            data_added(app, db, "department_data", register_dict)
            flash("Data Added Successfully", "success")
            return redirect(url_for('department_data_list', _external=True, _scheme=secure_type))
        else:
            return render_template("admins/add-department.html", all_notifications=get_notifications(admin_id),key="add", type=type, admin_id=admin_id, photo_link=photo_student_link)

    except Exception as e:
        app.logger.debug(f"Error in add departpent data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_department', _external=True, _scheme=secure_type))


@app.route("/admin/subject_data", methods=["GET", "POST"])
@token_required
def subject_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "subject_data")
        if all_keys and all_values:
            return render_template("admins/subjects.html", all_keys=all_keys[0], all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)
        else:
            return render_template("admins/subjects.html", all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('department_data_list', _external=True, _scheme=secure_type))


@app.route("/admin/add_subject", methods=["GET", "POST"])
@token_required
def add_subject():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]
        if request.method == "POST":
            subject_name = request.form["subject_name"]
            department = request.form["department"]
            subject_date = request.form["subject_date"]

            all_subject_data = find_spec_data(app, db, "subject_data", {"subject_name": subject_name, "department_name": department})
            if len(list(all_subject_data))!=0:
                flash("Subject name already exits. Please try with different subject name", "danger")
                return render_template("admins/add-subject.html", key="add", type=type, admin_id=admin_id,
                                   photo_link=photo_link, alldepartment=alldepartment)

            # department-id validation
            all_subject_data = find_all_data(app, db, "subject_data")
            get_all_subject_id = [sub_data["subject_id"] for sub_data in all_subject_data]
            unique_subject_id = get_unique_subject_id(app, get_all_subject_id)
            register_dict = {
                "subject_id": unique_subject_id,
                "subject_name": subject_name,
                "department_name": department,
                "subject_start_date": subject_date,
                "type": "subject",
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            data_added(app, db, "subject_data", register_dict)
            flash("Data Added Successfully", "success")
            return redirect(url_for('subject_data_list', _external=True, _scheme=secure_type))
        else:
            return render_template("admins/add-subject.html", type=type, admin_id=admin_id,
                                   photo_link=photo_link,key="add", alldepartment=alldepartment)

    except Exception as e:
        app.logger.debug(f"Error in add subject data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_subject', _external=True, _scheme=secure_type))


@app.route("/update_data/<update_object>", methods=["GET", "POST"])
@token_required
def update_data(update_object):
    print(f"Object is : {update_object}")
    panel_id, panel_type = update_object.split("-")
    print(f"Panel Id is : {panel_id} and panel_type is : {panel_type}")
    form_data = request.form.to_dict()
    print(f"All data got from {panel_id} form is : {form_data}")
    try:
        print("Trying to update data in all related collections")
        db = client["college_management"]
        for key, value in form_data.items():
            print(f"Key is : {key} and value is : {value}")

            # db = client["college_management"]
            # result = import_data_into_database(app, db, panel_type, form_data)
            # print(f"Result is : {result}")

            # if result == "success":
            #     flash("Data updated successfully...", "success")
            #     return redirect(url_for(f'{panel_type}_data_list', _external=True, _scheme=secure_type))
            # else:
            #     flash("Please try again...", "danger")
            #     return redirect(url_for(f'edit_{panel_type}', _external=True, _scheme=secure_type))

        return redirect(url_for(f'{panel_type}_data_list', _external=True, _scheme=secure_type))
    except Exception as e:
        flash("Please try again...", "danger")
        return render_template(f'{panel_type}s.html')


# @app.route("/teacher/attendance_data", methods=["GET", "POST"])
# @token_required
# def attendance_data_list():
#     """
#     That funcation can use show all admins data from admin panel
#     """

#     try:
#         login_dict = session.get("login_dict", "nothing")
#         type = login_dict["type"]
#         id = login_dict["id"]
#         photo_link = "/" + login_dict["photo_link"]
#         all_keys, all_values = get_admin_data(app, client, "college_management", "attendance_data")
#         if all_keys and all_values:
#             return render_template("attendance.html", all_keys=all_keys[0], all_values=all_values, type=type, teacher_id=id,
#                                    photo_link=photo_link)
#         else:
#             return render_template("attendance.html", all_keys=all_keys, all_values=all_values, type=type, teacher_id=id,
#                                    photo_link=photo_link)

#     except Exception as e:
#         flash("Please try again...", "danger")
#         return redirect(url_for('attendance_data_list', _external=True, _scheme=secure_type))

@app.route("/admin/email_sending", methods=["GET", "POST"])
@token_required
def email_sending():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        class_data = find_all_data(app, db, "class_data")
        allclass_data = [class_text["class_name"] for class_text in class_data]
        allclass = list(set(list(allclass_data)))
        user_data = find_all_data(app, db, "user_data")
        alladmin, allstudent, allteacher = [], [], []
        for user_text in user_data:
            if user_text["type"] == "admin":
                alladmin.append(user_text["username"])
            elif user_text["type"] == "student":
                allstudent.append(user_text["username"])
            elif user_text["type"] == "teacher":
                allteacher.append(user_text["username"])
        allservertype = ["GMAIL", "OUTLOOK", "HOSTINGER", "YAHOO", "OTHER"]
        server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
        server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
        server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
        server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")

        if request.method == "POST":
            subject_title = request.form["topic"]
            mail_format = request.form["mail_format"]
            typemessage = request.form["type"]
            selected_data = request.form["selectlist"]

            selected_list = selected_data.split(",")
            attachment_all_file = []
            if 'file' in request.files:
                images = request.files.getlist("file")
                for file in images:
                    if file.filename != '':
                        attach_file_path = os.path.join("static/uploads/attach_file", file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mail_split = mail_format.split("\n")
            all_message_list = []
            for message in mail_split:
                if message != "\r":
                    message = message.replace("\r", "")
                    all_message_list.append(message)
            html_format = ""
            for msg in all_message_list:
                html_format = html_format + "<p>" + msg + "</p>"

            all_email_data = []
            if typemessage=="Whole School":
                user_mail_data = find_all_data(app, db, "user_data")
                all_email_data = [user_di["email"] for user_di in user_mail_data]
            elif typemessage=="Class":
                all_email_data = []
                for class_name in selected_list:
                    all_class_st_data = find_spec_data(app, db, "user_data", {"class_name": class_name})
                    for st_data in all_class_st_data:
                        all_email_data.append(st_data["email"])

                for class_name in selected_list:
                    all_class_te_data = find_spec_data(app, db, "class_data", {"class_name": class_name})
                    all_te_data = [te["teacher_name"] for te in all_class_te_data]
                    for te_data in all_te_data:
                        all_te_list = find_spec_data(app, db, "user_data", {"username": te_data})
                        for te_dict in all_te_list:
                            all_email_data.append(te_dict["email"])
            elif typemessage=="Students":
                all_email_data = []
                for st_data in selected_list:
                    all_st_data = find_spec_data(app, db, "user_data", {"username": st_data})
                    for st_dict in all_st_data:
                        all_email_data.append(st_dict["email"])
            elif typemessage=="Teachers":
                all_email_data = []
                for te_data in selected_list:
                    all_te_data = find_spec_data(app, db, "user_data", {"username": te_data})
                    for te_dict in all_te_data:
                        all_email_data.append(te_dict["email"])
            elif typemessage=="Admins":
                all_email_data = []
                for ad_data in selected_list:
                    all_ad_data = find_spec_data(app, db, "user_data", {"username": ad_data.lower()})
                    for ad_dict in all_ad_data:
                        all_email_data.append(ad_dict["email"])

            mapping_dict_main = app.config["email_configuration"].get(admin_id, {})
            if len(list(mapping_dict_main.keys())) != 0:
                server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
                server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
                server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
                server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")
                sending_email_mail(app, all_email_data, subject_title, mail_format, html_format, server_username,
                                   server_password, server_host, int(server_port), attachment_all_file)
                flash("Email_sent_successfully...", "danger")
                return {"message": "done"}
            else:
                flash("Please first setup your mail server configuration...", "danger")
                return {"message": "fail"}

        else:
            return render_template("admins/admin_email.html", allservertype=allservertype, allclass=allclass,
                               allteacher=allteacher, alladmin=alladmin, allstudent=allstudent, type=type, admin_id=admin_id, photo_link=photo_link,
                                   server_password=server_password, server_port=server_port, server_host=server_host, server_username=server_username)

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/teacher_mail"  , methods=["GET", "POST"])
@token_required
def teacher_mail():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method == "POST":
            teacher = request.form["teacher"]
            subject_title = request.form["topic"]
            mail_format = request.form["mail_format"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/attach_file")

            if teacher == "Select Teacher":
                flash("Please select correct teacher name...", "danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mail_split = mail_format.split("\n")
            all_message_list = []
            for message in mail_split:
                if message != "\r":
                    message = message.replace("\r", "")
                    all_message_list.append(message)
            html_format = ""
            for msg in all_message_list:
                html_format = html_format + "<p>" + msg + "</p>"

            condition_dict = {"username": teacher, "type": "teacher"}
            teacher_data = find_spec_data(app, db, "login_mapping", condition_dict)
            email_data = [tea["email"] for tea in teacher_data]
            mapping_dict_main = app.config["email_configuration"].get(admin_id, {})
            if len(list(mapping_dict_main.keys())) != 0:
                server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
                server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
                server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
                server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")
                sending_email_mail(app, email_data, subject_title, mail_format, html_format, server_username, server_password, server_host,
                                   int(server_port), attachment_all_file)
            else:
                flash("Please first setup your mail server configuration...", "danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            flash("Mail sent successfully...", "success")
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('teacher_mail', _external=True, _scheme=secure_type))

@app.route("/import_mail", methods=["GET", "POST"])
@token_required
def import_mail():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        if request.method == "POST":
            email_file = request.files.get("email_file")
            subject_title = request.form["topic"]
            mail_format = request.form["mail_format"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/attach_file")
            email_message = ""
            file_flag = False

            if 'email_file' not in request.files:
                flash("Please import file...", "danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            if email_file.filename == '':
                flash("Please import file...", "danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            filename = email_file.filename
            exten = filename.split(".")[-1]
            if exten not in ["csv", "xlsx", "json"]:
                flash("Please import only CSV, JSON & EXCEL file...","danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            if file_flag == False:
                email_file.save(os.path.join(app.config["IMPORT_UPLOAD_FOLDER"], filename))
                email_file_path = os.path.join(app.config["IMPORT_UPLOAD_FOLDER"], filename)
                if exten == "xlsx":
                    df = pd.read_excel(email_file_path)
                    if int(df.shape[0])==0:
                        file_flag = True
                        email_message = "Please add data into your file"
                elif exten == "csv":
                    df = pd.read_csv(email_file_path)
                    if int(df.shape[0]) == 0:
                        file_flag = True
                        email_message = "Please add data into your file"
                elif exten == "json":
                    with open(email_file_path, encoding='utf-8') as json_file:
                        json_data = json.load(json_file)
                    get_data = json_data.get("Emails", [])
                    if len(get_data)==0:
                        file_flag = True
                        email_message = "Please add data into your file"

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mail_split = mail_format.split("\n")
            all_message_list = []
            for message in mail_split:
                if message != "\r":
                    message = message.replace("\r", "")
                    all_message_list.append(message)
            html_format = ""
            for msg in all_message_list:
                html_format = html_format + "<p>" + msg + "</p>"

            if not file_flag:
                if exten=="csv":
                    get_all_mail = list(df["Email"])
                elif exten=="xlsx":
                    get_all_mail = list(df["Email"])
                elif exten=="json":
                    get_all_mail = get_data

                mapping_dict_main = app.config["email_configuration"].get(admin_id, {})
                if len(list(mapping_dict_main.keys())) != 0:
                    server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
                    server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
                    server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
                    server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")
                    sending_email_mail(app, get_all_mail, subject_title, mail_format, html_format, server_username,
                                       server_password, server_host, int(server_port), attachment_all_file)
                else:
                    flash("Please first setup your mail server configuration...", "danger")
                    server_host = app.config['MAIL_SERVER']
                    server_port = app.config['MAIL_PORT']
                    server_username = app.config['MAIL_USERNAME']
                    server_password = app.config['MAIL_PASSWORD']
                    sending_email_mail(app, get_all_mail, subject_title, mail_format, html_format, server_username,
                                       server_password, server_host, int(server_port), attachment_all_file)

                    return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            if email_message:
                flash(email_message, "danger")
            else:
                flash("Mail sent successfully...", "success")
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...","danger")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/department_mail", methods=["GET", "POST"])
@token_required
def department_mail():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]
        if request.method == "POST":
            classes = request.form["classes"]
            department = request.form["department"]
            subject_title = request.form["topic"]
            mail_format = request.form["mail_format"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/attach_file")
            email_message = ""
            department_flag = False
            class_flag = False

            if department == "Select Department":
                flash("Please select department first...","danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            if classes == "Select Class":
                flash("Please select class...","danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mail_split = mail_format.split("\n")
            all_message_list = []
            for message in mail_split:
                if message != "\r":
                    message = message.replace("\r", "")
                    all_message_list.append(message)
            html_format = ""
            for msg in all_message_list:
                html_format = html_format + "<p>" + msg + "</p>"

            if not class_flag:
                if not department_flag:
                    condition_dict = {"department": department, "classes": classes}
                    students_data = find_spec_data(app, db, "students_data", condition_dict)
                    email_data = [stu["email"] for stu in students_data]
                    if email_data:
                        mapping_dict_main = app.config["email_configuration"].get(admin_id, {})
                        if len(list(mapping_dict_main.keys())) != 0:
                            server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
                            server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
                            server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
                            server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")
                            sending_email_mail(app, email_data, subject_title, mail_format, html_format,
                                               server_username, server_password, server_host, int(server_port), attachment_all_file)
                        else:
                            email_message = "Please first setup your mail server configuration..."
                            server_host = app.config['MAIL_SERVER']
                            server_port = app.config['MAIL_PORT']
                            server_username = app.config['MAIL_USERNAME']
                            server_password = app.config['MAIL_PASSWORD']
                            sending_email_mail(app, email_data, subject_title, mail_format, html_format,
                                               server_username,
                                               server_password, server_host, int(server_port), attachment_all_file)
                    else:
                        email_message = "Student data is not available in class..."
                else:
                    email_message = "Please select department when are you select class"

            if email_message:
                flash(email_message,"danger")
            else:
                flash("Mail sent successfully...","success")
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))


    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...","danger")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/email_mail", methods=["GET", "POST"])
@token_required
def email_mail():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        if request.method == "POST":
            email = request.form["email"]
            subject_title = request.form["topic"]
            mail_format = request.form["mail_format"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/attach_file")
            email_message = ""
            email_flag = False

            if email == "":
                flash("Please enter valid email address...")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mail_split = mail_format.split("\n")
            all_message_list = []
            for message in mail_split:
                if message != "\r":
                    message = message.replace("\r", "")
                    all_message_list.append(message)
            html_format = ""
            for msg in all_message_list:
                html_format = html_format + "<p>" + msg + "</p>"

            if not email_flag:
                mapping_dict_main = app.config["email_configuration"].get(admin_id, {})
                if len(list(mapping_dict_main.keys())) != 0:
                    server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
                    server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
                    server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
                    server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")
                    sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                                       server_password, server_host,
                                       int(server_port), attachment_all_file)
                else:
                    flash("Please first setup your mail server configuration...", "danger")
                    return redirect(url_for('email_sending', _external=True, _scheme=secure_type))
                    server_host = app.config['MAIL_SERVER']
                    server_port = app.config['MAIL_PORT']
                    server_username = app.config['MAIL_USERNAME']
                    server_password = app.config['MAIL_PASSWORD']
                    sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                                       server_password, server_host, int(server_port), attachment_all_file)

            if email_message:
                flash(email_message,"danger")
            else:
                flash("Mail sent successfully...", "success")
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...","danger")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/student_mail", methods=["GET", "POST"])
@token_required
def student_mail():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method == "POST":
            student = request.form["student"]
            subject_title = request.form["topic"]
            mail_format = request.form["mail_format"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/attach_file")
            email_message = ""
            student_flag = False
            if student == "Select Student":
                flash("Please selete student...","danger")
                return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mail_split = mail_format.split("\n")
            all_message_list = []
            for message in mail_split:
                if message != "\r":
                    message = message.replace("\r", "")
                    all_message_list.append(message)
            html_format = ""
            for msg in all_message_list:
                html_format = html_format + "<p>" + msg + "</p>"

            if not student_flag:
                condition_dict = {"username": student, "type": "student"}
                student_data = find_spec_data(app, db, "login_mapping", condition_dict)
                email_data = [stu["email"] for stu in student_data]
                mapping_dict_main = app.config["email_configuration"].get(admin_id, {})
                if len(list(mapping_dict_main.keys())) != 0:
                    server_host = app.config["email_configuration"].get(admin_id, {}).get("server_host", "")
                    server_port = app.config["email_configuration"].get(admin_id, {}).get("server_port", "")
                    server_username = app.config["email_configuration"].get(admin_id, {}).get("server_username", "")
                    server_password = app.config["email_configuration"].get(admin_id, {}).get("server_password", "")
                    sending_email_mail(app, email_data, subject_title, mail_format, html_format, server_username,
                                       server_password, server_host,
                                       int(server_port), attachment_all_file)
                else:
                    flash("Please first setup your mail server configuration...", "danger")
                    server_host = app.config['MAIL_SERVER']
                    server_port = app.config['MAIL_PORT']
                    server_username = app.config['MAIL_USERNAME']
                    server_password = app.config['MAIL_PASSWORD']
                    sending_email_mail(app, email_data, subject_title, mail_format, html_format, server_username,
                                       server_password, server_host, int(server_port), attachment_all_file)
                    return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

            if email_message:
                flash(email_message,"danger")
            else:
                flash("Mail sent successfully...","success")
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        flash("Please try again...","danger")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/admin/add_event", methods=["GET", "POST"])
@token_required
def add_event():
    """
    In this route we can handling admin register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_main_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        class_data = find_all_data(app, db, "class_data")
        allclass_data = [class_text["class_name"] for class_text in class_data]
        allclass = list(set(list(allclass_data)))

        if request.method == "POST":
            data = json.loads(request.data)
            event_name = data["event_name"]
            event_date = data["event_date"]
            event_time = data["event_time"]
            class_list = data["class_list"]
            event_description = data["event_description"]

            if is_date_passed(event_date):
                flash("Please select correct date... That date is already passed...", "danger")
                return render_template("admins/add-events.html",allclass=allclass, event_name=event_name, event_description=event_description,
                                       event_date=event_date, event_time=event_time, type=type, photo_link=photo_main_link, admin_id=admin_id)
            else:
                mapping_dict = {}
                mapping_dict["event_name"] = event_name
                mapping_dict["event_date"] = event_date
                mapping_dict["event_time"] = event_time
                mapping_dict["class"] = class_list
                mapping_dict["event_description"] = event_description
                mapping_dict["inserted_on"] = get_timestamp(app)
                mapping_dict["updated_on"] = get_timestamp(app)
                data_added(app, db, "event_data", mapping_dict)

                # for classes in class_list:
                #     all_student_info = find_spec_data(app, db, "students_data", {"department": classes})
                #     for student in all_student_info:
                #         student_id = student.get("student_id")
                #         username = student.get("username")
                #         get_notification_data = find_spec_data(app, db, "notification_data",
                #                                                {"student_id": student_id, "username": username})
                #         get_notification_data = list(get_notification_data)
                #         if get_notification_data:
                #             notification_list = get_notification_data[0]["notification_list"]
                #             notification_list.append(
                #                 {"msg": f"Event Added: {event_name}", "time": get_timestamp(app), "redirect": "/student/event_data"})
                #             update_mongo_data(app, db, "notification_data",
                #                               {"student_id": student_id, "username": username},
                #                               {"notification_list": notification_list})
                #
                #     all_teacher_info = find_spec_data(app, db, "teacher_data", {"department": classes})
                #     for teacher in all_teacher_info:
                #         teacher_id = teacher.get("teacher_id")
                #         username = teacher.get("username")
                #         get_notification_data = find_spec_data(app, db, "notification_data",
                #                                                {"teacher_id": teacher_id, "username": username})
                #         get_notification_data = list(get_notification_data)
                #         if get_notification_data:
                #             notification_list = get_notification_data[0]["notification_list"]
                #             notification_list.append(
                #                 {"msg": f"Event Added: {event_name}", "time": get_timestamp(app), "redirect": "/teacher/event_data"})
                #             update_mongo_data(app, db, "notification_data",
                #                               {"teacher_id": teacher_id, "username": username},
                #                               {"notification_list": notification_list})
                #
                # all_admin_info = find_all_data(app, db, "admin_data")
                # for admin in all_admin_info:
                #     username = admin.get("username")
                #     get_notification_data = find_spec_data(app, db, "notification_data",
                #                                            {"username": username})
                #     get_notification_data = list(get_notification_data)
                #     if get_notification_data:
                #         notification_list = get_notification_data[0]["notification_list"]
                #         notification_list.append(
                #             {"msg": f"Event Added: {event_name}", "time": get_timestamp(app), "redirect": "/admin/event_data"})
                #         update_mongo_data(app, db, "notification_data",
                #                           {"username": username},
                #                           {"notification_list": notification_list})


            flash("Event Added Successfully", "success")
            return jsonify({"messgae":"done"})
        else:
            return render_template("admins/add-events.html", allclass=allclass, type=type, photo_link=photo_main_link, admin_id=admin_id)

    except Exception as e:
        app.logger.debug(f"Error in add admin data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_event', _external=True, _scheme=secure_type))

@app.route("/admin/event_data", methods=["GET", "POST"])
@token_required
def event_data_list():
    """
    That funcation can use show all admins data from admin panel
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["event_data"]
        get_all_data = coll.find({})
        event_names = []
        add_minutes = []
        classname = []
        flags = []
        mapping_dict = {}
        for data in get_all_data:
            del data["_id"]
            event_name = data.get("event_name", "")
            event_date = data.get("event_date", "23-01-2024")
            event_time = data.get("event_time", "00:00")
            current_datetime = datetime.now().strftime("%d-%m-%Y %H:%M")
            target_datetime = f"{event_date} {event_time}"
            mapping_dict[event_name] = data
            remaining_milliseconds, gone = calculate_remaining_milliseconds(current_datetime, target_datetime)
            if gone:
                flag = "true"
                class_name = "bg-success"
            else:
                flag = "false"
                class_name = "bg-purple"

            event_names.append(event_name)
            add_minutes.append(int(remaining_milliseconds))
            classname.append(class_name)
            flags.append(flag)

        decoded_dict = {key: {k: html.unescape(v) for k, v in sub_dict.items()} for key, sub_dict in
                        mapping_dict.items()}

        # Convert Python dictionary to JSON string
        json_data = json.dumps(decoded_dict)

        print(json_data)

        return render_template("admins/event.html", event_names=event_names, mapping_dict=json_data, type=type, admin_id=id,
                               photo_link=photo_link, add_minutes=add_minutes, classname=classname, flags=flags)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('event_data_list', _external=True, _scheme=secure_type))

@app.route("/admin/add_email_configuration", methods=["GET", "POST"])
@token_required
def add_email_configuration():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_student_link = "/" + login_dict["photo_link"]
        db = client["college_management"]

        if request.method == "POST":
            server_username = request.form["server_username"]
            server_password = request.form["server_password"]
            server_host = request.form["server_host"]
            server_port = request.form["server_port"]

            app.config["email_configuration"][admin_id] = {"server_host": server_host, "server_port": server_port,
                                                           "server_username": server_username, "server_password": server_password}
            flash("Email-Server configure successfully...", "success")
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in add_email_configuration route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/admin/add_homework", methods=["GET", "POST"])
@token_required
def add_homework():
    """
    In this route we can handling admin register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_main_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        class_data = find_all_data(app, db, "class_data")
        allclass_data = [class_text["class_name"] for class_text in class_data]
        allclass = list(set(list(allclass_data)))

        class_data = find_all_data(app, db, "class_data")
        allsubject_data = [class_text["subject_name"] for class_text in class_data]
        allsubject = list(set(list(allsubject_data)))

        if request.method == "POST":
            class_name = request.form["class"]
            subject_name = request.form["subject"]
            title = request.form["title"]
            description = request.form["description"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/homework_attach_file")

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        filename = file.filename
                        ans = checking_upload_folder(app=app, filename=filename)
                        if ans != "duplicate":
                            attach_file_path = os.path.join(attach_save_dir, file.filename)
                            attachment_all_file.append(attach_file_path)
                            file.save(attach_file_path)
                        else:
                            flash('This filename already exits', "danger")
                            return render_template("admins/add_homework.html", type=type,
                                   photo_link=photo_main_link, admin_id=admin_id, allclass=allclass,allsubject=allsubject)

            db = client["college_management"]
            comment_di = {}
            comment_di["title"] = title
            comment_di["teacher_name"] = admin_id
            comment_di["class_name"] = class_name
            comment_di["subject_name"] = subject_name
            comment_di["description"] = description

            all_homework_data = find_all_data(app, db, "homework_data")
            li = []
            for homework in all_homework_data:
                del homework["_id"]
                del homework["attechment_filepath_list"]
                del homework["inserted_on"]
                del homework["updated_on"]
                li.append(homework)

            if comment_di not in li:
                data_dict = {"title": title, "teacher_name": admin_id, "description": description, "class_name":class_name, "subject_name":subject_name,
                             "attechment_filepath_list": attachment_all_file,
                             "inserted_on": get_timestamp(app), "updated_on": get_timestamp(app)}

                comment_data_dict = {"title": title, "description": description, "class_name": class_name, "subject_name": subject_name,
                                     "attechment_filepath_list": attachment_all_file, "comments":[], "inserted_on": get_timestamp(app), "updated_on": get_timestamp(app)}

                data_added(app, db, "homework_data", data_dict)
                data_added(app, db, "homework_comment", comment_data_dict)
                # depart = department_name
                # all_student_info = find_spec_data(app, db, "students_data", {"department": depart})
                # for student in all_student_info:
                #     student_id = student.get("student_id")
                #     username = student.get("username")
                #     get_notification_data = find_spec_data(app, db, "notification_data",{"student_id": student_id, "username": username})
                #     get_notification_data = list(get_notification_data)
                #     if get_notification_data:
                #         notification_list = get_notification_data[0]["notification_list"]
                #         notification_list.append({"msg": f"Homework Added: {title}", "time": get_timestamp(app), "redirect": "/student/homework_data"})
                #         update_mongo_data(app, db, "notification_data",{"student_id": student_id, "username": username},{"notification_list": notification_list})
                #
                # all_teacher_info = find_spec_data(app, db, "teacher_data", {"department": depart})
                # for teacher in all_teacher_info:
                #     teacher_id = teacher.get("teacher_id")
                #     username = teacher.get("username")
                #     get_notification_data = find_spec_data(app, db, "notification_data",{"teacher_id": teacher_id, "username": username})
                #     get_notification_data = list(get_notification_data)
                #     if get_notification_data:
                #         notification_list = get_notification_data[0]["notification_list"]
                #         notification_list.append({"msg": f"Homework Added: {title}", "time": get_timestamp(app), "redirect": "/teacher/homework_data"})
                #         update_mongo_data(app, db, "notification_data",{"teacher_id": teacher_id, "username": username},{"notification_list": notification_list})
                #
                # all_admin_info = find_all_data(app, db, "admin_data")
                # for admin in all_admin_info:
                #     username = admin.get("username")
                #     get_notification_data = find_spec_data(app, db, "notification_data",{"username": username})
                #     get_notification_data = list(get_notification_data)
                #     if get_notification_data:
                #         notification_list = get_notification_data[0]["notification_list"]
                #         notification_list.append({"msg": f"Homework Added: {title}", "time": get_timestamp(app), "redirect": "/admin/homework_data"})
                #         update_mongo_data(app, db, "notification_data",{"username": username},{"notification_list": notification_list})

                flash("Home work data added successfully...", "success")
                return redirect(url_for('homework_data_list', _external=True, _scheme=secure_type))
            else:
                flash("That data is already exits...", "danger")
                return redirect(url_for('add_homework', _external=True, _scheme=secure_type))
        else:
            return render_template("admins/add_homework.html", allclass=allclass,type=type,
                                   photo_link=photo_main_link, admin_id=admin_id, allsubject=allsubject)

    except Exception as e:
        app.logger.debug(f"Error in add homework data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('add_homework', _external=True, _scheme=secure_type))

@app.route("/admin/homework_data", methods=["GET", "POST"])
@token_required
def homework_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "homework_data")
        if all_keys and all_values:
            return render_template("admins/homework.html", all_keys=all_keys[0], all_values=all_values, type=type,
                                   admin_id=id, photo_link=photo_link)
        else:
            return render_template("admins/homework.html", all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('department_data_list', _external=True, _scheme=secure_type))

@app.route("/admin/comment", methods=["GET", "POST"])
@token_required
def comment_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        data = json.loads(request.data)
        finding_dict = {}
        finding_dict['title'] = data["title"]
        finding_dict['department_name'] = data["department_name"]
        finding_dict['class_name'] = data["class_name"]

        all_data_dict = {'finding_dict':finding_dict, "photo_link": photo_link, "type": type}
        app.config[id] = all_data_dict
        return render_template("admins/comments.html",)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('comment_data_list', _external=True, _scheme=secure_type))


@app.route("/admin/data_comment", methods=["GET", "POST"])
@token_required
def data_comment():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        id = login_dict["id"]
        db = client["college_management"]
        data_show = app.config[id]
        finding_dict = data_show.get("finding_dict", {})
        photo_link = data_show.get("photo_link", "")
        type = data_show.get("type", "")
        comments_data = find_spec_data(app, db, "homework_comment", finding_dict)
        comments_data = list(comments_data)
        comments_data = comments_data[0]
        mapping_list = []
        comment_di = {}
        comment_di["title"] = comments_data.get("title", "")
        comment_di["department_name"] = comments_data.get("department_name", "")
        comment_di["class_name"] = comments_data.get("class_name", "")
        comment_di["subject_name"] = comments_data.get("subject_name", "")
        description = comments_data.get("description", "")
        attechment_list = comments_data.get("attechment_filepath_list", [])
        attechment_main_data = []
        for attechment in attechment_list:
            attechment = attechment.replace("\\", "----")
            attechment_main_data.append(attechment)
        condition_mapping = f'{str(comment_di["title"])}---{str(comment_di["department_name"])}---{str(comment_di["class_name"])}---{str(comment_di["subject_name"])}'
        all_di_list = list(comment_di.keys())
        if attechment_main_data:
            attachment_flag = True
        else:
            attachment_flag = False

        comments_list = comments_data.get("comments", [])
        count_comment = len(comments_list)
        for number in comments_list:
            di = {}
            result = calculate_time_ago(number["time"])
            if result == None:
                result = "now"
            di["image"] = number["image"]
            di["username"] = number["username"]
            di["time"] = number["time"]
            di["comment_data"] = number["comment_data"]
            mapping_list.append(di)
        return render_template("admins/comments.html", condition_mapping=condition_mapping, description=description,
                               all_di_list=all_di_list, comment_di=comment_di, attachment_flag=attachment_flag,
                               type=type, attechment_list=attechment_list, admin_id=id, photo_link=photo_link,
                               count_comment=count_comment, mapping_list=mapping_list)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('data_comment', _external=True, _scheme=secure_type))

@app.route("/admin/add_comment", methods=["GET", "POST"])
@token_required
def add_comment():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method == "POST":
            data = json.loads(request.data)
            comment_text = data.get("comment", "")
            object_data = data.get("object_data", "")
            spliting = object_data.split("---")
            title = spliting[0]
            department_name = spliting[1]
            class_name = spliting[2]
            subject_name = spliting[3]
            di = {}
            di["image"] = photo_link
            di["username"] = id
            di["time"] = get_timestamp(app)
            di["comment_data"] = comment_text

            condition_dict = {"title": title, "department_name": department_name, "class_name": class_name, "subject_name":subject_name}
            comments_data = find_spec_data(app, db, "homework_comment", condition_dict)
            comments_data = list(comments_data)
            comments_data = comments_data[0]
            comment_data_data = comments_data.get("comments", [])
            comment_data_data.append(di)
            update_mongo_data(app, db, "homework_comment", condition_dict, {"comments":comment_data_data})
            return jsonify({"message": "done"})
        else:
            return jsonify({"message": "done"})

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return jsonify({"message": "failed"})

def get_current_time():
    current_time = datetime.now()
    formatted_time = current_time.strftime('%m-%d-%Y %H:%M:%S')
    return formatted_time

# @app.route("/admin/attendance_data", methods=["GET", "POST"])
# @token_required
# def admin_attendance_data_list():
#     """
#     That funcation can use show all admins data from admin panel
#     """
#
#     try:
#         login_dict = session.get("login_dict", "nothing")
#         type = login_dict["type"]
#         admin_id = login_dict["id"]
#         photo_link = "/" + login_dict["photo_link"]
#         db = client["college_management"]
#         class_data = find_all_data(app, db, "class_data")
#         allclass = [class_text["class_name"] for class_text in class_data]
#         allclass = list(set(allclass))
#         coll_user = db["user_data"]
#         all_student = coll_user.find({"type": "student"})
#         all_student_list = [student["username"] for student in all_student]
#         all_user_data = coll_user.find({})
#         all_user_names_list = [userdata["username"] for userdata in all_user_data]
#         if request.method=="POST":
#             panel = request.args.get("panel", "student")
#             if 'add' in request.form:
#                 class_name = request.form["class_name"]
#                 attendance_date = request.form["date"]
#                 all_spec_students = find_spec_data(app, db, "user_data", {"class_name": class_name, "type": "student"})
#                 all_spec_students = list(all_spec_students)
#                 all_student_username = [students["username"] for students in all_spec_students]
#                 return render_template("admins/teacher_attendance.html", allclass=allclass,class_name=class_name,
#                                        allstudent=all_student_list,all_user_names_list=all_user_names_list, type=type, admin_id=admin_id,datakey="yes",
#                                        photo_link=photo_link,all_student_username=all_student_username,attendance_date=attendance_date)
#
#             elif "view" in request.form:
#                 class_name = request.form["class_name"]
#                 attendance_date = request.form["date"]
#                 all_spec_students = find_spec_data(app, db, "attendance_data", {"class_name": class_name, "attendance_date": attendance_date})
#                 all_spec_students = list(all_spec_students)
#                 return render_template("admins/teacher_attendance.html", allclass=allclass, class_name=class_name,
#                                        allstudent=all_student_list, type=type, admin_id=admin_id,
#                                        datakey="no",all_user_names_list=all_user_names_list, photo_link=photo_link, all_student_username=all_spec_students,
#                                        attendance_date=attendance_date)
#
#         return render_template("admins/teacher_attendance.html", allclass=allclass,
#                                allstudent=all_student_list,all_user_names_list=all_user_names_list, type=type, admin_id=admin_id, photo_link=photo_link)
#
#     except Exception as e:
#         app.logger.debug(f"Error in add teacher data route: {e}")
#         return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/admin/attendance_data", methods=["GET", "POST"])
@token_required
def admin_attendance_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        atten_date = request.args.get("atten_date", "nothing")
        db = client["college_management"]
        class_data = find_all_data(app, db, "class_data")
        allclass = [class_text["class_name"] for class_text in class_data]
        allclass = list(set(allclass))
        if atten_date!="nothing":
            today_date = atten_date
        else:
            today_date = datetime.today().strftime('%d-%m-%Y')
        coll_attendance = db["attendance_data"]
        class_status = []
        for class_text in allclass:
            atten_data = coll_attendance.find({"class_name": class_text, "attendance_date": today_date})
            atten_data = list(atten_data)
            if atten_data:
                class_status.append([class_text, today_date, "Active"])
            else:
                class_status.append([class_text, today_date, "Not Active"])

        coll_user = db["user_data"]
        all_student = coll_user.find({"type": "student"})
        all_student_list = [student["username"] for student in all_student]
        all_user_data = coll_user.find({})
        all_user_names_list = [userdata["username"] for userdata in all_user_data]
        if request.method=="POST":
            panel = request.args.get("panel", "view")
            if panel=="add":
                data = json.loads(request.data)
                class_name = data["class_name"]
                attendance_date = data["selected_date"]
                all_spec_students = find_spec_data(app, db, "user_data", {"class_name": class_name, "type": "student"})
                all_spec_students = list(all_spec_students)
                all_student_username = [students["username"] for students in all_spec_students]
                json_data = {"class_name": class_name, "attendance_date": attendance_date, "all_students": all_student_username}
                return {"output_data": json_data}

            elif panel=="view":
                data = json.loads(request.data)
                class_name = data["class_name"]
                attendance_date = data["selected_date"]
                all_spec_students = find_spec_data(app, db, "attendance_data", {"class_name": class_name, "attendance_date": attendance_date})
                all_spec_students = list(all_spec_students)
                all_students = []
                for student_text in all_spec_students:
                    all_students.append([student_text["username"], student_text["status"], student_text["reason"]])
                json_data = {"class_name": class_name, "attendance_date": attendance_date,
                             "all_students": all_students}

                return {"output_data": json_data}

        return render_template("admins/teacher_attendance.html", allclass=allclass,class_status=class_status,
                               allstudent=all_student_list,all_user_names_list=all_user_names_list, type=type, admin_id=admin_id, photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/admin/attendance", methods=["GET", "POST"])
@token_required
def admin_date_attendance_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        admin_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        class_data = find_all_data(app, db, "class_data")
        allclass = [class_text["class_name"] for class_text in class_data]
        allclass = list(set(allclass))


        coll_user = db["user_data"]
        all_student = coll_user.find({"type": "student"})
        all_student_list = [student["username"] for student in all_student]
        all_user_data = coll_user.find({})
        all_user_names_list = [userdata["username"] for userdata in all_user_data]
        if request.method=="POST":
            attendance_date = request.form["atten_date"]
            date_obj = datetime.strptime(attendance_date, '%Y-%m-%d')
            atten_date = date_obj.strftime('%d-%m-%Y')
            coll_attendance = db["attendance_data"]
            class_status = []
            for class_text in allclass:
                atten_data = coll_attendance.find({"class_name": class_text, "attendance_date": atten_date})
                atten_data = list(atten_data)
                if atten_data:
                    class_status.append([class_text, atten_date, "Active"])
                else:
                    class_status.append([class_text, atten_date, "Not Active"])

        return render_template("admins/teacher_attendance.html", allclass=allclass,class_status=class_status,
                               allstudent=all_student_list,all_user_names_list=all_user_names_list, type=type, admin_id=admin_id, photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))


@app.route("/admin/add_attendance", methods=["GET", "POST"])
@token_required
def add_attendance():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        db = client["college_management"]
        coll = db["attendance_data"]
        if request.method=="POST":
            panel = request.args.get("panel", "student")
            data = json.loads(request.data)
            mapping_dict = data["get_data"]
            attendance_date = mapping_dict[0]["attendance_date"]
            all_data = coll.find({"attendance_date": attendance_date, "type": "student"})
            all_data_main = [[data_main["attendance_date"], data_main["username"]] for data_main in all_data]
            for var in mapping_dict:
                li = [var["attendance_date"], var["username"]]
                if li not in all_data_main:
                    var["inserted_on"] = get_timestamp(app)
                    var["updated_on"] = get_timestamp(app)
                    coll.insert_one(var)
            flash("Attendance successfully...", "success")
            return jsonify({"status": 200})

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

def mapping_data_dict(type):
    try:
        db = client["college_management"]
        coll = db["chats_mapping"]
        all_data = []
        if type=="admin":
            all_data = coll.find({"type": "student"})
            all_data = list(all_data)
            all_data_main = coll.find({"type": "teacher"})
            all_data_main = list(all_data_main)
            all_data.extend(all_data_main)
        elif type=="student":
            all_data = coll.find({"type": "teacher"})
            all_data = list(all_data)
        elif type=="teacher":
            all_data = coll.find({"type": "student"})
            all_data = list(all_data)

        return all_data

    except Exception as e:
        print(e)

@app.route('/chat')
@token_required
def chat():
    login_dict = session["login_dict"]
    type = login_dict.get("type", "admin")
    id = login_dict.get("id", "admin")
    photo_link = login_dict.get("photo_link", "/static/assets/assets/media/avatar/4.png")
    startmessage = "Nice meeting you again"
    db = client["college_management"]
    coll = db["chats_mapping"]
    try:
        user_mapping_dict = app.config["user_mapping_dict"][id]
        all_mapping_data = app.config["all_mapping_data"]
    except:
        all_data = coll.find({"username": id})
        all_data = list(all_data)
        if all_data:
            user_mapping_dict = all_data[0]["user_mapping_dict"]
        else:
            user_mapping_dict = []
        app.config["user_mapping_dict"][id] = user_mapping_dict
        all_mapping_data = mapping_data_dict(type=type)
        app.config["all_mapping_data"] = all_mapping_data

    condition_dict = {"username": id}
    update_data = {"status": "active", "last_seen": get_timestamp(app)}
    coll.update_one(condition_dict, {"$set": update_data})

    return render_template('chats/chatscreen.html', user_image=photo_link, username=id, startmessage=startmessage,
                           mapping_dict=all_mapping_data, chats_mapping=user_mapping_dict)

@app.route('/data_add', methods=["GET", "POST"])
def data_add():
    try:
        db = client["college_management"]
        coll = db["chats_mapping"]
        data = json.loads(request.data)
        login_dict = session["login_dict"]
        id = login_dict.get("id", "admin")
        username = data["username"]
        last_seen = data["last_seen"]
        photo_link = data["photo_link"]
        user_mapping_dict_data = app.config["user_mapping_dict"][id]
        data_mapping = [var["username"] for var in user_mapping_dict_data]
        if username not in data_mapping:
            mapping_dict = {}
            mapping_dict["username"] = username
            mapping_dict["last_seen"] = get_timestamp(app)
            mapping_dict["photo_link"] = photo_link
            mapping_dict["last_message"] = ""
            mapping_dict["chat_history"] = []
            mapping_dict["status"] = "inactive"
            user_mapping_dict_data.append(mapping_dict)
            app.config["user_mapping_dict"][id] = user_mapping_dict_data
            coll.update_one({"username": id}, {"$set": {"user_mapping_dict": user_mapping_dict_data}})
            return jsonify({"status": 200})
        else:
            return jsonify({"status": 200})
    except Exception as e:
        print(e)

@app.route('/chatscreen/<object>')
@token_required
def chatscreen(object):
    login_dict = session["login_dict"]
    type = login_dict.get("type", "admin")
    id = login_dict.get("id", "admin")
    photo_link = login_dict.get("photo_link", "/static/assets/assets/media/avatar/4.png")
    spliting_object = object.split("***")
    username = spliting_object[0]
    last_seen = spliting_object[1]

    db = client["college_management"]
    coll = db["chats_mapping"]
    try:
        user_mapping_dict = app.config["user_mapping_dict"][id]
        all_mapping_data = app.config["all_mapping_data"]
        chats_data = app.config["chats_data"][id][username]
    except:
        chats_list = []
        all_data = coll.find({"username": id})
        all_data = list(all_data)
        if all_data:
            user_mapping_dict = all_data[0]["user_mapping_dict"]
        else:
            user_mapping_dict = []
        app.config["user_mapping_dict"][id] = user_mapping_dict
        all_mapping_data = mapping_data_dict(type=type)
        app.config["all_mapping_data"] = all_mapping_data
        for data_text in user_mapping_dict:
            if data_text["username"]==username:
                chats_list = data_text.get("chat_history", [])

        app.config["chats_data"][id] = {}
        app.config["chats_data"][id][username] = chats_list

    user_mapping_dict_data = {}
    for user_data in user_mapping_dict:
        if user_data["username"] == username:
            if "status" not in list(user_data.keys()):
                user_data["status"] = "inactive"
            user_mapping_dict_data = user_data

    chats_data = app.config["chats_data"][id][username]
    return render_template('chats/chat-1.html',user_image=photo_link, username=id,user_mapping_dict_data=user_mapping_dict_data,
                           mapping_dict=all_mapping_data, chats_mapping=user_mapping_dict, chats_data=chats_data)

@socketio.on('message')
def handle_message(message):
    data = message
    finding_dict = {}
    login_dict = session["login_dict"]
    id = login_dict.get("id", "admin")
    question = data["question"]
    send_username = data["send_username"]
    db = client["college_management"]
    coll = db["chats_mapping"]

    id_mapping_dict = {"msg": question, "is_user": True, "status": False, "time": get_timestamp(app)}
    sent_mapping_dict = {"msg": question, "is_user": False, "status": False, "time": get_timestamp(app)}

    try:
        user_mapping_dict = app.config["user_mapping_dict"][id]
        all_mapping_data = app.config["all_mapping_data"]
        chats_data = app.config["chats_data"][id][send_username]

        chats_data.append(id_mapping_dict)
        app.config["chats_data"][id][send_username] = chats_data

        all_user_mapping_dict = []
        for data in user_mapping_dict:
            if data["username"] == send_username:
                mapping_di = {}
                mapping_di["username"] = data["username"]
                mapping_di["last_seen"] = get_timestamp(app)
                mapping_di["photo_link"] = data["photo_link"]
                mapping_di["last_message"] = question
                mapping_di["chat_history"] = chats_data
                mapping_di["status"] = data.get("status", "inactive")
                all_user_mapping_dict.append(data)
            else:
                all_user_mapping_dict.append(data)

        app.config["user_mapping_dict"][id] = all_user_mapping_dict

        all_new_mapping = []
        for new_map in all_mapping_data:
            if new_map["username"] == id:
                new_di = {}
                new_di["username"] = new_map["username"]
                new_di["photo_link"] = new_map["photo_link"]
                new_di["user_mapping_dict"] = all_user_mapping_dict
                new_di["status"] = new_map["status"]
                new_di["type"] = new_map["type"]
                new_di["last_seen"] = get_timestamp(app)
                all_new_mapping.append(new_di)
            else:
                all_new_mapping.append(new_map)

        app.config["all_mapping_data"] = all_new_mapping

        coll.update_one({"username": id},
                        {"$set": {"user_mapping_dict": all_user_mapping_dict}})

    except Exception as e:
        print(e)

    try:
        user_mapping_dict_data = app.config["user_mapping_dict"][send_username]
        all_mapping_data_data = app.config["all_mapping_data"]
        chats_data_data = app.config["chats_data"][send_username][id]
    except:
        app.config["chats_data"][send_username] = {}
        app.config["chats_data"][send_username][id] = []
        app.config["user_mapping_dict"][send_username] = []
        user_mapping_dict_data = app.config["user_mapping_dict"][send_username]
        all_mapping_data_data = app.config["all_mapping_data"]
        chats_data_data = app.config["chats_data"][send_username][id]

    chats_data_data.append(sent_mapping_dict)
    app.config["chats_data"][send_username][id] = chats_data_data

    all_user_mapping_dict1 = []
    key_flag = True
    for data in user_mapping_dict_data:
        if data["username"] == id:
            mapping_di = {}
            key_flag = False
            mapping_di["username"] = data["username"]
            mapping_di["last_seen"] = get_timestamp(app)
            mapping_di["photo_link"] = data["photo_link"]
            mapping_di["last_message"] = question
            mapping_di["chat_history"] = chats_data_data
            mapping_di["status"] = data.get("status", "inactive")
            all_user_mapping_dict1.append(data)
        else:
            all_user_mapping_dict1.append(data)

    if key_flag:
        mapping_di = {"username": id, "last_seen": get_timestamp(app), "photo_link": login_dict.get("photo_link"), "last_message": question,
                      "chat_history": chats_data_data, "status": "inactive"}
        all_user_mapping_dict1.append(mapping_di)

    app.config["user_mapping_dict"][send_username] = all_user_mapping_dict1

    all_new_mapping1 = []
    key_map = True
    for new_map in all_mapping_data_data:
        if new_map["username"] == send_username:
            new_di = {}
            key_map = False
            new_di["username"] = new_map["username"]
            new_di["photo_link"] = new_map["photo_link"]
            new_di["user_mapping_dict"] = all_user_mapping_dict1
            new_di["status"] = new_map["status"]
            new_di["type"] = new_map["type"]
            new_di["last_seen"] = get_timestamp(app)
            all_new_mapping1.append(new_di)
        else:
            all_new_mapping1.append(new_map)

    if key_map:
        new_di = {"username": send_username, "photo_link": "", "user_mapping_dict": all_user_mapping_dict1, "status": "inactive", "type": "", "last_seen": get_timestamp(app)}
        all_new_mapping1.append(new_di)

    app.config["all_mapping_data"] = all_new_mapping1

    coll.update_one({"username": send_username},
                    {"$set": {"user_mapping_dict": all_user_mapping_dict1}})

    emit('message', "message", broadcast=True)
    return {"status": 200}

@app.route("/admin/feedback_form", methods=["GET", "POST"])
def feedback_form():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        if request.method == "POST":
            feedback_msg = request.form["feedback_msg"]
            username_msg = request.form["username_msg"]
            subject_msg = request.form["subject_msg"]
            attachment_all_file = []
            if 'images[]' in request.files:
                images = request.files.getlist('images[]')
                for file in images:
                    if file.filename != '':
                        attach_file_path = os.path.join("static/uploads/attach_file", file.filename)
                        attachment_all_file.append(attach_file_path)
                        file.save(attach_file_path)

            mapping_dict = {}
            mapping_dict["username"] = username_msg
            mapping_dict["subject_msg"] = subject_msg
            mapping_dict["feedback_msg"] = feedback_msg
            mapping_dict["all_attechment_files"] = attachment_all_file
            mapping_dict["inserted_on"] = get_timestamp(app)
            mapping_dict["updated_on"] = get_timestamp(app)
            db = client["college_management"]
            coll = db["feedback_data"]
            coll.insert_one(mapping_dict)

            name = "Mohmad"
            mail_format = f"Dear {name},\n\nI hope this email finds you well. I am writing to inform you that a user has recently filled out a feedback form on our platform, and I thought it would be valuable for you to be aware of the input provided.\n\nHere are the details of the feedback:\n\n- User's Name: {username_msg}\n\n- Date and Time of Submission: {get_timestamp(app)}\n\nFeedback:{feedback_msg}\n\nPlease take a moment to review the detailed feedback in the attached document or by accessing our feedback management system. If you have any questions or need further information, feel free to reach out to the user directly. I'll be happy to assist.\n\nThank you for your attention to this matter, and I appreciate your commitment to ensuring the best possible experience for our users.\n\nBest regards,\n{username_msg}"
            html_format = f"<p>Dear {name},</p><p>I hope this email finds you well. I am writing to inform you that a user has recently filled out a feedback form on our platform, and I thought it would be valuable for you to be aware of the input provided.</p><p>Here are the details of the feedback:</p><p>- User's Name: {username_msg}</p><p>- Date and Time of Submission: {get_timestamp(app)}</p><p>Feedback:{feedback_msg}</p><p>Please take a moment to review the detailed feedback in the attached document or by accessing our feedback management system. If you have any questions or need further information, feel free to reach out to the user directly. I'll be happy to assist.</p><p>Thank you for your attention to this matter, and I appreciate your commitment to ensuring the best possible experience for our users.</p><p>Best regards,<br>{username_msg}</p>"
            server_host = app.config['MAIL_SERVER']
            server_port = app.config['MAIL_PORT']
            server_username = app.config['MAIL_USERNAME']
            server_password = app.config['MAIL_PASSWORD']
            subject_title = f"Feedback Form: {subject_msg}"
            email = "harshitgadhiya8980@gmail.com"
            sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                               server_password, server_host, int(server_port), attachment_all_file)

            flash("Query Added Successfully...", "success")
            return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))
        else:
            return redirect(url_for('admin_data_list', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in feedback form route: {e}")
        flash("Server Not Responding...","danger")
        return redirect(url_for('chat', _external=True, _scheme=secure_type))

@app.route('/admin/add_group', methods=["GET", "POST"])
def add_group():
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["chats_mapping"]
        groupname = request.form.get("groupname", "undefined")
        department = request.form.get("department", "")
        classes = request.form.get("class", "")
        selected_student = request.form.getlist('preferences')
        photo_path = "/static/assets/img/group_icon.png"

        if selected_student:
            pass
        else:
            coll_student = db["students_data"]
            all_st_data = coll_student.find({"department": department, "classes": classes})
            selected_student = [st["username"] for st in all_st_data]

        if selected_student:
            app.config["group_data"][id] = selected_student
            for student in selected_student:
                try:
                    per_user_mapping_dict = app.config["user_mapping_dict"][student]
                    mapping_di = {
                        "username": groupname,
                        "last_seen": get_timestamp(app),
                        "photo_link": photo_path,
                        "last_message": "",
                        "chat_history": [],
                        "status": "inactive"
                    }
                    per_user_mapping_dict.append(mapping_di)
                    app.config["user_mapping_dict"][student] = per_user_mapping_dict
                except:
                    coll_map = db["chats_mapping"]
                    all_stu_data = coll_map.find({"username": student})
                    all_stu_data = list(all_stu_data)
                    per_user_mapping_dict = all_stu_data[0]["user_mapping_dict"]
                    all_username_data = [var["username"] for var in per_user_mapping_dict]
                    mapping_di = {
                        "username": groupname,
                        "last_seen": get_timestamp(app),
                        "photo_link": photo_path,
                        "last_message": "",
                        "chat_history": [],
                        "status": "inactive"
                    }
                    if groupname not in all_username_data:
                        per_user_mapping_dict.append(mapping_di)
                    app.config["user_mapping_dict"][student] = per_user_mapping_dict

                db = client["college_management"]
                coll = db["chats_mapping"]
                coll.update_one({"username": student}, {"$set": {"user_mapping_dict": per_user_mapping_dict}})

        try:

            per_user_mapping_dict_id = app.config["user_mapping_dict"][id]
            mapping_di_dict = {
                "username": groupname,
                "last_seen": get_timestamp(app),
                "photo_link": photo_path,
                "last_message": "",
                "chat_history": [],
                "status": "inactive"
            }
            per_user_mapping_dict_id.append(mapping_di_dict)
            app.config["user_mapping_dict"][id] = per_user_mapping_dict_id
        except:
            mapping_di_dict = {
                "username": groupname,
                "last_seen": get_timestamp(app),
                "photo_link": photo_path,
                "last_message": "",
                "chat_history": [],
                "status": "inactive"
            }
            per_user_mapping_dict_id = [mapping_di_dict]
            app.config["user_mapping_dict"][id] = per_user_mapping_dict_id

        db = client["college_management"]
        coll = db["chats_mapping"]
        coll.update_one({"username": id}, {"$set": {"user_mapping_dict": per_user_mapping_dict_id}})

        return redirect(url_for('chat', _external=True, _scheme=secure_type))

    except Exception as e:
        print(e)

@app.route("/admin/feedback_data", methods=["GET", "POST"])
def feedback_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "feedback_data")

        if all_keys and all_values:
            return render_template("admins/feedback.html", all_notifications=get_notifications(id),
                                all_keys=all_keys[0], all_values=all_values, type=type, admin_id=id,
                                photo_link=photo_link)
        else:
            return render_template("admins/feedback.html", all_notifications=get_notifications(id),
                                   all_keys=all_keys, all_values=all_values, type=type, admin_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show feedback data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('feedback_data_list', _external=True, _scheme=secure_type))

########################################## student dashboard ##########################################

@app.route("/student_dashboard", methods=["GET", "POST"])
@token_required
def student_dashboard():
    """
    That funcation can use otp_verification and new_password set link generate
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]

        db = client["college_management"]
        coll = db["students_data"]
        student_data = coll.find({"username": id})
        student_data = list(student_data)
        department = student_data[0]["department"]
        classes = student_data[0]["classes"]
        datashow = []
        upcoming_event = []
        completed_list = []
        coll_event = db["event_data"]
        get_all_data = coll_event.find({}).sort("event_date")
        for data in get_all_data:
            department_list = data["department"]
            if department in department_list:
                del data["_id"]
                del data["inserted_on"]
                del data["updated_on"]
                date_str = f"{data['event_date']} {data['event_time']}"
                date_format = "%d-%m-%Y %H:%M"
                date = datetime.strptime(date_str, date_format)

                # Get the current datetime
                current_datetime = datetime.now()

                # Compare the parsed datetime with the current datetime
                if date < current_datetime:
                    completed_list.append(data)
                else:
                    upcoming_event.append(data)
                datashow.append(data)

        condition_dict = {"department_name": department, "class_name": classes}
        coll_home = db["homework_data"]
        data_home = coll_home.find(condition_dict)
        homework_data = list(data_home)

        coll_atten = db["attendance_data"]
        attendance_data = coll_atten.find({"username": id}).sort("_id")
        attendance_data = list(attendance_data)
        attendance_list = []
        for at in attendance_data[:3]:
            del at["_id"]
            attendance_list.append(at)
        attendance_count = len(attendance_data)
        homework_count = len(homework_data)
        event_count = len(datashow)
        return render_template("students/student-dashboard.html",attendance_list=attendance_list,homework_data=homework_data,all_notifications=get_notifications(id), student_id=id, type=type, photo_link=photo_link,
                               attendance_count=attendance_count,upcoming_event=upcoming_event, completed_list=completed_list, homework_count=homework_count, event_count=event_count)

    except Exception as e:
        flash("Please try again...", "danger")
        print(f"Excpetion in student dashboard: {e}")
        return redirect(url_for('student_dashboard', _external=True, _scheme=secure_type))

@app.route("/student/event_data", methods=["GET", "POST"])
@token_required
def student_event_data_list():
    """
    That funcation can use show all admins data from admin panel
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["students_data"]
        student_data = coll.find({"username":id})
        student_data = list(student_data)
        department = student_data[0]["department"]
        classes = student_data[0]["classes"]

        coll_event = db["event_data"]
        get_all_data = coll_event.find({})
        event_names = []
        add_minutes = []
        classname = []
        flags = []
        mapping_dict = {}
        for data in get_all_data:
            department_list = data["department"]
            if department in department_list:
                del data["_id"]
                event_name = data.get("event_name", "")
                event_date = data.get("event_date", "23-01-2024")
                event_time = data.get("event_time", "00:00")
                current_datetime = datetime.now().strftime("%d-%m-%Y %H:%M")
                target_datetime = f"{event_date} {event_time}"
                mapping_dict[event_name] = data
                remaining_milliseconds, gone = calculate_remaining_milliseconds(current_datetime, target_datetime)
                if gone:
                    flag = "true"
                    class_name = "bg-success"
                else:
                    flag = "false"
                    class_name = "bg-purple"

                event_names.append(event_name)
                add_minutes.append(int(remaining_milliseconds))
                classname.append(class_name)
                flags.append(flag)

        decoded_dict = {key: {k: html.unescape(v) for k, v in sub_dict.items()} for key, sub_dict in
                        mapping_dict.items()}

        # Convert Python dictionary to JSON string
        json_data = json.dumps(decoded_dict)

        print(json_data)

        return render_template("students/event.html",all_notifications=get_notifications(id), event_names=event_names, mapping_dict=json_data, type=type, student_id=id,
                               photo_link=photo_link, add_minutes=add_minutes, classname=classname, flags=flags)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('event_data_list', _external=True, _scheme=secure_type))

@app.route("/student/homework_data", methods=["GET", "POST"])
@token_required
def student_homework_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]

        db = client["college_management"]
        coll = db["students_data"]
        student_data = coll.find({"username": id})
        student_data = list(student_data)
        department = student_data[0]["department"]
        classes = student_data[0]["classes"]
        
        condition_dict = {"department_name": department, "class_name": classes}
        all_keys, all_values = get_student_data(app, client, "college_management", "homework_data", condition_dict)
        if all_keys and all_values:
            return render_template("students/homework.html",all_notifications=get_notifications(id), all_keys=all_keys[0], all_values=all_values, type=type,
                                   student_id=id, photo_link=photo_link)
        else:
            return render_template("students/homework.html",all_notifications=get_notifications(id), all_keys=all_keys, all_values=all_values, type=type, student_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('department_data_list', _external=True, _scheme=secure_type))

@app.route("/student/comment", methods=["GET", "POST"])
@token_required
def student_comment_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        data = json.loads(request.data)
        finding_dict = {}
        finding_dict['title'] = data["title"]
        finding_dict['department_name'] = data["department_name"]
        finding_dict['class_name'] = data["class_name"]

        all_data_dict = {'finding_dict':finding_dict, "photo_link": photo_link, "type": type}
        app.config[id] = all_data_dict
        return render_template("students/comments.html",)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('student_comment_data_list', _external=True, _scheme=secure_type))

@app.route("/student/data_comment", methods=["GET", "POST"])
@token_required
def student_data_comment():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        id = login_dict["id"]
        db = client["college_management"]
        data_show = app.config[id]
        finding_dict = data_show.get("finding_dict", {})
        photo_link = data_show.get("photo_link", "")
        type = data_show.get("type", "")
        comments_data = find_spec_data(app, db, "homework_comment", finding_dict)
        comments_data = list(comments_data)
        comments_data = comments_data[0]
        mapping_list = []
        comment_di = {}
        comment_di["title"] = comments_data.get("title", "")
        comment_di["department_name"] = comments_data.get("department_name", "")
        comment_di["class_name"] = comments_data.get("class_name", "")
        comment_di["subject_name"] = comments_data.get("subject_name", "")
        description = comments_data.get("description", "")
        attechment_list = comments_data.get("attechment_filepath_list", [])
        attechment_main_data = []
        for attechment in attechment_list:
            attechment = attechment.replace("\\", "----")
            attechment_main_data.append(attechment)
        condition_mapping = f'{str(comment_di["title"])}---{str(comment_di["department_name"])}---{str(comment_di["class_name"])}---{str(comment_di["subject_name"])}'
        all_di_list = list(comment_di.keys())
        if attechment_main_data:
            attachment_flag = True
        else:
            attachment_flag = False

        comments_list = comments_data.get("comments", [])
        count_comment = len(comments_list)
        for number in comments_list:
            di = {}
            result = calculate_time_ago(number["time"])
            if result == None:
                result = "now"
            di["image"] = number["image"]
            di["username"] = number["username"]
            di["time"] = number["time"]
            di["comment_data"] = number["comment_data"]
            mapping_list.append(di)
        return render_template("students/comments.html", condition_mapping=condition_mapping, description=description,
                               all_di_list=all_di_list, comment_di=comment_di, attachment_flag=attachment_flag,
                               type=type, attechment_list=attechment_list, student_id=id, photo_link=photo_link,
                               count_comment=count_comment, mapping_list=mapping_list)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('student_data_comment', _external=True, _scheme=secure_type))

@app.route("/student/add_comment", methods=["GET", "POST"])
@token_required
def student_add_comment():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method == "POST":
            data = json.loads(request.data)
            comment_text = data.get("comment", "")
            object_data = data.get("object_data", "")
            spliting = object_data.split("---")
            title = spliting[0]
            department_name = spliting[1]
            class_name = spliting[2]
            subject_name = spliting[3]
            di = {}
            di["image"] = photo_link
            di["username"] = id
            di["time"] = get_timestamp(app)
            di["comment_data"] = comment_text

            condition_dict = {"title": title, "department_name": department_name, "class_name": class_name, "subject_name":subject_name}
            comments_data = find_spec_data(app, db, "homework_comment", condition_dict)
            comments_data = list(comments_data)
            comments_data = comments_data[0]
            comment_data_data = comments_data.get("comments", [])
            comment_data_data.append(di)
            update_mongo_data(app, db, "homework_comment", condition_dict, {"comments":comment_data_data})
            return jsonify({"message": "done"})
        else:
            return jsonify({"message": "done"})

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return jsonify({"message": "failed"})

@app.route("/student/attendance_data", methods=["GET", "POST"])
@token_required
def student_attendance_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]

        db = client["college_management"]

        condition_dict = {"username": id}
        all_keys, all_values = get_student_data(app, client, "college_management", "attendance_data", condition_dict)
        if all_keys and all_values:
            return render_template("students/attendance.html", all_notifications=get_notifications(id),
                                   all_keys=all_keys[0], all_values=all_values, type=type,
                                   student_id=id, photo_link=photo_link)
        else:
            return render_template("students/attendance.html", all_notifications=get_notifications(id), all_keys=all_keys,
                                   all_values=all_values, type=type, student_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('student_attendance_data_list', _external=True, _scheme=secure_type))

@app.route("/student/search/<object>", methods=["GET", "POST"])
@token_required
def student_search_data(object):
    """
    That funcation can use search data from student, teacher and admin from admin panel
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        if request.method=="POST":
            if object=="homework":
                htmlfilename = "students/homework.html"
                condition_dict = {}
                department_name = request.form["department_name"]
                title = request.form["title"]
                class_name = request.form["class_name"]
                subject_name = request.form["subject_name"]
                if department_name:
                    condition_dict["department_name"] = department_name
                if title:
                    condition_dict["title"] = title
                if class_name:
                    condition_dict["class_name"] = class_name
                if subject_name:
                    condition_dict["subject_name"] = subject_name
                all_keys, all_values = get_student_data(app, client, "college_management", "homework_data",
                                                        condition_dict)

                output_list = [list(item) for item in zip(*all_values)]

                searched_value = []
                for search_text in all_values:
                    title = search_text[0]
                    department_name = search_text[1]
                    class_name = search_text[2]
                    subject_name = search_text[3]
                    if title in output_list[0] or department_name in output_list[1] or class_name in output_list[2] or subject_name in output_list[3]:
                        searched_value.append(search_text)

            elif object=="attendance":
                htmlfilename = "students/attendance.html"
                condition_dict = {}
                attendance_date_text = request.form["attendance_date"]
                datasplit = attendance_date_text.split("-")
                attendance_date = "-".join(datasplit[::-1])
                status = request.form["status"]
                reason = request.form["reason"]
                condition_dict["username"] = id
                if attendance_date:
                    condition_dict["attendance_date"] = attendance_date
                if status:
                    condition_dict["status"] = status
                if reason:
                    condition_dict["reason"] = reason
                all_keys, searched_value = get_student_data(app, client, "college_management", "attendance_data",
                                                        condition_dict)

            if all_keys and searched_value:
                return render_template(htmlfilename, all_keys=all_keys[0], all_values=searched_value,
                                       type=type, admin_id=id, photo_link=photo_link, search="true")
            else:
                return render_template(htmlfilename, all_keys=all_keys, all_values=searched_value,
                                       type=type, admin_id=id, photo_link=photo_link, search="true")

    except Exception as e:
        app.logger.debug(f"Error in searching data from database: {e}")
        print("Error in searching data", e)
        flash("Please try again...", "danger")
        panel = object
        if panel == "class":
            return render_template(f'{panel}es.html')
        return render_template(f'{panel}s.html')

@app.route("/student/filter/<datafilter>", methods=["GET", "POST"])
@token_required
def student_filter_data(datafilter):
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["students_data"]
        student_data = coll.find({"username": id})
        student_data = list(student_data)
        department = student_data[0]["department"]
        classes = student_data[0]["classes"]
        spliting = datafilter.split("-")
        if spliting[0] == "homework":
            condition_dict = {"department_name": department, "class_name": classes}
            coll_name = "homework_data"
            filename = "students/homework.html"
            all_keys, all_values = get_filtered_student_data(app, client, "college_management", coll_name, spliting[1], condition_dict)
        elif spliting[0] == "attendance":
            condition_dict = {"username": id}
            coll_name = "attendance_data"
            filename = "students/attendance.html"
            all_keys, all_values = get_filtered_student_data(app, client, "college_management", coll_name, spliting[1], condition_dict)

        if all_keys and all_values:
            return render_template(filename, all_keys=all_keys[0], all_notifications=get_notifications(id),all_values=all_values, type=type, student_id=id,
                                   photo_link=photo_link)
        else:
            return render_template(filename, all_keys=all_keys, all_notifications=get_notifications(id), all_values=all_values, type=type, student_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('student_filter_data', _external=True, _scheme=secure_type))


########################################## teacher dashboard ##########################################


@app.route("/teacher/delete_data/<object>", methods=["GET", "POST"])
@token_required
def teacher_delete_data(object):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """
    try:
        if "event" in object or "homework" in object:
            spliting_object = object.split("$$$")
        else:
            spliting_object = object.split("-")
        panel = spliting_object[0]
        id = spliting_object[1]
        delete_dict = {}
        if panel == "event":
            delete_dict["event_name"] = id.split("***")[0]
            delete_dict["event_description"] = id.split("***")[-1]
            coll_name = "event_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            return redirect(url_for('teacher_event_data_list', _external=True, _scheme=secure_type))
        elif panel == "homework":
            datasplit = id.split("***")
            title = datasplit[0]
            department_name = datasplit[1]
            class_name = datasplit[2]
            delete_dict["title"] = title
            delete_dict["department_name"] = department_name
            delete_dict["class_name"] = class_name
            coll_name = "homework_data"
            delete_panel_data(app, client, "college_management", coll_name, delete_dict)
            coll_name1 = "homework_comment"
            delete_panel_data(app, client, "college_management", coll_name1, delete_dict)
            return redirect(url_for('teacher_homework_data_list', _external=True, _scheme=secure_type))

    except Exception as e:
        app.logger.debug(f"Error in delete data from database: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('teacher_delete_data', _external=True, _scheme=secure_type))

@app.route("/teacher/export/<object>", methods=["GET", "POST"])
@token_required
def teacher_export_data(object):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        db = client["college_management"]
        spliting_object = object.split("-")
        panel = spliting_object[0]
        type = spliting_object[1]
        if panel == "event":
            res = find_all_data(app, db, "event_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_teacher_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)
        elif panel == "homework":
            res = find_all_data(app, db, "homework_data")
            all_data = []
            for each_res in res:
                del each_res["_id"]
                all_data.append(each_res)
            output_path = export_teacher_panel_data(app, all_data, panel, type)
            return send_file(output_path, as_attachment=True)

    except Exception as e:
        app.logger.debug(f"Error in export data from database: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('teacher_export_data', _external=True, _scheme=secure_type))

@app.route("/teacher/import_data/<panel_obj>", methods=["GET", "POST"])
@token_required
def teacher_import_data(panel_obj):
    """
    That funcation can use delete from student, teacher and admin from admin panel
    """

    try:
        db = client["college_management"]
        if request.method == "POST":
            file = request.files["file"]
            if file.filename != "":
                ## Securing file and getting file extension
                file_name = secure_filename(file.filename)
                if not os.path.isdir(app.config['IMPORT_UPLOAD_FOLDER']):
                    os.makedirs(app.config['IMPORT_UPLOAD_FOLDER'], exist_ok=True)
                if not os.path.exists(app.config['REJECTED_DATA_UPLOAD_FOLDER']):
                    os.makedirs(app.config['REJECTED_DATA_UPLOAD_FOLDER'], exist_ok=True)
                file_path = os.path.join(app.config['IMPORT_UPLOAD_FOLDER'], file_name)
                file.save(file_path)
                file_extension = os.path.splitext(file_name)[1]

                if file_extension == ".xlsx":
                    dataload_excel = pd.read_excel(file_path)
                    json_record_data = dataload_excel.to_json(orient='records')
                    json_data = json.loads(json_record_data)
                elif file_extension == ".csv":
                    dataload = pd.read_csv(file_path)
                    json_record_data = dataload.to_json(orient='records')
                    json_data = json.loads(json_record_data)
                else:
                    with open(file_path, encoding='utf-8') as json_file:
                        json_data = json.load(json_file)


                flag = False
                if panel_obj == "homework":
                    get_homework_data = find_all_data(app, db, "homework_data")
                    all_homework_data = []
                    for homework_data in get_homework_data:
                        di = {}
                        di["title"] = str(homework_data["title"])
                        di["description"] = homework_data["description"]
                        di["department_name"] = homework_data["department_name"]
                        di["class_name"] = homework_data["class_name"]
                        di["subject_name"] = homework_data["subject_name"]
                        all_homework_data.append(di)
                    for record in json_data:
                        make_dict = {}
                        make_dict["title"] = str(record["title"])
                        make_dict["description"] = record["description"]
                        make_dict["department_name"] = record["department_name"]
                        make_dict["class_name"] = record["class_name"]
                        make_dict["subject_name"] = record["subject_name"]
                        if make_dict not in all_homework_data:
                            coll = db["homework_data"]
                            coll.insert_one(record)
                            make_dict["attechment_filepath_list"] = []
                            make_dict["comments"] = []
                            make_dict["inserted_on"] = get_timestamp(app=app)
                            make_dict["updated_on"] = get_timestamp(app=app)
                            coll1 = db["homework_comment"]
                            coll1.insert_one(make_dict)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/teacher/{panel_obj}_data')
                elif panel_obj == "event":
                    get_event_data = find_all_data(app, db, "event_data")
                    all_event_data = []
                    for event_data in get_event_data:
                        di = {}
                        di["event_name"] = event_data["event_name"]
                        di["event_date"] = event_data["event_date"]
                        di["event_time"] = event_data["event_time"]
                        di["teacher_name"] = event_data["teacher_name"]
                        di["classes"] = event_data["classes"]
                        di["event_description"] = event_data["event_description"]
                        all_event_data.append(di)
                    for record in json_data:
                        make_dict = {}
                        make_dict["event_name"] = record["event_name"]
                        make_dict["event_date"] = record["event_date"]
                        make_dict["event_time"] = record["event_time"]
                        make_dict["teacher_name"] = record["teacher_name"]
                        make_dict["classes"] = record["classes"]
                        make_dict["event_description"] = record["event_description"]
                        coll = db["event_data"]
                        if make_dict not in all_event_data:
                            coll.insert_one(record)
                        else:
                            flag=True
                    if flag:
                        flash("Your some data is not valid so that data is not imported..", "warning")
                    return redirect(f'/teacher/{panel_obj}_data')

            else:
                flash("No file selected, please select a file", "danger")
            return render_template(f'teachers/{panel_obj}s.html')

    except Exception as e:
        app.logger.debug(f"Error in export data from database: {e}")
        print(f"Error in export data from database: {e}")
        flash("Please try again...", "danger")
        if panel_obj == "class":
            return render_template(f'teachers/{panel_obj}es.html')
        return render_template(f'teacher/{panel_obj}s.html')

@app.route("/teacher/view_data", methods=["GET", "POST"])
@token_required
def teacher_view_data():
    """
    That funcation can use search data from student, teacher and admin from admin panel
    """
    try:
        data = json.loads(request.data)
        type = data["type"]
        db = client["college_management"]
        if type=="department":
            coll = db["department_data"]
            department_name = data["department_name"]
            all_data = coll.find({"department_name": department_name})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data['department_id'] = all_data_main["department_id"]
            all_type_data['department_name'] = all_data_main["department_name"]
            all_type_data['HOD_name'] = all_data_main["HOD_name"]
            all_type_data['inserted_on'] = all_data_main["inserted_on"]
            all_type_data['updated_on'] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="student":
            coll = db["students_data"]
            username = data["username"]
            all_data = coll.find({"username": username})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["photo_link"] = all_data_main["photo_link"]
            all_type_data["student_id"] = all_data_main["student_id"]
            all_type_data["username"] = all_data_main["username"]
            all_type_data["first_name"] = all_data_main["first_name"]
            all_type_data["last_name"] = all_data_main["last_name"]
            all_type_data["dob"] = all_data_main["dob"]
            all_type_data["gender"] = all_data_main["gender"]
            all_type_data["contact_no"] = all_data_main["contact_no"]
            all_type_data["emergency_contact_no"] = all_data_main["emergency_contact_no"]
            all_type_data["email"] = all_data_main["email"]
            all_type_data["address"] = all_data_main["address"]
            all_type_data["city"] = all_data_main["city"]
            all_type_data["province"] = all_data_main["province"]
            all_type_data["admission_date"] = all_data_main["admission_date"]
            all_type_data["classes"] = all_data_main["classes"]
            all_type_data["department"] = all_data_main["department"]
            all_type_data["batch_year"] = all_data_main["batch_year"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="admin":
            coll = db["admin_data"]
            username = data["username"]
            all_data = coll.find({"username": username})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["photo_link"] = all_data_main["photo_link"]
            all_type_data["admin_id"] = all_data_main["admin_id"]
            all_type_data["username"] = all_data_main["username"]
            all_type_data["first_name"] = all_data_main["first_name"]
            all_type_data["last_name"] = all_data_main["last_name"]
            all_type_data["gender"] = all_data_main["gender"]
            all_type_data["contact_no"] = all_data_main["contact_no"]
            all_type_data["emergency_contact_no"] = all_data_main["emergency_contact_no"]
            all_type_data["email"] = all_data_main["email"]
            all_type_data["address"] = all_data_main["address"]
            all_type_data["city"] = all_data_main["city"]
            all_type_data["province"] = all_data_main["province"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="teacher":
            coll = db["teacher_data"]
            username = data["username"]
            all_data = coll.find({"username": username})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["photo_link"] = all_data_main["photo_link"]
            all_type_data["teacher_id"] = all_data_main["teacher_id"]
            all_type_data["username"] = all_data_main["username"]
            all_type_data["first_name"] = all_data_main["first_name"]
            all_type_data["last_name"] = all_data_main["last_name"]
            all_type_data["dob"] = all_data_main["dob"]
            all_type_data["gender"] = all_data_main["gender"]
            all_type_data["contact_no"] = all_data_main["contact_no"]
            all_type_data["emergency_contact_no"] = all_data_main["emergency_contact_no"]
            all_type_data["email"] = all_data_main["email"]
            all_type_data["address"] = all_data_main["address"]
            all_type_data["city"] = all_data_main["city"]
            all_type_data["province"] = all_data_main["province"]
            all_type_data["qualification"] = all_data_main["qualification"]
            all_type_data["department"] = all_data_main["department"]
            all_type_data["subject"] = all_data_main["subject"]
            all_type_data["joining_date"] = all_data_main["joining_date"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)
        elif type=="subject":
            coll = db["subject_data"]
            username = data["username"]
            all_data = coll.find({"subject_name": username})
            all_data_main = list(all_data)[0]
            all_type_data = {}
            all_type_data["subject_id"] = all_data_main["subject_id"]
            all_type_data["subject_name"] = all_data_main["subject_name"]
            all_type_data["department_name"] = all_data_main["department_name"]
            all_type_data["inserted_on"] = all_data_main["inserted_on"]
            all_type_data["updated_on"] = all_data_main["updated_on"]

            return jsonify(all_type_data)

    except Exception as e:
        app.logger.debug(f"Error in view data from database: {e}")
        flash("Please try again...", "danger")
        return jsonify({"message": "none"})

@app.route("/teacher_dashboard", methods=["GET", "POST"])
@token_required
def teacher_dashboard():
    """
    That funcation can use otp_verification and new_password set link generate
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]

        db = client["college_management"]
        coll = db["teacher_data"]
        teacher_data = coll.find({"username": id})
        teacher_data = list(teacher_data)
        department = teacher_data[0]["department"]
        datashow = []
        upcoming_event = []
        completed_list = []
        coll_event = db["event_data"]
        get_all_data = coll_event.find({}).sort("event_date")
        for data in get_all_data:
            department_list = data["department"]
            del data["_id"]
            del data["inserted_on"]
            del data["updated_on"]
            date_str = f"{data['event_date']} {data['event_time']}"
            date_format = "%d-%m-%Y %H:%M"
            date = datetime.strptime(date_str, date_format)

            # Get the current datetime
            current_datetime = datetime.now()

            # Compare the parsed datetime with the current datetime
            if date < current_datetime:
                completed_list.append(data)
            else:
                upcoming_event.append(data)
            datashow.append(data)

        coll_home = db["homework_data"]
        data_home = coll_home.find({})
        homework_data = list(data_home)

        coll_atten = db["attendance_data"]
        attendance_data = coll_atten.find({"username": id}).sort("_id")
        attendance_data = list(attendance_data)
        attendance_list = []
        for at in attendance_data[:3]:
            del at["_id"]
            attendance_list.append(at)
        attendance_count = len(attendance_data)
        homework_count = len(homework_data)
        event_count = len(datashow)
        return render_template("teachers/teacher-dashboard.html", attendance_list=attendance_list,
                               homework_data=homework_data, all_notifications=get_notifications(id), teacher_id=id,
                               type=type, photo_link=photo_link,
                               attendance_count=attendance_count, upcoming_event=upcoming_event,
                               completed_list=completed_list, homework_count=homework_count, event_count=event_count)

    except Exception as e:
        flash("Please try again...", "danger")
        print(f"Excpetion in teacher dashboard: {e}")
        return redirect(url_for('teacher_dashboard', _external=True, _scheme=secure_type))

@app.route("/teacher/add_event", methods=["GET", "POST"])
@token_required
def teacher_add_event():
    """
    In this route we can handling admin register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        teacher_id = login_dict["id"]
        photo_main_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]

        if request.method == "POST":
            data = json.loads(request.data)
            event_name = data["event_name"]
            event_date = data["event_date"]
            event_time = data["event_time"]
            department_list = data["department_list"]
            event_description = data["event_description"]

            if is_date_passed(event_date):
                flash("Please select correct date... That date is already passed...", "danger")
                return render_template("teacher_id/add-events.html",all_notifications=get_notifications(teacher_id),alldepartment=alldepartment, event_name=event_name, event_description=event_description,
                                       event_date=event_date, event_time=event_time, type=type, photo_link=photo_main_link, teacher_id=teacher_id)
            else:
                mapping_dict = {}
                mapping_dict["event_name"] = event_name
                mapping_dict["event_date"] = event_date
                mapping_dict["event_time"] = event_time
                mapping_dict["department"] = department_list
                mapping_dict["event_description"] = event_description
                mapping_dict["inserted_on"] = get_timestamp(app)
                mapping_dict["updated_on"] = get_timestamp(app)
                data_added(app, db, "event_data", mapping_dict)

                for depart in department_list:
                    all_student_info = find_spec_data(app, db, "students_data", {"department": depart})
                    for student in all_student_info:
                        student_id = student.get("student_id")
                        username = student.get("username")
                        get_notification_data = find_spec_data(app, db, "notification_data",
                                                               {"student_id": student_id, "username": username})
                        get_notification_data = list(get_notification_data)
                        if get_notification_data:
                            notification_list = get_notification_data[0]["notification_list"]
                            notification_list.append(
                                {"msg": f"Event Added: {event_name}", "time": get_timestamp(app), "redirect": "/student/event_data"})
                            update_mongo_data(app, db, "notification_data",
                                              {"student_id": student_id, "username": username},
                                              {"notification_list": notification_list})

                    all_teacher_info = find_spec_data(app, db, "teacher_data", {"department": depart})
                    for teacher in all_teacher_info:
                        teacher_id = teacher.get("teacher_id")
                        username = teacher.get("username")
                        get_notification_data = find_spec_data(app, db, "notification_data",
                                                               {"teacher_id": teacher_id, "username": username})
                        get_notification_data = list(get_notification_data)
                        if get_notification_data:
                            notification_list = get_notification_data[0]["notification_list"]
                            notification_list.append(
                                {"msg": f"Event Added: {event_name}", "time": get_timestamp(app), "redirect": "/teacher/event_data"})
                            update_mongo_data(app, db, "notification_data",
                                              {"teacher_id": teacher_id, "username": username},
                                              {"notification_list": notification_list})

                all_admin_info = find_all_data(app, db, "admin_data")
                for admin in all_admin_info:
                    username = admin.get("username")
                    get_notification_data = find_spec_data(app, db, "notification_data",
                                                           {"username": username})
                    get_notification_data = list(get_notification_data)
                    if get_notification_data:
                        notification_list = get_notification_data[0]["notification_list"]
                        notification_list.append(
                            {"msg": f"Event Added: {event_name}", "time": get_timestamp(app), "redirect": "/admin/event_data"})
                        update_mongo_data(app, db, "notification_data",
                                          {"username": username},
                                          {"notification_list": notification_list})


            flash("Event Added Successfully", "success")
            return jsonify({"messgae":"done"})
        else:
            return render_template("teachers/add-events.html", all_notifications=get_notifications(teacher_id), alldepartment=alldepartment,type=type, photo_link=photo_main_link, teacher_id=teacher_id)

    except Exception as e:
        app.logger.debug(f"Error in add admin data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('teacher_add_event', _external=True, _scheme=secure_type))

@app.route("/teacher/event_data", methods=["GET", "POST"])
@token_required
def teacher_event_data_list():
    """
    That funcation can use show all admins data from admin panel
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["event_data"]
        get_all_data = coll.find({})
        event_names = []
        add_minutes = []
        classname = []
        flags = []
        mapping_dict = {}
        for data in get_all_data:
            del data["_id"]
            event_name = data.get("event_name", "")
            event_date = data.get("event_date", "23-01-2024")
            event_time = data.get("event_time", "00:00")
            current_datetime = datetime.now().strftime("%d-%m-%Y %H:%M")
            target_datetime = f"{event_date} {event_time}"
            mapping_dict[event_name] = data
            remaining_milliseconds, gone = calculate_remaining_milliseconds(current_datetime, target_datetime)
            if gone:
                flag = "true"
                class_name = "bg-success"
            else:
                flag = "false"
                class_name = "bg-purple"

            event_names.append(event_name)
            add_minutes.append(int(remaining_milliseconds))
            classname.append(class_name)
            flags.append(flag)

        decoded_dict = {key: {k: html.unescape(v) for k, v in sub_dict.items()} for key, sub_dict in
                        mapping_dict.items()}

        # Convert Python dictionary to JSON string
        json_data = json.dumps(decoded_dict)
        return render_template("teachers/event.html",all_notifications=get_notifications(id), event_names=event_names, mapping_dict=json_data, type=type, teacher_id=id,
                               photo_link=photo_link, add_minutes=add_minutes, classname=classname, flags=flags)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('teacher_event_data_list', _external=True, _scheme=secure_type))

@app.route("/teacher/add_homework", methods=["GET", "POST"])
@token_required
def teacher_add_homework():
    """
    In this route we can handling admin register process
    :return: register template
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        teacher_id = login_dict["id"]
        photo_main_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]

        subject_data = find_all_data(app, db, "subject_data")
        allsubject = [subject["subject_name"] for subject in subject_data]
        allsubject = list(set(allsubject))

        allclasses = ["A", "B", "C", "D"]

        if request.method == "POST":
            department_name = request.form["department"]
            class_name = request.form["class"]
            subject_name = request.form["subject"]
            title = request.form["title"]
            description = request.form["description"]
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/uploads/homework_attach_file")

            attachment_all_file = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        filename = file.filename
                        ans = checking_upload_folder(app=app, filename=filename)
                        if ans != "duplicate":
                            attach_file_path = os.path.join(attach_save_dir, file.filename)
                            attachment_all_file.append(attach_file_path)
                            file.save(attach_file_path)
                        else:
                            flash('This filename already exits', "danger")
                            return render_template("teachers/add_homework.html", all_notifications=get_notifications(teacher_id),alldepartment=alldepartment,type=type,
                                   photo_link=photo_main_link, teacher_id=teacher_id, allclasses=allclasses,allsubject=allsubject)

            db = client["college_management"]
            comment_di = {}
            comment_di["description"] = description
            comment_di["title"] = title
            comment_di["department_name"] = department_name
            comment_di["class_name"] = class_name
            comment_di["subject_name"] = subject_name

            all_homework_data = find_all_data(app, db, "homework_data")
            li = []
            for homework in all_homework_data:
                del homework["_id"]
                del homework["attechment_filepath_list"]
                del homework["inserted_on"]
                del homework["updated_on"]
                li.append(homework)

            if comment_di not in li:
                data_dict = {"title": title, "description": description, "department_name":department_name, "class_name":class_name,
                             "subject_name":subject_name, "attechment_filepath_list": attachment_all_file,
                             "inserted_on": get_timestamp(app), "updated_on": get_timestamp(app)}

                comment_data_dict = {"title": title, "description": description, "department_name": department_name,
                             "class_name": class_name, "subject_name": subject_name, "attechment_filepath_list": attachment_all_file, "comments":[],
                             "inserted_on": get_timestamp(app), "updated_on": get_timestamp(app)}

                data_added(app, db, "homework_data", data_dict)
                data_added(app, db, "homework_comment", comment_data_dict)
                depart = department_name
                all_student_info = find_spec_data(app, db, "students_data", {"department": depart})
                for student in all_student_info:
                    student_id = student.get("student_id")
                    username = student.get("username")
                    get_notification_data = find_spec_data(app, db, "notification_data",{"student_id": student_id, "username": username})
                    get_notification_data = list(get_notification_data)
                    if get_notification_data:
                        notification_list = get_notification_data[0]["notification_list"]
                        notification_list.append({"msg": f"Homework Added: {title}", "time": get_timestamp(app), "redirect": "/student/homework_data"})
                        update_mongo_data(app, db, "notification_data",{"student_id": student_id, "username": username},{"notification_list": notification_list})

                all_teacher_info = find_spec_data(app, db, "teacher_data", {"department": depart})
                for teacher in all_teacher_info:
                    teacher_id = teacher.get("teacher_id")
                    username = teacher.get("username")
                    get_notification_data = find_spec_data(app, db, "notification_data",{"teacher_id": teacher_id, "username": username})
                    get_notification_data = list(get_notification_data)
                    if get_notification_data:
                        notification_list = get_notification_data[0]["notification_list"]
                        notification_list.append({"msg": f"Homework Added: {title}", "time": get_timestamp(app), "redirect": "/teacher/homework_data"})
                        update_mongo_data(app, db, "notification_data",{"teacher_id": teacher_id, "username": username},{"notification_list": notification_list})

                all_admin_info = find_all_data(app, db, "admin_data")
                for admin in all_admin_info:
                    username = admin.get("username")
                    get_notification_data = find_spec_data(app, db, "notification_data",{"username": username})
                    get_notification_data = list(get_notification_data)
                    if get_notification_data:
                        notification_list = get_notification_data[0]["notification_list"]
                        notification_list.append({"msg": f"Homework Added: {title}", "time": get_timestamp(app), "redirect": "/admin/homework_data"})
                        update_mongo_data(app, db, "notification_data",{"username": username},{"notification_list": notification_list})

                flash("Home work data added successfully...", "success")
                return redirect(url_for('teacher_homework_data_list', _external=True, _scheme=secure_type))
            else:
                flash("That data is already exits...", "danger")
                return redirect(url_for('teacher_add_homework', _external=True, _scheme=secure_type))
        else:
            return render_template("teachers/add_homework.html",all_notifications=get_notifications(teacher_id), alldepartment=alldepartment,type=type,
                                   photo_link=photo_main_link, teacher_id=teacher_id, allclasses=allclasses,allsubject=allsubject)

    except Exception as e:
        app.logger.debug(f"Error in add homework data route: {e}")
        flash("Please try again...", "danger")
        return redirect(url_for('teacher_add_homework', _external=True, _scheme=secure_type))

@app.route("/teacher/homework_data", methods=["GET", "POST"])
@token_required
def teacher_homework_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        all_keys, all_values = get_admin_data(app, client, "college_management", "homework_data")
        if all_keys and all_values:
            return render_template("teachers/homework.html", all_notifications=get_notifications(id),all_keys=all_keys[0], all_values=all_values, type=type,
                                   teacher_id=id, photo_link=photo_link)
        else:
            return render_template("teachers/homework.html",all_notifications=get_notifications(id), all_keys=all_keys, all_values=all_values, type=type, teacher_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('teacher_homework_data_list', _external=True, _scheme=secure_type))

@app.route("/teacher/comment", methods=["GET", "POST"])
@token_required
def teacher_comment_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        data = json.loads(request.data)
        finding_dict = {}
        finding_dict['title'] = data["title"]
        finding_dict['department_name'] = data["department_name"]
        finding_dict['class_name'] = data["class_name"]

        all_data_dict = {'finding_dict':finding_dict, "photo_link": photo_link, "type": type}
        app.config[id] = all_data_dict
        return render_template("teachers/comments.html", all_notifications=get_notifications(id))

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        return redirect(url_for('teacher_comment_data_list', _external=True, _scheme=secure_type))

@app.route("/teacher/data_comment", methods=["GET", "POST"])
@token_required
def teacher_data_comment():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        id = login_dict["id"]
        db = client["college_management"]
        data_show = app.config[id]
        finding_dict = data_show.get("finding_dict", {})
        photo_link = data_show.get("photo_link", "")
        type = data_show.get("type", "")
        comments_data = find_spec_data(app, db, "homework_comment", finding_dict)
        comments_data = list(comments_data)
        comments_data = comments_data[0]
        mapping_list = []
        comment_di = {}
        comment_di["title"] = comments_data.get("title", "")
        comment_di["department_name"] = comments_data.get("department_name", "")
        comment_di["class_name"] = comments_data.get("class_name", "")
        comment_di["subject_name"] = comments_data.get("subject_name", "")
        description = comments_data.get("description", "")
        attechment_list = comments_data.get("attechment_filepath_list", [])
        attechment_main_data = []
        for attechment in attechment_list:
            attechment = attechment.replace("\\", "----")
            attechment_main_data.append(attechment)
        condition_mapping = f'{str(comment_di["title"])}---{str(comment_di["department_name"])}---{str(comment_di["class_name"])}---{str(comment_di["subject_name"])}'
        all_di_list = list(comment_di.keys())
        if attechment_main_data:
            attachment_flag = True
        else:
            attachment_flag = False

        comments_list = comments_data.get("comments", [])
        count_comment = len(comments_list)
        for number in comments_list:
            di = {}
            result = calculate_time_ago(number["time"])
            if result == None:
                result = "now"
            di["image"] = number["image"]
            di["username"] = number["username"]
            di["time"] = number["time"]
            di["comment_data"] = number["comment_data"]
            mapping_list.append(di)
        return render_template("/comments.html", all_notifications=get_notifications(id),condition_mapping=condition_mapping, description=description,
                               all_di_list=all_di_list, comment_di=comment_di, attachment_flag=attachment_flag,
                               type=type, attechment_list=attechment_list, teacher_id=id, photo_link=photo_link,
                               count_comment=count_comment, mapping_list=mapping_list)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('data_comment', _external=True, _scheme=secure_type))

@app.route("/teacher/add_comment", methods=["GET", "POST"])
@token_required
def teacher_add_comment():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        if request.method == "POST":
            data = json.loads(request.data)
            comment_text = data.get("comment", "")
            object_data = data.get("object_data", "")
            spliting = object_data.split("---")
            title = spliting[0]
            department_name = spliting[1]
            class_name = spliting[2]
            subject_name = spliting[3]
            di = {}
            di["image"] = photo_link
            di["username"] = id
            di["time"] = get_timestamp(app)
            di["comment_data"] = comment_text

            condition_dict = {"title": title, "department_name": department_name, "class_name": class_name, "subject_name":subject_name}
            comments_data = find_spec_data(app, db, "homework_comment", condition_dict)
            comments_data = list(comments_data)
            comments_data = comments_data[0]
            comment_data_data = comments_data.get("comments", [])
            comment_data_data.append(di)
            update_mongo_data(app, db, "homework_comment", condition_dict, {"comments":comment_data_data})
            return jsonify({"message": "done"})
        else:
            return jsonify({"message": "done"})

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return jsonify({"message": "failed"})

@app.route("/teacher/personal_attendance_data", methods=["GET", "POST"])
@token_required
def teacher_personal_attendance_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]

        db = client["college_management"]

        condition_dict = {"username": id}
        all_keys, all_values = get_student_data(app, client, "college_management", "attendance_data", condition_dict)
        if all_keys and all_values:
            return render_template("teachers/attendance.html", all_notifications=get_notifications(id),
                                   all_keys=all_keys[0], all_values=all_values, type=type,
                                   student_id=id, photo_link=photo_link)
        else:
            return render_template("teachers/attendance.html", all_notifications=get_notifications(id),
                                   all_keys=all_keys,
                                   all_values=all_values, type=type, student_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show departments data from department panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('student_attendance_data_list', _external=True, _scheme=secure_type))

@app.route("/teacher/search/<object>", methods=["GET", "POST"])
@token_required
def teacher_search_data(object):
    """
    That funcation can use search data from student, teacher and admin from admin panel
    """
    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        if request.method == "POST":
            if object == "homework":
                htmlfilename = "teachers/homework.html"
                condition_dict = {}
                department_name = request.form["department_name"]
                title = request.form["title"]
                class_name = request.form["class_name"]
                subject_name = request.form["subject_name"]
                if department_name:
                    condition_dict["department_name"] = department_name
                if title:
                    condition_dict["title"] = title
                if class_name:
                    condition_dict["class_name"] = class_name
                if subject_name:
                    condition_dict["subject_name"] = subject_name
                all_keys, all_values = get_student_data(app, client, "college_management", "homework_data",
                                                        condition_dict)

                output_list = [list(item) for item in zip(*all_values)]

                searched_value = []
                for search_text in all_values:
                    title = search_text[0]
                    department_name = search_text[1]
                    class_name = search_text[2]
                    subject_name = search_text[3]
                    if title in output_list[0] or department_name in output_list[1] or class_name in output_list[
                        2] or subject_name in output_list[3]:
                        searched_value.append(search_text)

            elif object == "attendance":
                htmlfilename = "teachers/attendance.html"
                condition_dict = {}
                attendance_date_text = request.form["attendance_date"]
                datasplit = attendance_date_text.split("-")
                attendance_date = "-".join(datasplit[::-1])
                status = request.form["status"]
                reason = request.form["reason"]
                condition_dict["username"] = id
                if attendance_date:
                    condition_dict["attendance_date"] = attendance_date
                if status:
                    condition_dict["status"] = status
                if reason:
                    condition_dict["reason"] = reason
                all_keys, searched_value = get_student_data(app, client, "college_management", "attendance_data",
                                                            condition_dict)

            if all_keys and searched_value:
                return render_template(htmlfilename, all_notifications=get_notifications(id),all_keys=all_keys[0], all_values=searched_value,
                                       type=type, teacher_id=id, photo_link=photo_link, search="true")
            else:
                return render_template(htmlfilename, all_notifications=get_notifications(id), all_keys=all_keys, all_values=searched_value,
                                       type=type, teacher_id=id, photo_link=photo_link, search="true")

    except Exception as e:
        app.logger.debug(f"Error in searching data from database: {e}")
        print("Error in searching data", e)
        flash("Please try again...", "danger")
        panel = object
        if panel == "class":
            return render_template(f'{panel}es.html')
        return render_template(f'{panel}s.html')

@app.route("/teacher/filter/<datafilter>", methods=["GET", "POST"])
@token_required
def teacher_filter_data(datafilter):
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        coll = db["teacher_data"]
        teacher_data = coll.find({"username": id})
        teacher_data = list(teacher_data)
        department = teacher_data[0]["department"]
        spliting = datafilter.split("-")
        if spliting[0] == "homework":
            condition_dict = {}
            coll_name = "homework_data"
            filename = "teachers/homework.html"
            all_keys, all_values = get_filtered_student_data(app, client, "college_management", coll_name, spliting[1],
                                                             condition_dict)
        elif spliting[0] == "attendance":
            condition_dict = {"username": id}
            coll_name = "attendance_data"
            filename = "teachers/attendance.html"
            all_keys, all_values = get_filtered_student_data(app, client, "college_management", coll_name, spliting[1],
                                                             condition_dict)

        if all_keys and all_values:
            return render_template(filename, all_keys=all_keys[0], all_notifications=get_notifications(id),
                                   all_values=all_values, type=type, teacher_id=id,
                                   photo_link=photo_link)
        else:
            return render_template(filename, all_keys=all_keys, all_notifications=get_notifications(id),
                                   all_values=all_values, type=type, teacher_id=id,
                                   photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in show admins data from admin panel: {e}")
        flash("Please try again..", "danger")
        return redirect(url_for('teacher_filter_data', _external=True, _scheme=secure_type))

@app.route("/teacher/attendance_data", methods=["GET", "POST"])
@token_required
def teacher_attendance_data_list():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        login_dict = session.get("login_dict", "nothing")
        type = login_dict["type"]
        teacher_id = login_dict["id"]
        photo_link = "/" + login_dict["photo_link"]
        db = client["college_management"]
        department_data = find_all_data(app, db, "department_data")
        alldepartment = [department["department_name"] for department in department_data]
        student_data = find_all_data(app, db, "students_data")
        allstudent = [stu["username"] for stu in student_data]
        teacher_data = find_all_data(app, db, "teacher_data")
        allteacher = [tea["username"] for tea in teacher_data]
        if request.method=="POST":
            panel = request.args.get("panel", "student")
            if 'add' in request.form:
                if panel=="student":
                    department = request.form["department"]
                    classes = request.form["class"]
                    attendance_date = request.form["date"]
                    all_spec_students = find_spec_data(app, db, "students_data", {"department": department, "classes": classes})
                    all_spec_students = list(all_spec_students)
                    all_student_username = [students["username"] for students in all_spec_students]
                    return render_template("teachers/student_attendance.html", alldepartment=alldepartment,department=department, classes=classes,
                                           allteacher=allteacher, allstudent=allstudent, type=type, teacher_id=teacher_id,datakey="yes",
                                           photo_link=photo_link,all_student_username=all_student_username,attendance_date=attendance_date)

                elif panel=="teacher":
                    department = request.form["department"]
                    attendance_date = request.form["date"]
                    all_spec_teachers = find_spec_data(app, db, "teacher_data", {"department": department})
                    all_spec_teachers = list(all_spec_teachers)
                    all_teacher_username = [teachers["username"] for teachers in all_spec_teachers]
                    return render_template("teachers/teacher_attendance.html", alldepartment=alldepartment,department=department,
                                       allteacher=allteacher, allstudent=allstudent, type=type, teacher_id=teacher_id,datakey="yes",
                                       photo_link=photo_link,all_teacher_username=all_teacher_username, attendance_date=attendance_date)
            elif "view" in request.form:
                if panel=="student":
                    department = request.form["department"]
                    classes = request.form["class"]
                    attendance_date = request.form["date"]
                    all_spec_students = find_spec_data(app, db, "attendance_data", {"department": department, "classes": classes, "attendance_date":attendance_date, "type": "student"})
                    all_spec_students = list(all_spec_students)
                    return render_template("admins/student_attendance.html", alldepartment=alldepartment, department=department,classes=classes,
                                           allteacher=allteacher, allstudent=allstudent, type=type, teacher_id=teacher_id,
                                           datakey="no", photo_link=photo_link, all_student_username=all_spec_students,
                                           attendance_date=attendance_date)
                elif panel=="teacher":
                    department = request.form["department"]
                    attendance_date = request.form["date"]
                    all_spec_teachers = find_spec_data(app, db, "attendance_data", {"department": department, "attendance_date":attendance_date, "type": "teacher"})
                    all_spec_teachers = list(all_spec_teachers)
                    return render_template("admins/teacher_attendance.html", alldepartment=alldepartment, department=department,
                                           allteacher=allteacher, allstudent=allstudent, type=type, teacher_id=teacher_id,
                                           datakey="no",photo_link=photo_link, all_teacher_username=all_spec_teachers,
                                           attendance_date=attendance_date)

        return render_template("admins/teacher_attendance.html", alldepartment=alldepartment,
                           allteacher=allteacher, allstudent=allstudent, type=type, teacher_id=teacher_id, photo_link=photo_link)

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))

@app.route("/teacher/add_attendance", methods=["GET", "POST"])
@token_required
def teacher_add_attendance():
    """
    That funcation can use show all admins data from admin panel
    """

    try:
        db = client["college_management"]
        coll = db["attendance_data"]
        if request.method=="POST":
            panel = request.args.get("panel", "student")
            if panel == "teacher":
                data = json.loads(request.data)
                mapping_dict = data["get_data"]
                attendance_date = mapping_dict[0]["attendance_date"]
                all_data = coll.find({"attendance_date": attendance_date, "type": "teacher"})
                all_data_main = [[data_main["attendance_date"], data_main["username"]] for data_main in all_data]
                for var in mapping_dict:
                    li = [var["attendance_date"], var["username"]]
                    if li not in all_data_main:
                        var["inserted_on"] = get_timestamp(app)
                        var["updated_on"] = get_timestamp(app)
                        coll.insert_one(var)

                return jsonify({"status": 200})
            elif panel=="student":
                data = json.loads(request.data)
                mapping_dict = data["get_data"]
                attendance_date = mapping_dict[0]["attendance_date"]
                all_data = coll.find({"attendance_date": attendance_date, "type": "student"})
                all_data_main = [[data_main["attendance_date"], data_main["username"]] for data_main in all_data]
                for var in mapping_dict:
                    li = [var["attendance_date"], var["username"]]
                    if li not in all_data_main:
                        var["inserted_on"] = get_timestamp(app)
                        var["updated_on"] = get_timestamp(app)
                        coll.insert_one(var)
                return jsonify({"status": 200})

    except Exception as e:
        app.logger.debug(f"Error in add teacher data route: {e}")
        return redirect(url_for('email_sending', _external=True, _scheme=secure_type))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=81, debug=True)
