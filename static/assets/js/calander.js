$(document).ready(function () {
	$("#calendar-doctor").simpleCalendar({
		fixedStartDay: 0, // begin weeks by sunday
		disableEmptyDetails: true,
		events: [
			// generate new event after tomorrow for one hour
			{
				startDate: new Date(new Date().setHours(new Date().getHours() + 24)).toDateString(),
				endDate: new Date(new Date().setHours(new Date().getHours() + 25)).toISOString(),
				summary: 'Conference with teachers'
			},
			{
				startDate: new Date(new Date().setHours(new Date().getHours() + 32)).toDateString(),
				endDate: new Date(new Date().setHours(new Date().getHours() + 45)).toISOString(),
				summary: 'Conference with students'
			},
			{
				startDate: new Date(new Date().setHours(new Date().getHours() + 60)).toDateString(),
				endDate: new Date(new Date().setHours(new Date().getHours() + 65)).toISOString(),
				summary: 'Conference with departments'
			},
			// generate new event for yesterday at noon
			{
				startDate: new Date(new Date().setHours(new Date().getHours() - new Date().getHours() - 12, 0)).toISOString(),
				endDate: new Date(new Date().setHours(new Date().getHours() - new Date().getHours() - 11)).getTime(),
				summary: 'Old classes'
			},
			// generate new event for the last two days
			{
				startDate: new Date(new Date().setHours(new Date().getHours() - 48)).toISOString(),
				endDate: new Date(new Date().setHours(new Date().getHours() - 24)).getTime(),
				summary: 'Old Lessons'
			},
			{
				startDate: new Date(new Date().setHours(new Date().getHours() + 18)).toISOString(),
				endDate: new Date(new Date().setHours(new Date().getHours() + 21)).getTime(),
				summary: 'Old Lessons data'
			},
			{
				startDate: new Date(new Date().setHours(new Date().getHours() + 10)).toISOString(),
				endDate: new Date(new Date().setHours(new Date().getHours() + 12)).getTime(),
				summary: 'Old Lessons data1'
			},
			{
				startDate: new Date(new Date().setHours(new Date().getHours() + 3)).toISOString(),
				endDate: new Date(new Date().setHours(new Date().getHours() + 8)).getTime(),
				summary: 'Old Lessons data2'
			}
		],

	});
});