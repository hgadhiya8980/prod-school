/*
Author       : Dreamguys
Template Name: Preskool - Bootstrap Admin Template
Version      : 1.0
*/

function hide_button() {
	document.getElementById('remove_tag').style.visibility = 'hidden';
}

(function($) {
    "use strict";
	
	// Variables declarations
	
	var $wrapper = $('.main-wrapper');
	var $pageWrapper = $('.page-wrapper');
	var $slimScrolls = $('.slimscroll');
	
	// Sidebar
	
	var Sidemenu = function() {
		this.$menuItem = $('#sidebar-menu a');
	};

	function init() {
		var $this = Sidemenu;
		$('#sidebar-menu a').on('click', function(e) {
			if($(this).parent().hasClass('submenu')) {
				e.preventDefault();
			}
			if(!$(this).hasClass('subdrop')) {
				$('ul', $(this).parents('ul:first')).slideUp(350);
				$('a', $(this).parents('ul:first')).removeClass('subdrop');
				$(this).next('ul').slideDown(350);
				$(this).addClass('subdrop');
			} else if($(this).hasClass('subdrop')) {
				$(this).removeClass('subdrop');
				$(this).next('ul').slideUp(350);
			}
		});
		$('#sidebar-menu ul li.submenu a.active').parents('li:last').children('a:first').addClass('active').trigger('click');
	}
	
	// Sidebar Initiate
	init();
	
	// Mobile menu sidebar overlay
	
	$('body').append('<div class="sidebar-overlay"></div>');
	$(document).on('click', '#mobile_btn', function() {
		$wrapper.toggleClass('slide-nav');
		$('.sidebar-overlay').toggleClass('opened');
		$('html').addClass('menu-opened');
		return false;
	});
	
	// toggle-password
	
	if($('.toggle-password').length > 0) {
		$(document).on('click', '.toggle-password', function() {
			$(this).toggleClass("feather-eye feather-eye-off");
			var input = $(".pass-input");
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	}
	if($('.reg-toggle-password').length > 0) {
		$(document).on('click', '.reg-toggle-password', function() {
			$(this).toggleClass("feather-eye feather-eye-off");
			var input = $(".pass-confirm");
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});
	}
	
	// Sidebar overlay
	
	$(".sidebar-overlay").on("click", function () {
		$wrapper.removeClass('slide-nav');
		$(".sidebar-overlay").removeClass("opened");
		$('html').removeClass('menu-opened');
	});
	
	// Logo Hide Btn

	$(document).on("click",".logo-hide-btn",function () {
		$(this).parent().hide();
	});
	
	// Page Content Height
	
	if($('.page-wrapper').length > 0 ){
		var height = $(window).height();	
		$(".page-wrapper").css("min-height", height);
	}


	
	// Page Content Height Resize
	
	$(window).resize(function(){
		if($('.page-wrapper').length > 0 ){
			var height = $(window).height();
			$(".page-wrapper").css("min-height", height);
		}
	});
	
	// Select 2
	
	if ($('.select').length > 0) {
		$('.select').select2({
			minimumResultsForSearch: -1,
			width: '100%'
		});
	}
	
	// editor
	if ($('#editor').length > 0) {
		ClassicEditor
		.create( document.querySelector( '#editor' ), {
			toolbar: {
                items: [
                    'heading', '|',
                    'fontfamily', 'fontsize', '|',
                    'alignment', '|',
                    'fontColor', 'fontBackgroundColor', '|',
                    'bold', 'italic', 'strikethrough', 'underline', 'subscript', 'superscript', '|',
                    'link', '|',
                    'outdent', 'indent', '|',
                    'bulletedList', 'numberedList', 'todoList', '|',
                    'code', 'codeBlock', '|',
                    'insertTable', '|',
                    'uploadImage', 'blockQuote', '|',
                    'undo', 'redo'
                ],
                shouldNotGroupWhenFull: true
            }
		} )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
	}
	
	// Experience Add More
	
    $(".settings-form").on('click','.trash', function () {
		$(this).closest('.links-cont').remove();
		return false;
    });

    $(document).on("click",".add-links",function () {
		var experiencecontent = '<div class="row form-row links-cont">' +
			'<div class="form-group d-flex">' +
				'<button class="btn social-icon"><i class="feather-github"></i></button>' +
				'<input type="text" class="form-control" placeholder="Social Link">' +
				'<div><a href="#" class="btn trash"><i class="feather-trash-2"></i></a></div>' +
			'</div>' +
		'</div>';
		
        $(".settings-form").append(experiencecontent);
        return false;
    });
	
	// Datetimepicker
	
	if($('.datetimepicker').length > 0 ){
		$('.datetimepicker').datetimepicker({
			format: 'DD-MM-YYYY',
			icons: {
				up: "fas fa-angle-up",
				down: "fas fa-angle-down",
				next: 'fas fa-angle-right',
				previous: 'fas fa-angle-left'
			}
		});
		$('.datetimepicker').on('dp.show',function() {
			$(this).closest('.table-responsive').removeClass('table-responsive').addClass('temp');
		}).on('dp.hide',function() {
			$(this).closest('.temp').addClass('table-responsive').removeClass('temp')
		});
	}

	// Tooltip
	
	if($('[data-toggle="tooltip"]').length > 0 ){
		$('[data-toggle="tooltip"]').tooltip();
	}
	
    // Datatable

	if ($('.datatable').length > 0) {
        $('.datatable').DataTable({
            "bFilter": false,
        });
    }
    if ($('.datatables').length > 0) {
        $('.datatables').DataTable({
            "bFilter": true,
        });
    }
	
	// Zoom in
	
	if($('.zoom-screen .header-nav-list').length > 0) {
        $('.zoom-screen .header-nav-list').on('click', function(e){
            if (!document.fullscreenElement) {
                document.documentElement.requestFullscreen();
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                }
            }
        })
    }

	// Check all email
	
	$(document).on('click', '#check_all', function() {
		$('.checkmail').click();
		return false;
	});
	if($('.checkmail').length > 0) {
		$('.checkmail').each(function() {
			$(this).on('click', function() {
				if($(this).closest('tr').hasClass('checked')) {
					$(this).closest('tr').removeClass('checked');
				} else {
					$(this).closest('tr').addClass('checked');
				}
			});
		});
	}
	
	// Mail important
	
	$(document).on('click', '.mail-important', function() {
		$(this).find('i.fa').toggleClass('fa-star').toggleClass('fa-star-o');
	});
	
	// Summernote
	
	if($('.summernote').length > 0) {
		$('.summernote').summernote({
			height: 300,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			focus: false                 // set focus to editable area after initializing summernote
		});
	}
	
	
	// Sidebar Slimscroll

	if($slimScrolls.length > 0) {
		$slimScrolls.slimScroll({
			height: 'auto',
			width: '100%',
			position: 'right',
			size: '7px',
			color: '#ccc',
			allowPageScroll: false,
			wheelStep: 10,
			touchScrollStep: 100
		});
		var wHeight = $(window).height() - 60;
		$slimScrolls.height(wHeight);
		$('.sidebar .slimScrollDiv').height(wHeight);
		$(window).resize(function() {
			var rHeight = $(window).height() - 60;
			$slimScrolls.height(rHeight);
			$('.sidebar .slimScrollDiv').height(rHeight);
		});
	}
	
	// Small Sidebar

	$(document).on('click', '#toggle_btn', function() {
		if($('body').hasClass('mini-sidebar')) {
			$('body').removeClass('mini-sidebar');
			$('.subdrop + ul').slideDown();
		} else {
			$('body').addClass('mini-sidebar');
			$('.subdrop + ul').slideUp();
		}
		setTimeout(function(){ 
			
		}, 300);
		return false;
	});
	$(document).on('mouseover', function(e) {
		e.stopPropagation();
		if($('body').hasClass('mini-sidebar') && $('#toggle_btn').is(':visible')) {
			var targ = $(e.target).closest('.sidebar').length;
			if(targ) {
				$('body').addClass('expand-menu');
				$('.subdrop + ul').slideDown();
			} else {
				$('body').removeClass('expand-menu');
				$('.subdrop + ul').slideUp();
			}
			return false;
		}
	});

	// Circle Progress Bar
	function animateElements() {
		$('.circle-bar1').each(function () {
			var elementPos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var percent = $(this).find('.circle-graph1').attr('data-percent');
			var animate = $(this).data('animate');
			if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
				$(this).data('animate', true);
				$(this).find('.circle-graph1').circleProgress({
					value: percent / 100,
					size : 400,
					thickness: 30,
					fill: {
						color: '#6e6bfa'
					}
				});
			}
		});
		$('.circle-bar2').each(function () {
			var elementPos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var percent = $(this).find('.circle-graph2').attr('data-percent');
			var animate = $(this).data('animate');
			if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
				$(this).data('animate', true);
				$(this).find('.circle-graph2').circleProgress({
					value: percent / 100,
					size : 400,
					thickness: 30,
					fill: {
						color: '#6e6bfa'
					}
				});
			}
		});
		$('.circle-bar3').each(function () {
			var elementPos = $(this).offset().top;
			var topOfWindow = $(window).scrollTop();
			var percent = $(this).find('.circle-graph3').attr('data-percent');
			var animate = $(this).data('animate');
			if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
				$(this).data('animate', true);
				$(this).find('.circle-graph3').circleProgress({
					value: percent / 100,
					size : 400,
					thickness: 30,
					fill: {
						color: '#6e6bfa'
					}
				});
			}
		});
	}	
	
	if($('.circle-bar').length > 0) {
		animateElements();
	}
	$(window).scroll(animateElements);
	
	// Preloader
	
	$(window).on('load', function () {
		if($('#loader').length > 0) {
			$('#loader').delay(350).fadeOut('slow');
			$('body').delay(350).css({ 'overflow': 'visible' });
		}
	})
	
	// Checkbox Select
	
	$('.app-listing .selectBox').on("click", function() {
        $(this).parent().find('#checkBoxes').fadeToggle();
        $(this).parent().parent().siblings().find('#checkBoxes').fadeOut();
    });

    $('.invoices-main-form .selectBox').on("click", function() {
        $(this).parent().find('#checkBoxes-one').fadeToggle();
        $(this).parent().parent().siblings().find('#checkBoxes-one').fadeOut();
    });

	//Checkbox Select
	
	if($('.SortBy').length > 0) {
		var show = true;
		var checkbox1 = document.getElementById("checkBox");
		$('.selectBoxes').on("click", function() {
			
			if (show) {
				checkbox1.style.display = "block";
				show = false;
			} else {
				checkbox1.style.display = "none";
				show = true;
			}
		});		
	}

	// Invoices Checkbox Show

	$(function() {
		$("input[name='invoice']").click(function() {
			if ($("#chkYes").is(":checked")) {
				$("#show-invoices").show();
			} else {
				$("#show-invoices").hide();
			}
		});
	});

	// Invoices Add More
	
    $(".links-info-one").on('click','.service-trash', function () {
		$(this).closest('.links-cont').remove();
		return false;
    });

    $(document).on("click",".add-links",function () {
		var experiencecontent = '<div class="links-cont">' +
			'<div class="service-amount">' +
				'<a href="#" class="service-trash"><i class="fe fe-minus-circle me-1"></i>Service Charge</a> <span>$ 4</span' +
			'</div>' +
		'</div>';
		
        $(".links-info-one").append(experiencecontent);
        return false;
    });

     $(".links-info-discount").on('click','.service-trash-one', function () {
		$(this).closest('.links-cont-discount').remove();
		return false;
    });

    $(document).on("click",".add-links-one",function () {
		var experiencecontent = '<div class="links-cont-discount">' +
			'<div class="service-amount">' +
				'<a href="#" class="service-trash-one"><i class="fe fe-minus-circle me-1"></i>Offer new</a> <span>$ 4 %</span' +
			'</div>' +
		'</div>';
		
        $(".links-info-discount").append(experiencecontent);
        return false;
    });
	
	// Form Wizard
	
	$(".seller-next-btn").on('click', function () { // Function Runs On NEXT Button Click
		$(this).closest('.tab-pane').next().css("display" , "block").css("opacity" , "1");
		$(this).closest('.tab-pane').css({
			'display': 'none'
		});
		// Adding Class Active To Show Steps Forward;
		$('#basic-pills-wizard .nav-item.active').removeClass('active').addClass('activated').next().addClass('active');
	});
	$(".seller-previous-btn").on('click', function () { // Function Runs On NEXT Button Click
		$(this).closest('.tab-pane').prev().css("display", "block");
		$(this).closest('.tab-pane').css({
			'display': 'none'
		});
		// Adding Class Active To Show Steps Forward;
		$('#basic-pills-wizard .nav-item.active').removeClass('active').prev().removeClass('activated').addClass('active');
	});

	// Summernote
	
	if($('#summernote').length > 0) {
        $('#summernote').summernote({
		  height: 300,                 // set editor height
		  minHeight: null,             // set minimum height of editor
		  maxHeight: null,             // set maximum height of editor
		  focus: true                  // set focus to editable area after initializing summernote
		});
    }
	
	// Counter 
	
	if($('.counter').length > 0) {
	   $('.counter').counterUp({
			delay: 20,
            time: 2000
       });
	}
	
	if($('#timer-countdown').length > 0) {
		$( '#timer-countdown' ).countdown( {
			from: 180, // 3 minutes (3*60)
			to: 0, // stop at zero
			movingUnit: 1000, // 1000 for 1 second increment/decrements
			timerEnd: undefined,
			outputPattern: '$day Day $hour : $minute : $second',
			autostart: true
		});
	}
	
	if($('#timer-countup').length > 0) {
		$( '#timer-countup' ).countdown( {
			from: 0,
			to: 180 
		});
	}
	
	if($('#timer-countinbetween').length > 0) {
		$( '#timer-countinbetween' ).countdown( {
			from: 30,
			to: 20 
		});
	}
	
	if($('#timer-countercallback').length > 0) {
		$( '#timer-countercallback' ).countdown( {
			from: 10,
			to: 0,
			timerEnd: function() {
				this.css( { 'text-decoration':'line-through' } ).animate( { 'opacity':.5 }, 500 );
			} 
		});
	}
	
	if($('#timer-outputpattern').length > 0) {
		$( '#timer-outputpattern' ).countdown( {
			outputPattern: '$day Days $hour Hour $minute Min $second Sec..',
			from: 60 * 60 * 24 * 3
		});
	}
	
	// Tooltip
	
	if($('[data-bs-toggle="tooltip"]').length > 0) {
		var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
		var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		  return new bootstrap.Tooltip(tooltipTriggerEl)
		})
	}
	
	// Popover
	
	if($('.popover-list').length > 0) {
		var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
		var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
		  return new bootstrap.Popover(popoverTriggerEl)
		})
	}
	
	// Clipboard 
	
	if($('.clipboard').length > 0) {
		var clipboard = new Clipboard('.btn');
	}

    // Invoices Table Add More
	
    $(".add-table-items").on('click','.remove-btn', function () {
		$(this).closest('.add-row').remove();
		return false;
    });

    $(document).on("click",".add-btn",function () {
		var experiencecontent = '<tr class="add-row">' +
			'<td>' +
				'<input type="text" class="form-control">' +
			'</td>' +
			'<td>' +
				'<input type="text" class="form-control">' +
			'</td>' +
			'<td>' +
				'<input type="text" class="form-control">' +
			'</td>' +
			'<td>' +
				'<input type="text" class="form-control">' +
			'</td>' +
			'<td>' +
				'<input type="text" class="form-control">' +
			'</td>' +
			'<td>' +
				'<input type="text" class="form-control">' +
			'</td>' +
			'<td class="add-remove text-end">' +
				'<a href="javascript:void(0);" class="add-btn me-2"><i class="fas fa-plus-circle"></i></a> ' +
				'<a href="#" class="copy-btn me-2"><i class="fe fe-copy"></i></a>' +
				'<a href="javascript:void(0);" class="remove-btn"><i class="fe fe-trash-2"></i></a>' +
			'</td>' +
		'</tr>';
		
        $(".add-table-items").append(experiencecontent);
        return false;
    });
		
	feather.replace();
	
	
})(jQuery);

function googleTranslateElementInit() {
	new google.translate.TranslateElement(
		{pageLanguage: 'en', includedLanguages: "nl,en,fy"},
		'google_translate_element'
	);
}

const myTimeout = setTimeout(hide_button, 5000);

function hide_button() {
			document.getElementById('remove_tag').style.display = 'none';
		}

function opentargeturl(targeturl) {
    window.location.href=targeturl;
}

document.addEventListener("DOMContentLoaded", function () {
    const company_logo = "https://rylee.in/images/logo.png";
    const bg_color = "#333333";
    const companyname = "Rylee";
    // Please note please add only google font icon in here
    const chaticon = '<span class="material-icons">smart_toy</span>';
    const roboticon = '<span class="material-icons">smart_toy</span>';
    // end of google font icon section
    const all_content = "Please enter your company content";
    const logo_height = "55px";
    const logo_width = "55px";
    const marquee_bg = "#333";
    const marquee_color = "#fff";
    const db_name = "chatbot_developement";
    const coll_name = "conversational_data";

    // Create a new link element
    const linkElement = document.createElement('link');

    // Set the attributes for the link element
    linkElement.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';
    linkElement.rel = 'stylesheet';

    // Append the link element to the document's head
    document.head.appendChild(linkElement);

    const styleTag = document.createElement('style');

    // Set the type attribute to "text/css" (required for compatibility)
    styleTag.type = 'text/css';
    const cssContent = `
        @font-face {
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 400;
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        src: url(https://example.com/MaterialIcons-Regular.eot); /* For IE6-8 */
        src: local('Material Icons'),
          local('MaterialIcons-Regular'),
          url(https://example.com/MaterialIcons-Regular.woff2) format('woff2'),
          url(https://example.com/MaterialIcons-Regular.woff) format('woff'),
          url(https://example.com/MaterialIcons-Regular.ttf) format('truetype');
        }

        body {
        font-family: 'Poppins', sans-serif;
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        }
        .chatbot-toggler {
        position: fixed;
        right: 40px;
        bottom: 35px;
        height: 50px;
        width: 50px;
        z-index: 1;
        color: #fff;
        border: none;
        display: flex;
        align-items: center;
        justify-content: center;
        outline: none;
        cursor: pointer;
        background: #3D5EE1;
        border-radius: 50%;
        transition: all 0.2s ease;
        }

        .chatbot-togg {
        position: fixed;
        right: 100px;
        visibility: visible;
        bottom: 20px;
        padding: 0 20px;
        animation: fadeInUp 3s;
        height: 50px;
        min-width: 150px;
        color: #fff;
        border: none;
        display: flex;
        align-items: center;
        justify-content: center;
        outline: none;
        border-radius: 10px;
        cursor: pointer;
        background: #3D5EE1;
        transition: all 0.2s ease;
        }
        .show-chatbot .chatbot-toggler {
        transform: rotate(90deg);
        }
        .chatbot-toggler span {
        position: absolute;
        }
        .show-chatbot .chatbot-toggler span:first-child,
        .chatbot-toggler span:last-child {
        opacity: 0;
        }
        .show-chatbot .chatbot-toggler span:last-child {
        opacity: 1;
        }
        .chatbot {
        position: fixed;
        right: 40px;
        bottom: 100px;
        width: 420px;
        height: 70%;
        z-index: 999;
        transform: scale(0.5);
        opacity: 0;
        pointer-events: none;
        overflow: hidden;
        background: #fff;
        border-radius: 15px;
        transform-origin: bottom right;
        box-shadow: 0 0 128px 0 rgba(0, 0, 0, 0.1),
                    0 32x 64px -48px rgba(0,0,0,0.5);
        transition: all 0.1s ease;
        }
        .show-chatbot .chatbot {
        transform: scale(1);
        opacity: 1;
        pointer-events: auto;
        }
        .chatbot header {
        background: #333333;
        height: 15%;
        overflow: hidden;
        box-sizing: border-box;
        padding: 8px 0;
        text-align: center;
        position: relative;
        }
        .chatbot header h2 {
        color: #fff;
        font-size: 1.6rem;
        }
        .chatbot header span {
        position: absolute;
        right: 20px;
        top: 50%;
        color: #fff;
        cursor: pointer;
        display: none;
        transform: translateY(-50);
        }
        .chatbot .chatbox {
        height: 67%;
        overflow: hidden;
        z-index: 1;
        box-sizing: border-box;
        overflow-y: auto;
        padding: 30px 20px 70px;
        }
        .chatbox .chat {
        display: flex;
        }
        .chatbox .incoming span {
        height: 32px;
        width: 32px;
        color: #fff;
        align-self: flex-end;
        background: #333333;
        text-align: center;
        line-height: 32px;
        border-radius: 4px;
        margin: 0 10px 7px 0;
        }
        .chatbox .outgoing {
        margin: 20px 0;
        justify-content: flex-end;
        }
        .chatbox .chat p {
        color: #fff;
        font-size: 0.95rem;
        max-width: 75%;
        white-space: pre-wrap;
        margin: 0;
        padding: 12px 16px;
        border-radius: 10px 10px 0 10px;
        background: #333333;
        }
        .chatbox .chat p.error {
        color: #721c24;
        background: #f8d7da;
        }
        .chatbot .incoming p {
        color: #000;
        background: #f2f2f2;
        line-height: 24px;
        border-radius: 10px 10px 10px 0;
        }
        .chatbot .chat-input {
        position: absolute;
        display: none;
        bottom: 0;
        width: 100%;
        height: 13%;
        display: flex;
        gap: 5px;
        background: #fff;
        padding: 5px 20px;
        border-top: 1px solid #ccc;
        }
        .chat-input textarea {
        height: 40px;
        display: none;
        width: 100%;
        border: none;
        outline: none;
        overflow: hidden;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 1rem;
        resize: none;
        padding: 16px 15px 16px 0;
        }
        .chat-input span {
        align-self: flex-end;
        height: 55px;
        display: none;
        line-height: 55px;
        color: #991c47;
        font-size: 1.35rem;
        cursor: pointer;
        visibility: hidden;
        }
        .chat-input textarea:valid ~ span {
        display: none;
        }

        @media(max-width: 490px) {
        .chatbot {
            right: 0;
            bottom: 0;
            width: 100%;
            height: 70%;
            border-radius: 0;
        }
        .chatbot .chatbox {
            height: 70%;
        }
        .chatbot header span {
            display: block;
        }
        }
        `;
    styleTag.appendChild(document.createTextNode(cssContent));

    // Add the <style> element to the <head>
    document.head.appendChild(styleTag);

    const newDiv = document.createElement('div');

    var tag_name_add = "";
    
    console.log(all_user_names_list);
    all_user_names_list = all_user_names_list.replaceAll('&#39;', '"')
    console.log(all_user_names_list);
    var allusers_list = JSON.parse(all_user_names_list);
    console.log(allusers_list);
    for (var i=0; i<allusers_list.length; i++) {
        tag_name_add = tag_name_add + "<option>"+ allusers_list[i] + "</option>"
    }
    console.log(tag_name_add);

    const htmlContent = `
    <button class="chatbot-toggler">
        <span class="material-icons">smart_toy</span>
        <span class="material-icons">close</span>
    </button>
    <div class="chatbot">
        <header style="color: #fff; font-size: 25px; padding: 20px; height: 70px; box-sizing: border-box;">
            Feedback Form
            <span class="close-btn material-icons">close</span>
        </header>
        <div class="data" style="overflow-y: scroll; height: 90%; background-color: #F0F2F5;">
            <form id="uploadForm" method="POST" enctype="multipart/form-data" style="padding: 20px 30px;">
                <div class="form-group">
                    <label for="user" style="color: #000; font-family: "Roboto" font-weight: 500;>Username</label>
                    <select name="username_msg" for="user" id="usernameinput" class="form-control select" style="background-color: #fff; padding: 10px; height: 50px; margin-bottom: 25px;" required>
                    ${tag_name_add}
                    </select>
                </div>
                <div class="form-group">
                    <label for="subject" style="color: #000; font-family: "Roboto" font-weight: 500;">Subject</label>
                    <input class="form-control" for="subject" name="subject_msg" id="subjectinput" type="text" style="background-color: #fff; padding: 10px; height: 50px; margin-bottom: 25px;" required>
                </div>
                <div class="form-group">
                    <label for="msg" style="color: #000; font-family: "Roboto" font-weight: 500;"> Feedback Message </label>
                    <textarea for="msg" id="feedbackinput" rows=2 name="feedback_msg" style="margin-bottom: 20px" class="form-control" spellcheck="false" required></textarea>
                </div>
                <div class="form-group">
                    <label for="filedata" style="color: #000; font-family: "Roboto" font-weight: 500;">Attechment Files</label>
                    <input class="form-control" for="filedata" name="images[]" id="imageInput" type="file" style="background-color: #fff; padding: 10px; height: 50px; margin-bottom: 25px;" multiple>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="width: 150px">Submit</button>
                </div>
            </form>
            <ul class="chatbox" style="display: none;">
                <li class="chat incoming" style="display: none;">
                    <span class="material-icons">smart_toy</span>
                </li>
            </ul>
            <div class="chat-input" style="display: none;">
                <textarea style="display: none;" id="a" name="a" placeholder="Enter a message...." required></textarea>
                <span id="clickMe" class="material-icons">send</span>
            </div>
        </div>
    </div>
    `;


    newDiv.innerHTML = htmlContent;

    document.body.appendChild(newDiv);

    const chatInput = document.querySelector(".chat-input textarea");
    // const sendChatBtn = document.querySelector(".chat-input span");
    const sendChatBtn = document.getElementById("clickMe");
    const chatbox = document.querySelector(".chatbox");
    const chatbotToggler = document.querySelector(".chatbot-toggler");
    const element = document.getElementsByClassName('chatbot-togg');
    const chatbotCloseBtn = document.querySelector(".close-btn");

    let userMessage;
    const API_KEY = "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";

    const mainCode = document.createElement("div");


    const createChatLi = (message, className) => {
        // create a chat <li> element with passed message and classname
        const chatLi = document.createElement("li");
        chatLi.classList.add("chat", className);
        let chatContent = className === "outgoing" ?

            `<p></p>` : `${roboticon}<p></p>`;

        chatLi.innerHTML = chatContent;
        chatLi.querySelector("p").textContent = message
        return chatLi;
    }

    const generateResponse = (incomingChatLi) => {
        const questionansgen = `Please provide a human-like answer based on the given content. Please note when question answer not provide in given content then only provide simple contact us response, without mentioning that you are an AI or an agent. Keep the answer concise, like a response from a chatbot and please Note when question as give me list then provide answer as like list. content="Please enter your company content" and question="${userMessage}"`;


        const API_URL = "https://api.openai.com/v1/chat/completions";
        const messageElement = incomingChatLi.querySelector("p");

        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization":

                    `Bearer ${API_KEY}`

            },
            body: JSON.stringify({
                model: "gpt-3.5-turbo",
                messages: [{ role: "user", content: questionansgen }]
            })
        }

        // Send POST request to API, get response
        fetch(API_URL, requestOptions).then(res => res.json()).then(data => {
            messageElement.textContent = data.choices[0].message.content;
        }).catch((error) => {
            messageElement.classList.add("error");
            messageElement.textContent = "Oops! Something went wrong.Please try again.";
        }).finally(() => chatbox.scrollTo(0, chatbox.scrollHeight));
        const today = new Date();

        const day = String(today.getDate()).padStart(2, '0');
        const month = String(today.getMonth() + 1).padStart(2, '0'); // Months are zero-based, so we add 1
        const year = today.getFullYear();

        const formattedDate = `${day}/${month}/${year}`;
        const hours = today.getHours().toString().padStart(2, '0');
        const minutes = today.getMinutes().toString().padStart(2, '0');
        const formattedTime = `${hours}:${minutes}`;

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append('Access-Control-Allow-Origin', '*');
        myHeaders.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

        var raw = JSON.stringify({
            "coll_name": coll_name,
            "new_dict": { "cp_name": companyname, "question": userMessage, "date": formattedDate, "time": formattedTime },
            "db_name": db_name
        });

        var requestOptions1 = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://mongodeployappnew.onrender.com/add_data", requestOptions1)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }

    const handleChat = () => {
        userMessage = chatInput.value.trim();
        if (!userMessage) return;
        chatInput.value = "";

        // Append the user's message to the chatbox
        chatbox.appendChild(createChatLi(userMessage, "outgoing"));
        chatbox.scrollTo(0, chatbox.scrollHeight);

        setTimeout(() => {
            // Display "Thinking..." message while waiting for the response
            const incomingChatLi = createChatLi("Thinking...", "incoming");
            chatbox.appendChild(incomingChatLi);
            chatbox.scrollTo(0, chatbox.scrollHeight);
            generateResponse(incomingChatLi);
        }, 600)
    }

    chatInput.addEventListener("keydown", (e) => {
        if (e.key === "Enter" && !e.shiftKey && window.innerWidth > 800) {
            e.preventDefault();
            handleChat();
        }
    });

    sendChatBtn.addEventListener("click", handleChat);
    chatbotCloseBtn.addEventListener("click", () => document.body.classList.remove("show-chatbot"));
    chatbotToggler.addEventListener("click", () => document.body.classList.toggle("show-chatbot"));
});

function showLoader() {
    document.getElementById("loader").style.display = "block";
}
// Function to hide loader
function hideLoader() {
    document.getElementById("loader").style.display = "none";
}

$(document).ready(function() {
  $('#uploadForm').submit(function(event) {
    event.preventDefault(); // Prevent default form submission
    
    var formData = new FormData();
    formData.append('feedback_msg', $('#feedbackinput').val());
    formData.append('username_msg', $('#usernameinput').val());
    formData.append('subject_msg', $('#subjectinput').val());
    var files = $('#imageInput')[0].files;
    for (var i = 0; i < files.length; i++) {
      formData.append('images[]', files[i]); // Get all uploaded image files
    }
    
    $.ajax({
      url: '/admin/feedback_form', // Flask route to handle the upload
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function(response) {
        console.log(response); // Handle successful response
        window.location.reload()
      },
      error: function(xhr, status, error) {
        console.error(xhr.responseText); // Handle error
      }
    });
  });
});

function openpanel(panel) {
    window.location.href = panel;
}

