// Initialize EmojioneArea on the messageInput textarea

// const message = document.getElementById("messageInput").value
// const intro_scene = document.getElementById("intro_scene")
// const chat_scene = document.getElementById("chat_scene")
const chats = document.getElementsByClassName("chats")
const create_chat = document.getElementById("create_chat")
const chatbot_id = create_chat.value
const user_id = document.getElementById("user_id").value
const user_photo_link = document.getElementById("user_photo_link").value

var currentDate = new Date();
var hours = currentDate.getHours();
var minutes = currentDate.getMinutes();
var ampm = hours >= 12 ? 'PM' : 'AM';
hours = hours % 12;
hours = hours ? hours : 12; // the hour '0' should be '12'
minutes = minutes < 10 ? '0' + minutes : minutes;
var timeInAmPm = hours + ':' + minutes + ' ' + ampm;

create_chat.addEventListener('click', function() {
    const myObj = {'display': 'flex ', 'flex-direction': 'column', 'justify-content': 'flex-start' , 'text-align': 'center'}
    if (intro_scene.style.display !== 'none') {
        for (var property in myObj) {
            intro_scene.style.removeProperty(property);
            chat_scene.style.setProperty(property, myObj[property]);
        }
        intro_scene.classList.remove('h-100');
        intro_scene.classList.remove('w-100');
        intro_scene.style.display = 'none';
        chat_scene.classList.add('h-100');
        chat_scene.classList.add('w-100');
        chat_scene.style.display = 'block';
    } 
    else {
        for (var property in myObj) {
            intro_scene.style.setProperty(property, myObj[property]);
            chat_scene.style.removeProperty(property);
        }
        chat_scene.classList.remove('h-100');
        chat_scene.classList.remove('w-100');
        chat_scene.style.display = 'none';
        intro_scene.classList.add('h-100');
        intro_scene.classList.add('w-100');
        intro_scene.style.display = 'block';
    }
});
