// Define the search_id object
search_id = {
    "student": ["username", "contact_no", "email", "department"],
    "admin": ["username", "contact_no", "email"],
    "teacher": ["username", "contact_no", "email", "department"],
    "department": ["department_name", "HOD_name"],
    "subject": ["department_name", "subject_name"],
    "class": ["student_id", "department", "class_name"],
    "events": ["event_name", "event_date", "classes"]
};

// // Get the search button
const searchButton = document.getElementById("searchButton");
var panel = searchButton.value;

// Get the open button and popup container
const openButton = document.getElementById("openButton");
// Get the popup container
const popupContainer = document.getElementById("popupContainer");

// Get the close button
const closeButton = document.getElementById("closeButton");
// Get the delete_all_button
const deleteAllButton = document.getElementById("delete_all_button");

const deleteMainButton = document.getElementById("delete_main_button");

// // Get the delete_main_button

// Function to open the popup
function openPopup() {
    // Make the background screen semi-transparent
    event.preventDefault();
    popupContainer.style.display = "flex";
}

// Function to close the popup
function closePopup() {
    // Hide the popup
    event.preventDefault();
    popupContainer.style.display = "none";
}

// Add event listener to the delete_all_button
document.getElementById("delete_all_button").addEventListener("click", function (event) {
    // Set the href attribute to the Flask endpoint
    a_href_name = "/admin/deleteall/" + panel;
    this.href = a_href_name;
});

// Add event listeners to the open and close buttons
openButton.addEventListener("click", openPopup);
closeButton.addEventListener("click", closePopup);

// Function to check if any input field is empty
function checkInputData() {
    for (let i = 0; i < input_tags.length; i++) {
        if (input_tags[i].value !== '') {
            return true;
        }
    }

    return false;
}

// Function to submit the form
function submitForm() {
    // const hasInputData = checkInputData();
    const hasInputData = true;
    const form = document.getElementById('search');

    if (hasInputData) {
        form.action = "/search_data/" + panel;
        form.submit();
    } else {
        alert('Please input a search value.');
    }
}

function close_details() {
    document.getElementById('overlay').style.display = 'none';
}

// Add eventlistener to button with id searchButton, where once clicked it will add action to form with id search
searchButton.addEventListener("click", submitForm);

function show_detailed_data(search_value, type, display) {
    // const hasInputData = checkInputData();
    let result_dict = {};
    if (document.getElementById('all_data').value) {
        all_data = document.getElementById('all_data').value;
        result_dict = eval('(' + all_data + ')');
    }
    else {
        search_obj = search_value + "|" + type + "|display" ;
        // search_obj = document.getElementById("showDetails").value;
        console.log("Search obj is : ",search_obj);

        fetch('/search_data/'+search_obj)
            .then(response => response.json())
            .then(data => {
                // Use the fetched data here
                result_dict = data

                result_dict = Object.keys(result_dict)
                .sort((a, b) => b.localeCompare(a)) // Sort in decreasing order
                .reduce((obj, key) => {
                    obj[key] = result_dict[key];
                    return obj;
                }, {});

            if (type === 'student' || type === 'admin' || type === 'teacher') {
                document.getElementById('userPhoto').src = "/" + result_dict['photo_link'];
            } else {
                console.log("Not adding picture");
                // Hide id userPhoto
                document.getElementById('userPhoto').style.display = 'none';
            }
            // document.getElementById('userPhoto').src = "/" + result_dict['photo_link'];

            let tableHtml = "";
            // tableHtml += `<img class="avatar-img rounded-circle" id="userPhoto" src="/${result_dict['photo_link']}" alt="User Image";>`;
            for (var key in result_dict) {
                if (Object.hasOwnProperty.call(result_dict, key)) {
                    const value = result_dict[key];
                    if (key === 'photo_link') {
                    }
                    else {
                        key = key.replace('_', ' ')
                        //  Convert first letter of each word to capital
                        key = key.charAt(0).toUpperCase() + key.slice(1);
                        tableHtml += `<tr><th>${key}</th><td style="width: 100%"; "padding-left: 2rem";>${value}</td></tr>`;
                    }
                }
            }
            document.getElementById('userDataTable').innerHTML = tableHtml;
            // this.classList.add('active');
        })
        .catch(error => {
            // Handle any errors
            console.error('Error fetching data:', error);
        });


        setTimeout(() => {
            document.getElementById('overlay').style.display = 'flex'; // This will be executed after a delay of 2000 milliseconds
        }, 750);
    }

    let tableHtml = "";

    // Reoder result_dict based on alphabet
    result_dict = Object.keys(result_dict)
        .sort((a, b) => b.localeCompare(a)) // Sort in decreasing order
        .reduce((obj, key) => {
            obj[key] = result_dict[key];
            return obj;
        }, {});

    if (type === 'student' || type === 'admin' || type === 'teacher') {
        document.getElementById('userPhoto').src = "/" + result_dict['photo_link'];
    } else {
        console.log("Not adding picture");
        // Hide id userPhoto
        document.getElementById('userPhoto').style.display = 'none';
    }

    for (var key in result_dict) {
        if (Object.hasOwnProperty.call(result_dict, key)) {
            const value = result_dict[key];
            if (key === 'photo_link') {
            }
            else {
                key = key.replace('_', ' ')
                //  Convert first letter of each word to capital
                key = key.charAt(0).toUpperCase() + key.slice(1);
                tableHtml += `<tr><th>${key}</th><td style="width: 60%";>${value}</td></tr>`;
                tableHtml += `<tr><th>${key}</th><td style="width: 60%";>${value}</td></tr>`;
            }
        }
    }

    setTimeout(() => {
        document.getElementById('overlay').style.display = 'flex'; // This will be executed after a delay of 2000 milliseconds
    }, 750);

    document.getElementById('userDataTable').innerHTML = tableHtml;
    // this.classList.add('active');
}